﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;

namespace NinjaTraderCustomWorkspaces
{
  public class NTWorkspaceCollection : System.Collections.CollectionBase
  {
    private NTWorkspace       m_objMainBaseWorkspace;
    private NTWorkspace       m_objRemoteBaseWorkspace;
    private NTChartCollection m_objMainCharts;
    private NTChartCollection m_objRemoteCharts;
  
    public NTWorkspaceCollection()
    {
      m_objMainBaseWorkspace   = new NTWorkspace("Main60Min5MinAndFootPrint");
      m_objMainBaseWorkspace.CreateMain60Min5MinAndFootPrint();

      m_objRemoteBaseWorkspace = new NTWorkspace("Remote60Min5MinAndFootPrint");
      m_objRemoteBaseWorkspace.CreateRemote60Min5MinAndFootPrint();

      m_objMainCharts          = new NTChartCollection();
      m_objMainCharts.FillMain();

      m_objRemoteCharts        = new NTChartCollection();
      m_objRemoteCharts.FillRemote();
    }

    public void Fill()
    {
      this.Clear();

      using (OleDbConnection objConnection = new OleDbConnection(Program.WorkspacesDataConnectionString))
      {
        objConnection.Open();
        OleDbCommand objCommand = objConnection.CreateCommand();
        StringBuilder sbQuery   = new StringBuilder();

        sbQuery.Append("SELECT tbl_Workspaces.ID, tbl_Workspaces.WorkspaceName FROM tbl_Workspaces;");

        objCommand.CommandText = sbQuery.ToString();
      
        using (OleDbDataReader objReader = objCommand.ExecuteReader())
        {
          while (objReader.Read())
          {
            this.Add(new NTWorkspace((Int32)objReader[0], (String)objReader[1], m_objMainCharts, m_objRemoteCharts));
          }
        }
      }
    }

    public Int32 Add(NTWorkspace objNTWorkspace)
    {
      return (List.Add(objNTWorkspace));
    }

    public Int32 IndexOf(NTWorkspace objNTWorkspace)
    {
      return (List.IndexOf(objNTWorkspace));
    }

    public void Insert(Int32 index, NTWorkspace objNTWorkspace)
    {
      List.Insert(index, objNTWorkspace);
    }

    public void Remove(NTWorkspace objNTWorkspace)
    {
      List.Remove(objNTWorkspace);
    }

    public NTWorkspace this[String strName]
    {
      get
      {
        for (int i = 0; i < this.Count; i++)
        {
          if (this[i].Name == strName)
              return this[i] as NTWorkspace;
        }

        return null;
      }
    }

    public NTWorkspace this[int index]
    {
      get
      {
        return (List[index] as NTWorkspace);
      }
      set
      {
        List[index] = value;
      }
    }


    public NTWorkspace MainBaseWorkspace
    {
      get
      {
        return m_objMainBaseWorkspace;
      }
    }

    public NTWorkspace RemoteBaseWorkspace
    {
      get
      {
        return m_objRemoteBaseWorkspace;
      }
    }

    public NTChartCollection MainCharts
    {
      get
      {
        return m_objMainCharts;
      }
    }

    public NTChartCollection RemoteCharts
    {
      get
      {
        return m_objRemoteCharts;
      }
    }
  }
}
