﻿namespace NinjaTraderCustomWorkspaces
{
  partial class frm_Alert
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.lblMessage = new System.Windows.Forms.Label();
      this.tmAlert = new System.Windows.Forms.Timer(this.components);
      this.SuspendLayout();
      // 
      // lblMessage
      // 
      this.lblMessage.AutoSize = true;
      this.lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.lblMessage.ForeColor = System.Drawing.Color.DarkSeaGreen;
      this.lblMessage.Location = new System.Drawing.Point(12, 19);
      this.lblMessage.Name = "lblMessage";
      this.lblMessage.Size = new System.Drawing.Size(94, 24);
      this.lblMessage.TabIndex = 0;
      this.lblMessage.Text = "Message";
      // 
      // frm_Alert
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.Yellow;
      this.ClientSize = new System.Drawing.Size(538, 68);
      this.Controls.Add(this.lblMessage);
      this.Name = "frm_Alert";
      this.Text = "frm_Alert";
      this.TopMost = true;
      this.Load += new System.EventHandler(this.frm_Alert_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label lblMessage;
    private System.Windows.Forms.Timer tmAlert;
  }
}