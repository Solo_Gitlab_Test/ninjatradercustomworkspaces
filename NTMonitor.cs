﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace NinjaTraderCustomWorkspaces
{
  public class NTMonitor
  {
    private Enums.ChartLocation m_enmChartLocation;
    private String              m_strName;
    private Rectangle           m_recScreenBounds;

    public NTMonitor(Enums.ChartLocation enmChartLocation, String strName, Rectangle recScreenBounds)
    {
      m_enmChartLocation         = enmChartLocation;
      m_strName                  = strName;

      m_recScreenBounds.Location = recScreenBounds.Location;
      m_recScreenBounds.Width    = recScreenBounds.Width;
      m_recScreenBounds.Height   = recScreenBounds.Height;

    }

    public Enums.ChartLocation ChartLocation
    {
      get
      {
        return m_enmChartLocation;
      }
    }

    public String Name
    {
      get
      {
        return m_strName;
      }
    }

    public Rectangle ScreenBounds
    {
      get
      {
        return m_recScreenBounds;
      }
    }
  }
}
