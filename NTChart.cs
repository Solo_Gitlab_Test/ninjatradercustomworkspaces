﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Data.OleDb;
using System.Text.RegularExpressions;

namespace NinjaTraderCustomWorkspaces
{
  public class NTChart : IComparable<NTChart>
  {
    private IntPtr                      m_hndHandle;
    private String                      m_strChartWindowCaption;
    private String                      m_strName;
    private String                      m_strPeriod;
    private Enums.ChartType             m_enmChartType;
    private Rectangle                   m_recRectangleArea;     
    private String                      m_strShortName;
    private Int32                       m_iGridX;
    private Int32                       m_iGridY;

    public const String                 CHART_PERIOD_5_MIN                 = "(5 Min)";
    public const String                 CHART_PERIOD_60_MIN                = "(60 Min)";
    public const String                 CHART_PERIOD_1440_MIN              = "(1440 Min)";
    public const String                 CHART_PERIOD_Weekly                = "(Weekly)";

    public const String                 CHART_CHARTTYPE_MIN                = "Min";
    public const String                 CHART_CHARTTYPE_FOOTPRINT_RANGE    = "Range";
    public const String                 CHART_CHARTTYPE_FOOTPRINT_VOLUME   = "Volume";
    public const String                 CHART_CHARTTYPE_FOOTPRINT_REVERSAL = "Reversal";

    public const String                 CHART_COMMON_ALERTS                = "Alerts";
    public const String                 CHART_COMMON_CONTROLCENTER         = "Control Center";

    private List<String> m_lstLowTimeframe = new List<string> { "(1 Min)", "(2 Min)", "(3 Min)", "(4 Min)", "(5 Min)", "(10 Min)", "(15 Min)", "(20 Min)" };
    private List<String> m_lstUpTimeframe  = new List<string> { "(30 Min)", "(60 Min)", "(120 Min)", "(180 Min)", "(240 Min)" };

    private Enums.ChartLocation m_enmLocation                              = Enums.ChartLocation.Main;


    public NTChart(String strChartWindowCaption, IntPtr Handle, Enums.ChartLocation enmLocation)
    {
      m_enmLocation = enmLocation;
      Create(strChartWindowCaption, Handle);
    }
    
    public NTChart(String strChartWindowCaption, IntPtr Handle)
    {
      m_enmLocation = Enums.ChartLocation.Main;
      Create(strChartWindowCaption, Handle);
    }

    private void Create(String strChartWindowCaption, IntPtr Handle)  
    {
      if (Regex.IsMatch(strChartWindowCaption, Program.RegExpKey, RegexOptions.IgnoreCase))
      {
          Match m                 = Regex.Match(strChartWindowCaption, Program.RegExpKey, RegexOptions.IgnoreCase);
          m_strChartWindowCaption = (strChartWindowCaption.Substring(0, strChartWindowCaption.IndexOf(m.Value))).Trim();
      }
      else
          m_strChartWindowCaption = strChartWindowCaption;
 
      m_hndHandle             = Handle;
      m_strName               = strChartWindowCaption.Substring(0, strChartWindowCaption.IndexOf('(') - 1);
      m_strPeriod             = strChartWindowCaption.Substring(strChartWindowCaption.IndexOf('('), strChartWindowCaption.IndexOf(')') - strChartWindowCaption.IndexOf('(') + 1);
      m_strShortName          = strChartWindowCaption.Substring(0, strChartWindowCaption.IndexOf('-') - 3);
      m_recRectangleArea      = Program.ScreensResolution.MainSecondScreenBounds;

      if (m_strPeriod.IndexOf(CHART_PERIOD_5_MIN) != -1)
          m_enmChartType = Enums.ChartType.Time5Min;

      if (m_strPeriod.IndexOf(CHART_PERIOD_60_MIN) != -1)
          m_enmChartType = Enums.ChartType.Time60Min;

      if (m_strPeriod.IndexOf(CHART_CHARTTYPE_FOOTPRINT_RANGE) != -1 ||
          m_strPeriod.IndexOf(CHART_CHARTTYPE_FOOTPRINT_VOLUME) != -1 ||
          m_strPeriod.IndexOf(CHART_CHARTTYPE_FOOTPRINT_REVERSAL) != -1)
      {
          m_enmChartType = Enums.ChartType.FootPrint;
      } 
    }

    public void SetWindowSizeAndMoveTo(IntPtr hndHandle, Rectangle recRectangleArea)
    {
      if (this.Location == Enums.ChartLocation.Main)
          Emulations.Window.SetWindowSizeAndMoveTo(hndHandle, recRectangleArea);

      if (this.Location == Enums.ChartLocation.Remote)
      {
          String strMessage = "SetWindowSizeAndMoveTo|" +
                              Emulations.Convert.Functions.IntPtrToStringHex(hndHandle) + "_" +
                              Emulations.Convert.Functions.ToString(recRectangleArea);

          Program.SendMessageToNTServer(strMessage);
      }
    }

    public void SetPositionFullScreen(Rectangle recScreenBounds)
    {
      Point pLocation = recScreenBounds.Location;
      Int32 iWidth    = recScreenBounds.Width;
      Int32 iHeith    = recScreenBounds.Height;

      this.m_recRectangleArea.Location = pLocation;
      this.m_recRectangleArea.Width    = iWidth;
      this.m_recRectangleArea.Height   = iHeith;

      SetWindowSizeAndMoveTo(this.m_hndHandle, this.m_recRectangleArea);
    }

    public void SetPositionTopHalfScreenHeight(Rectangle recScreenBounds)
    {
      Point pLocation = recScreenBounds.Location;
      Int32 iWidth    = recScreenBounds.Width;
      Int32 iHeith    = recScreenBounds.Height / 2;

      this.m_recRectangleArea.Location = pLocation;
      this.m_recRectangleArea.Width    = iWidth;
      this.m_recRectangleArea.Height   = iHeith;

      SetWindowSizeAndMoveTo(this.m_hndHandle, this.m_recRectangleArea);
    }

    public void SetPositionQuarter(Rectangle recScreenBounds, Enums.ChartQuarterPosition enmChartQuarterPosition)
    {
      Point pLocation = recScreenBounds.Location;
      Int32 iWidth    = recScreenBounds.Width / 2;
      Int32 iHeith    = recScreenBounds.Height / 2;

      if (enmChartQuarterPosition == Enums.ChartQuarterPosition.TopLeft)
      { 
      }

      if (enmChartQuarterPosition == Enums.ChartQuarterPosition.TopRight)
      {
        pLocation.X = recScreenBounds.Location.X + (recScreenBounds.Width / 2);
      }

      if (enmChartQuarterPosition == Enums.ChartQuarterPosition.BottomLeft)
      {
        pLocation.Y = recScreenBounds.Location.Y + (recScreenBounds.Height / 2);
      }

      if (enmChartQuarterPosition == Enums.ChartQuarterPosition.BottomRight)
      {
        pLocation.Y = recScreenBounds.Location.Y + (recScreenBounds.Height / 2);
        pLocation.X = recScreenBounds.Location.X + (recScreenBounds.Width / 2);
      }

      if (enmChartQuarterPosition == Enums.ChartQuarterPosition.FullScreen)
      {
        iWidth = recScreenBounds.Width; 
        iHeith = recScreenBounds.Height;
      }

      this.m_recRectangleArea.Location = pLocation;
      this.m_recRectangleArea.Width    = iWidth;
      this.m_recRectangleArea.Height   = iHeith;

      SetWindowSizeAndMoveTo(this.m_hndHandle, this.m_recRectangleArea);
    }

    public void SetPositionBottomHalfScreenHeight(Rectangle recScreenBounds)
    {
      Point pLocation = recScreenBounds.Location;
      Int32 iWidth    = recScreenBounds.Width;
      Int32 iHeith    = recScreenBounds.Height / 2;

      pLocation.Y     = recScreenBounds.Location.Y + (recScreenBounds.Height / 2) - 35;

      this.m_recRectangleArea.Location  = pLocation;
      this.m_recRectangleArea.Width     = iWidth;
      this.m_recRectangleArea.Height    = iHeith + 35;

      SetWindowSizeAndMoveTo(this.m_hndHandle, this.m_recRectangleArea);
    }

    public void SetPositionLeftHalfScreenWidth(Rectangle recScreenBounds)
    {
      Point pLocation = recScreenBounds.Location;
      Int32 iWidth    = recScreenBounds.Width / 2;
      Int32 iHeith    = recScreenBounds.Height;

      this.m_recRectangleArea.Location = pLocation;
      this.m_recRectangleArea.Width    = iWidth;
      this.m_recRectangleArea.Height   = iHeith;

      SetWindowSizeAndMoveTo(this.m_hndHandle, this.m_recRectangleArea);
    }

    public void SetPositionRightHalfScreenWidth(Rectangle recScreenBounds)
    {
      Point pLocation = recScreenBounds.Location;
      Int32 iWidth    = recScreenBounds.Width / 2;
      Int32 iHeith    = recScreenBounds.Height;

      pLocation.X = recScreenBounds.Location.X + (recScreenBounds.Width / 2);

      this.m_recRectangleArea.Location = pLocation;
      this.m_recRectangleArea.Width    = iWidth;
      this.m_recRectangleArea.Height   = iHeith;

      SetWindowSizeAndMoveTo(this.m_hndHandle, this.m_recRectangleArea);
    }

    public void SetPositionTopLeftQuarterScreenWidth(Rectangle recScreenBounds)
    {
      Point pLocation = recScreenBounds.Location;
      Int32 iWidth = recScreenBounds.Width / 2;
      Int32 iHeith = recScreenBounds.Height / 2;

      this.m_recRectangleArea.Location = pLocation;
      this.m_recRectangleArea.Width = iWidth;
      this.m_recRectangleArea.Height = iHeith;

      SetWindowSizeAndMoveTo(this.m_hndHandle, this.m_recRectangleArea);
    }

    public void SetPositionBottomLeftQuarterScreenWidth(Rectangle recScreenBounds)
    {
      Point pLocation = recScreenBounds.Location;
      Int32 iWidth = recScreenBounds.Width / 2;
      Int32 iHeith = recScreenBounds.Height / 2;

      pLocation.Y = recScreenBounds.Location.Y + (recScreenBounds.Height / 2);

      this.m_recRectangleArea.Location = pLocation;
      this.m_recRectangleArea.Width = iWidth;
      this.m_recRectangleArea.Height = iHeith;

      SetWindowSizeAndMoveTo(this.m_hndHandle, this.m_recRectangleArea);
    }

    public void SetPosition(Point Location, Int32 Width, Int32 Heith)
    {
      Point pLocation = Location;
      Int32 iWidth    = Width;
      Int32 iHeith    = Heith;

      this.m_recRectangleArea.Location = pLocation;
      this.m_recRectangleArea.Width    = iWidth;
      this.m_recRectangleArea.Height   = iHeith;

      SetWindowSizeAndMoveTo(this.m_hndHandle, this.m_recRectangleArea);
    }

    public void SetPositionAndShow()
    {
      SetWindowSizeAndMoveTo(this.m_hndHandle, this.m_recRectangleArea);
      this.Show();
    }

    public void Show()
    {
      if (this.Location == Enums.ChartLocation.Main)
          Emulations.Window.Activate(this.m_hndHandle);

      if (this.Location == Enums.ChartLocation.Remote)
      {
          String strMessage = "Show|" +
                              Emulations.Convert.Functions.IntPtrToStringHex(this.m_hndHandle);

          Program.SendMessageToNTServer(strMessage);
      }
    }

    public void Hide()
    {
      if (this.Location == Enums.ChartLocation.Main)
          Emulations.Window.ShowExt(this.m_hndHandle, Emulations.WindowEnums.WindowShowStyle.Hide);

      if (this.Location == Enums.ChartLocation.Remote)
      {
          String strMessage = "Hide|" +
                              Emulations.Convert.Functions.IntPtrToStringHex(this.m_hndHandle);

          Program.SendMessageToNTServer(strMessage);
      }          
    }

    public int CompareTo(NTChart objOtherNTChart)
    {
      /*
       * ссылка на реализацию сортировки объектов коллекции (и по string и по int
       * http://msdn.microsoft.com/ru-ru/library/ybcx56wz.aspx#BKMK_Sorting
       */
      int compare;
      compare = String.Compare(this.Name, objOtherNTChart.Name, true);

      if (compare == 0)
      {
          //int i = Convert.ToInt32(this.ChartType);
          //int j = Convert.ToInt32(objOtherNTChart.ChartType);
          //compare = i.CompareTo(j);
          compare = this.ChartType.CompareTo(objOtherNTChart.ChartType);
      }

      return compare;
    }

    public Boolean IsAlerts
    {
      get
      {
        return (m_strPeriod == CHART_COMMON_ALERTS) ? true : false;
      }
    }

    public Boolean IsControlCenter
    {
      get
      {
        return (m_strPeriod == CHART_COMMON_CONTROLCENTER) ? true : false;
      }
    }

    public Boolean IsWeeklyChart
    {
      get
      {
        return (m_strPeriod == CHART_PERIOD_Weekly) ? true : false;
      }
    }
    
    public Boolean Is1440MinChart
    {
      get
      {
        return (m_strPeriod == CHART_PERIOD_1440_MIN) ? true : false;
      } 
    }

    public Boolean Is60MinChart
    {
      get
      {
        //return (m_strPeriod == CHART_PERIOD_60_MIN) ? true : false;

        for (int i = 0; i < m_lstUpTimeframe.Count; i++)
        {
          if (m_strPeriod == m_lstUpTimeframe[i])
              return true;
        }

        return false;

      }
    }

    public Boolean Is5MinChart
    {
      get
      {
        //return (m_strPeriod == CHART_PERIOD_5_MIN || m_strPeriod == "(1 Min)" || m_strPeriod == "(2 Min)") ? true : false;
        
        for (int i = 0; i < m_lstLowTimeframe.Count; i++)
        {
          if (m_strPeriod == m_lstLowTimeframe[i])
              return true;
        }

        return false;
      }
    }

    public Boolean IsTimeChart
    {
      get
      {
        return (m_enmChartType == Enums.ChartType.Time5Min || m_enmChartType == Enums.ChartType.Time60Min) ? true : false;
      }
    }

    public Boolean IsFootPrintChart
    {
      get
      {
        return (m_enmChartType == Enums.ChartType.FootPrint) ? true : false;
      }
    }

    public IntPtr Handle
    {
      get
      {
        return m_hndHandle;
      }
      set
      {
        m_hndHandle = value;
      }
    }

    public String ChartWindowCaption
    {
      get
      {
        return m_strChartWindowCaption;
      }
    }

    public String Name
    {
      get
      {
        return m_strName;
      }
      set
      {
        m_strName = value;
      }
    }

    public string Period
    {
      get
      {
        return m_strPeriod;
      }
      set
      {
        m_strPeriod = value;
      }
    }

    public Rectangle RectangleArea
    {
      get
      {
        return m_recRectangleArea;
      }
      set
      {
        m_recRectangleArea = value;
      }
    }

    public Enums.ChartType ChartType
    {
      get
      {
        return m_enmChartType;
      }
    }

    public Enums.ChartDisposition Disposition
    {
      get
      {
        Enums.ChartDisposition enmReturnValue = Enums.ChartDisposition.Empty;

        if (this.Location == Enums.ChartLocation.Main)
        {
            if (this.RectangleArea.X >= Program.ScreensResolution.MainFirstScreenBounds.X &&
                this.RectangleArea.X < Program.ScreensResolution.MainFirstScreenBounds.X +
                                       Program.ScreensResolution.MainFirstScreenBounds.Width)
            {
                enmReturnValue = Enums.ChartDisposition.MainFirstScreen;
            }

            if (this.RectangleArea.X >= Program.ScreensResolution.MainSecondScreenBounds.X &&
                this.RectangleArea.X < Program.ScreensResolution.MainSecondScreenBounds.X +
                                       Program.ScreensResolution.MainSecondScreenBounds.Width)
            {
                enmReturnValue = Enums.ChartDisposition.MainSecondScreen;
            }
        }

        if (this.Location == Enums.ChartLocation.Remote)
        {
            if (this.RectangleArea.X >= Program.ScreensResolution.RemoteFirstScreenBounds.X &&
                this.RectangleArea.X < Program.ScreensResolution.RemoteFirstScreenBounds.X +
                                       Program.ScreensResolution.RemoteFirstScreenBounds.Width)
            {
                enmReturnValue = Enums.ChartDisposition.RemoteFirstScreen;
            }

            if (this.RectangleArea.X >= Program.ScreensResolution.RemoteSecondScreenBounds.X &&
                this.RectangleArea.X < Program.ScreensResolution.RemoteSecondScreenBounds.X +
                                       Program.ScreensResolution.RemoteSecondScreenBounds.Width)
            {
                enmReturnValue = Enums.ChartDisposition.RemoteSecondScreen;
            }
        }

        return enmReturnValue;
      }
    }

    public string ShortName
    {
      get
      {
        return m_strShortName;
      }
    }

    public int GridX
    {
      get
      {
        return m_iGridX;
      }
      set
      {
        m_iGridX = value;
      }
    }

    public int GridY
    {
      get
      {
        return m_iGridY;
      }
      set
      {
        m_iGridY = value;
      }
    }

    public Enums.ChartLocation Location
    {
      get
      {
        return m_enmLocation;
      }
    }


  }
}
