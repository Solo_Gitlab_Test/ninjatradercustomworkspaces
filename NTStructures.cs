﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace NinjaTraderCustomWorkspaces
{
  public class NTStructures
  {
    public struct ChartPoint
    {
      public Int32 X;
      public Int32 Y;
    }
  }
}
