﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NinjaTraderCustomWorkspaces
{
  public class NTMonitorCollection : System.Collections.CollectionBase
  {
    public void Create() // заменить на Fill из базы. таблица уже есть.
    {
      // связка идет по тексту на кнопке, обозначающей монитор
      this.Add(new NTMonitor(Enums.ChartLocation.Main,   "Fujitsu 1", 
                             Program.ScreensResolution.MainFirstScreenWorkingArea));

      this.Add(new NTMonitor(Enums.ChartLocation.Main,   "Fujitsu 2",
                             Program.ScreensResolution.MainSecondScreenBounds));

      this.Add(new NTMonitor(Enums.ChartLocation.Remote, "Lenovo 1",
                             Program.ScreensResolution.RemoteFirstScreenWorkingArea));

      this.Add(new NTMonitor(Enums.ChartLocation.Remote, "Lenovo 2",
                             Program.ScreensResolution.RemoteSecondScreenBounds));
    }

    public Int32 Add(NTMonitor objNTMonitor)
    {
      return (List.Add(objNTMonitor));
    }

    public Int32 IndexOf(NTMonitor objNTMonitor)
    {
      return (List.IndexOf(objNTMonitor));
    }

    public void Insert(Int32 index, NTMonitor objNTMonitor)
    {
      List.Insert(index, objNTMonitor);
    }

    public void Remove(NTMonitor objNTMonitor)
    {
      List.Remove(objNTMonitor);
    }

    public NTMonitor this[int index]
    {
      get
      {
        return (List[index] as NTMonitor);
      }
      set
      {
        List[index] = value;
      }
    }

    public NTMonitor this[String strName]
    {
      get
      {
        for (int i = 0; i < this.Count; i++)
        {
          if (this[i].Name == strName)
              return this[i] as NTMonitor;
        }

        return null;
      }
    }

  }
}
