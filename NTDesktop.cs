﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Data.OleDb;

namespace NinjaTraderCustomWorkspaces
{
  public class NTDesktop
  {
    private Int32             m_iID;
    private Int32             m_iWorkspaceID;
    private String            m_strName;
    private NTChartCollection m_objChartCollection;
    private Int32             m_iGridRowsCount;
    private Int32             m_iGridColumnsCount;
    private NTMonitor         m_objNTMonitor;

    public NTDesktop(String strName)
    {
      m_strName              = strName;
      m_objChartCollection   = new NTChartCollection();
    }

    public NTDesktop(Int32 iID, String strName, String strMonitorName, Int32 iGridRowsCount, Int32 iGridColumnsCount)
    {
      m_iID                = iID;
      m_strName            = strName;
      m_objNTMonitor       = Program.MonitorCollection[strMonitorName];
      m_iGridRowsCount     = iGridRowsCount;
      m_iGridColumnsCount  = iGridColumnsCount;
      m_objChartCollection = new NTChartCollection();
    }

    public NTDesktop(Int32 iWorkspaceID, String strName, NTMonitor objNTMonitor, DataGridView grdDesktop, 
                     NTChartCollection colMainCharts, NTChartCollection colRemoteCharts)
    {
      m_iWorkspaceID        = iWorkspaceID;
      m_strName             = strName;
      m_iGridRowsCount      = grdDesktop.RowCount;
      m_iGridColumnsCount   = grdDesktop.ColumnCount;
      m_objChartCollection  = new NTChartCollection();
      m_objNTMonitor        = objNTMonitor;

      foreach (DataGridViewRow objRow in grdDesktop.Rows)
      {
        foreach (DataGridViewTextBoxCell objCell in objRow.Cells)
        {
          if (objCell.Value != null)
          {
            NTChart objNTChart = null;

            if (m_objNTMonitor.ChartLocation == Enums.ChartLocation.Main)
            {
              objNTChart = new NTChart(colMainCharts[objCell.Value.ToString()].ChartWindowCaption,
                                       colMainCharts[objCell.Value.ToString()].Handle,
                                       Enums.ChartLocation.Main);
            }

            if (m_objNTMonitor.ChartLocation == Enums.ChartLocation.Remote)
            {
              objNTChart = new NTChart(colRemoteCharts[objCell.Value.ToString()].ChartWindowCaption,
                                       colRemoteCharts[objCell.Value.ToString()].Handle,
                                       Enums.ChartLocation.Remote);
            }

            objNTChart.GridX = objCell.ColumnIndex;
            objNTChart.GridY = objCell.RowIndex;

            m_objChartCollection.Add(objNTChart);
          }
        }
      }
    }

    public void Create()
    {
      using (OleDbConnection objConnection = new OleDbConnection(Program.WorkspacesDataConnectionString))
      {
        OleDbCommand objCommand            = objConnection.CreateCommand();
        OleDbTransaction objTransaction    = null;
        StringBuilder sbInsertDesktopQuery = new StringBuilder();

        sbInsertDesktopQuery.Append("INSERT INTO tbl_Desktops (WorkspaceID, DesktopName, MonitorName, GridRowsCount, GridColumnsCount) VALUES (");
        sbInsertDesktopQuery.Append(this.m_iWorkspaceID.ToString() + ",");
        sbInsertDesktopQuery.Append("'" + this.m_strName + "'" + ",");
        sbInsertDesktopQuery.Append("'" + this.m_objNTMonitor.Name + "',");
        sbInsertDesktopQuery.Append(this.m_iGridRowsCount.ToString() + ",");
        sbInsertDesktopQuery.Append(this.m_iGridColumnsCount.ToString() + ");");

        try
        {
          objConnection.Open();

          objTransaction         = objConnection.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
          objCommand.Transaction = objTransaction;

          objCommand.CommandText = sbInsertDesktopQuery.ToString();
          objCommand.ExecuteNonQuery();

          objCommand.CommandText = "select @@identity";
          this.m_iID             = int.Parse(objCommand.ExecuteScalar().ToString());

          foreach (NTChart objNTChart in this.m_objChartCollection)
          {
            StringBuilder sbInsertChartQuery = new StringBuilder();

            sbInsertChartQuery.Append("INSERT INTO tbl_Charts (DesktopID, ChartWindowCaption, ");
            sbInsertChartQuery.Append("RectangleAreaToString, GridX, GridY, ChartLocation) VALUES (");
            sbInsertChartQuery.Append(this.m_iID.ToString() + ",");
            sbInsertChartQuery.Append("'" + objNTChart.ChartWindowCaption + "',");
            sbInsertChartQuery.Append("'" + Emulations.Convert.Functions.ToString(objNTChart.RectangleArea) + "',");
            sbInsertChartQuery.Append(objNTChart.GridX.ToString() + ",");
            sbInsertChartQuery.Append(objNTChart.GridY.ToString() + ",");
            sbInsertChartQuery.Append((int)objNTChart.Location);
            sbInsertChartQuery.Append(")");

            objCommand.CommandText = sbInsertChartQuery.ToString();
            objCommand.ExecuteNonQuery();
          }

          objTransaction.Commit();
        }
        catch(Exception ex)
        {
          try
          {
            Program.ShowErrorMessage(ex.Message);
            objTransaction.Rollback();
          }
          catch
          { 
          }
        }
      } 
    }

    public void Regenerate(NTChartCollection colMainCharts, NTChartCollection colRemoteCharts)
    {
      if (this.m_iID <= 0)
          return;

      this.m_objChartCollection.Clear();

      using (OleDbConnection objConnection = new OleDbConnection(Program.WorkspacesDataConnectionString))
      {
        objConnection.Open();
        OleDbCommand objCommand = objConnection.CreateCommand();
        StringBuilder sbQuery   = new StringBuilder();

        sbQuery.Append("SELECT tbl_Desktops.WorkspaceID, tbl_Desktops.DesktopName, tbl_Desktops.MonitorName, ");
        sbQuery.Append("tbl_Charts.ChartWindowCaption, tbl_Charts.RectangleAreaToString, tbl_Charts.GridX, ");
        sbQuery.Append("tbl_Charts.GridY, tbl_Charts.ChartLocation ");
        sbQuery.Append("FROM tbl_Desktops INNER JOIN tbl_Charts ON tbl_Desktops.ID=tbl_Charts.DesktopID ");
        sbQuery.Append("WHERE tbl_Desktops.ID=" + this.m_iID);

        objCommand.CommandText = sbQuery.ToString();

        using (OleDbDataReader objReader = objCommand.ExecuteReader())
        {
          Boolean blnIsFirstRecord = true;

          while (objReader.Read())
          {
            if (blnIsFirstRecord)
            {
                this.m_iWorkspaceID = (Int32)objReader[0];
                this.m_strName      = (String)objReader[1];
                this.m_objNTMonitor = Program.MonitorCollection[(String)objReader[2]];
                blnIsFirstRecord    = false;
            }

            String strChartWindowCaption         = (String)objReader[3];
            Enums.ChartLocation enmChartLocation = (Enums.ChartLocation)objReader[7];           
            NTChart objNTChart                   = null;
            IntPtr hndChartHandle                = IntPtr.Zero;

            if (enmChartLocation == Enums.ChartLocation.Main)
                hndChartHandle = colMainCharts.GetHandleByChartWindowCaption(strChartWindowCaption);

            if (enmChartLocation == Enums.ChartLocation.Remote)
                hndChartHandle = colRemoteCharts.GetHandleByChartWindowCaption(strChartWindowCaption);

            if (hndChartHandle != IntPtr.Zero)
            {
                objNTChart = new NTChart(strChartWindowCaption, hndChartHandle, enmChartLocation);
              
                objNTChart.RectangleArea = Emulations.Convert.Functions.StringToRectangle((String)objReader[4]);
                objNTChart.GridX         = (Int32)objReader[5];
                objNTChart.GridY         = (Int32)objReader[6];
              
                this.ChartCollection.Add(objNTChart);
            }
          }
        }
      }
    }

    public void Delete()
    {
      using (OleDbConnection objConnection = new OleDbConnection(Program.WorkspacesDataConnectionString))
      {
        OleDbCommand objCommand            = objConnection.CreateCommand();
        OleDbTransaction objTransaction    = null;
        StringBuilder sbDeleteDesktopQuery = new StringBuilder();
        StringBuilder sbDeleteChartsQuery  = new StringBuilder();

        sbDeleteDesktopQuery.Append("DELETE FROM tbl_Desktops WHERE tbl_Desktops.ID="   + this.m_iID);
        sbDeleteChartsQuery.Append("DELETE FROM tbl_Charts WHERE tbl_Charts.DesktopID=" + this.m_iID);

        try
        {
          objConnection.Open();

          objTransaction         = objConnection.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
          objCommand.Transaction = objTransaction;

          objCommand.CommandText = sbDeleteChartsQuery.ToString();
          objCommand.ExecuteNonQuery();

          objCommand.CommandText = sbDeleteDesktopQuery.ToString();
          objCommand.ExecuteNonQuery();

          objTransaction.Commit();
        }
        catch (Exception ex)
        {
          try
          {
            Program.ShowErrorMessage(ex.Message);
            objTransaction.Rollback();
          }
          catch
          {
          }
        }
      }
    }

    public void CreateMain60Min5MinAndFootPrint(String strInstrumentName)
    {
      m_objChartCollection.FillMain60Min5MinAndFootPrint(strInstrumentName);
    }

    public void CreateRemote60Min5MinAndFootPrint(String strInstrumentName)
    {
      m_objChartCollection.FillRemote60Min5MinAndFootPrint(strInstrumentName);
    }

    public void DisposeChartsOnScreen(Rectangle recScreenBounds)
    {
      // Разрешение большого монитора нацело делится до конструкции 6 на 6 клеток.
      // нужно подумать о целочисленном делении - об универсальности.
      //Int32 iChartWidth   = recScreenBounds.Width  % m_iGridColumnsCount;
      //Int32 iChartHeight  = recScreenBounds.Height % m_iGridRowsCount;

      if (m_iGridColumnsCount == 0 || m_iGridRowsCount == 0)
          return;

      if (m_objChartCollection.Count == 0)
          return;

      Int32 iChartWidth   = recScreenBounds.Width  / m_iGridColumnsCount;
      Int32 iChartHeight  = recScreenBounds.Height / m_iGridRowsCount;
      
      Int32 iX;
      Int32 iY;

      iY = recScreenBounds.Location.Y;

      for (int y = 0; y < m_iGridRowsCount; y++)
      {
        iX = recScreenBounds.Location.X;

        for (int x = 0; x < m_iGridColumnsCount; x++)        
        {
          NTStructures.ChartPoint structChartPoint = new NTStructures.ChartPoint();
          structChartPoint.X = x;
          structChartPoint.Y = y;

          if (m_objChartCollection[structChartPoint] != null)
              m_objChartCollection[structChartPoint].SetPosition(new Point(iX, iY), iChartWidth, iChartHeight);

          iX = iX + iChartWidth;
        }

        iY = iY + iChartHeight;
      }
    }

    public void DisposeChartsOnGrid(DataGridView grdDesktop)
    {
      grdDesktop.Columns.Clear();
      grdDesktop.Rows.Clear();

      if (this.ChartCollection.Count == 0)
          return;

      for (int i = 0; i < m_iGridRowsCount; i++)
      {
        if (grdDesktop.Columns.Count == 0)
        {
            for (int j = 0; j < m_iGridColumnsCount; j++)
            {
              DataGridViewTextBoxColumn objColumn = new DataGridViewTextBoxColumn();
              objColumn.AutoSizeMode              = DataGridViewAutoSizeColumnMode.Fill;

              grdDesktop.Columns.Add(objColumn);
            }
        }
        else
        {
            DataGridViewTextBoxCell objCell = new DataGridViewTextBoxCell();
            DataGridViewRow objRow          = new DataGridViewRow();

            objRow.Cells.Add(objCell);
            grdDesktop.Rows.Add(objRow);
        }
      }

      foreach (DataGridViewRow objRow in grdDesktop.Rows)
        objRow.Height = 30;

      for (int i = 0; i < m_iGridRowsCount; i++)
      {
        for (int j = 0; j < m_iGridColumnsCount; j++)
        {
          NTStructures.ChartPoint structChartPoint = new NTStructures.ChartPoint();

          structChartPoint.X = j;
          structChartPoint.Y = i;

          if (this.ChartCollection[structChartPoint] == null)
              continue;

          grdDesktop.Rows[i].Cells[j].Value = this.ChartCollection[structChartPoint].ShortName + " " +
                                              this.ChartCollection[structChartPoint].Period;
        }
      }
    }

    public void SetPositionAndShow()
    {
      m_objChartCollection.SetPositionAndShow();
    }

    public void Show()
    {
      m_objChartCollection.Show();
    }

    public Int32 ID
    {
      get
      {
        return m_iID;
      }
    }

    public String Name
    {
      get
      {
        return m_strName;
      }
    }

    public NTChartCollection ChartCollection
    {
      get
      {
        return m_objChartCollection;
      }
    }

    public Int32 GridRowsCount
    {
      get
      {
        return m_iGridRowsCount;
      }
      set
      {
        m_iGridRowsCount = value;
      }
    }

    public Int32 GridColumnsCount
    {
      get
      {
        return m_iGridColumnsCount;
      }
      set
      {
        m_iGridColumnsCount = value;
      }
    }

    public NTMonitor Monitor
    {
      get
      {
        return m_objNTMonitor;
      }
    }
  }
}
