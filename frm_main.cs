﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Globalization;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text.RegularExpressions;

namespace NinjaTraderCustomWorkspaces
{
  public partial class frm_main : Form
  {
    private NTWorkspaceCollection objNTWorkspaceCollection;
    private Hashtable             objHashTableNTMarketData;

    private CultureInfo           objCultureInfo;
    private IFormatProvider       objFormatProvider;

    private NTChartCollection     objDaysHomework;

    public frm_main()
    {
      InitializeComponent();
      Program.Initialize();
    }

    private void Fill_lstInstruments(NTDesktopCollection objNTDesktopCollection)
    {
      lstInstruments.Items.Clear();
      
      foreach (NTDesktop objNTDesktop in objNTDesktopCollection)
        lstInstruments.Items.Add(objNTDesktop.Name);

      if (lstInstruments.Items.Count > 0)
          lstInstruments.SelectedIndex = 0;
    }

    private void Fill_lstInstrumentsDaysHomework(NTChartCollection objNTChartCollection)
    {
      lstInstruments.Items.Clear();

      foreach (NTChart objNTChart in objNTChartCollection)
        lstInstruments.Items.Add(objNTChart.ShortName + " " + objNTChart.Period);
    }

    private void Fill_lstConfigurationInstruments(NTChartCollection objNTChartCollection)
    {
      lstConfigurationInstruments.Items.Clear();

      foreach (NTChart objNTChart in objNTChartCollection)
        lstConfigurationInstruments.Items.Add(objNTChart.ShortName + " " + objNTChart.Period);
    }

    private void Fill_lstConfigurationWorkspaces()
    {
      objNTWorkspaceCollection.Fill();

      lstConfigurationWorkspaces.Items.Clear();
      lstWorkspaces.Items.Clear();

      foreach (NTWorkspace objNTWorkspace in objNTWorkspaceCollection)
      {
        lstConfigurationWorkspaces.Items.Add(objNTWorkspace.Name);
        lstWorkspaces.Items.Add(objNTWorkspace.Name);
      }

      if(lstConfigurationWorkspaces.SelectedItem != null)
          txtConfigurationWorkspace.Text = lstConfigurationWorkspaces.SelectedItem.ToString();

      if (lstConfigurationWorkspaces.Items.Count > 0)
          lstConfigurationWorkspaces.SelectedIndex = 0;
    }

    private void Fill_lstConfigurationDesktops(String strWorkspaceName)
    {
      lstConfigurationDesktops.Items.Clear();

      foreach (NTDesktop objNTDesktop in objNTWorkspaceCollection[strWorkspaceName].DesktopCollection)
        lstConfigurationDesktops.Items.Add(objNTDesktop.Name);

      if (lstConfigurationDesktops.Items.Count > 0)
          lstConfigurationDesktops.SelectedIndex = 0;

      if (lstConfigurationDesktops.Items.Count == 0)
      {
          grdDesktop.Rows.Clear();
          grdDesktop.Columns.Clear();
      }
    }

    private Boolean CheckRemote()
    {
      // ###Solo
      // return false; - не влияет
      // end Solo

      String strCommandResult = Program.SendMessageToNTServer("IsNTProcessRunning");

      if (strCommandResult == "ServerNotAvailable" || strCommandResult == "NO")
          return false;
      else
          return true;
    }

    private void StartWork()
    {
      objCultureInfo    = new CultureInfo("en-Us", false);
      objFormatProvider = objCultureInfo;

      objNTWorkspaceCollection = new NTWorkspaceCollection();
      objDaysHomework          = new NTChartCollection();
      objNTWorkspaceCollection.Fill();
      //Fill_lstConfigurationWorkspaces();

      lstInstruments.Items.Clear();
      lstConfigurationInstruments.Items.Clear();
      grdNTMarketData.Rows.Clear();
      lstConfigurationInstruments.DrawMode = DrawMode.OwnerDrawFixed;

      Fill_lstInstruments(objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection);
      rbnRemote.Enabled = CheckRemote();

      objHashTableNTMarketData = new Hashtable(Program.NTDistinctInstruments.Count);
      
      Int32 i = 0;
      
      foreach (String strInstrument in Program.NTDistinctInstruments)
      {
        DataGridViewTextBoxCell firstCell  = new DataGridViewTextBoxCell();
        DataGridViewTextBoxCell secondCell = new DataGridViewTextBoxCell();
        DataGridViewRow row                = new DataGridViewRow();
        
        firstCell.Value = strInstrument;
        row.Height      = 20;

        row.Cells.AddRange(firstCell, secondCell);
        grdNTMarketData.Rows.Add(row);

        objHashTableNTMarketData.Add(strInstrument, i);
        i++;
      }

      lstInstruments.SelectedIndex = 0;

      //objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection[lstInstruments.SelectedItem.ToString()].ChartCollection.BaseShow_Standart();
      objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection[lstInstruments.SelectedItem.ToString()].ChartCollection.BaseShow_Standart5();

      Program.SelectedMonitorName = btnFujitsu1.Text;
      btnFujitsu1_Click(btnFujitsu1, new EventArgs());

      Fill_lstConfigurationWorkspaces();

    }

    private void btnFill_Click(object sender, EventArgs e)
    {
      StartWork();
    }

    private void rbnStandart_CheckedChanged(object sender, EventArgs e)
    {
      if (lstInstruments.SelectedItem == null) 
          return;

      RadioButton rbn       = (RadioButton)sender;
      String strDesktopName = lstInstruments.SelectedItem.ToString();
      label1.Text           = lstInstruments.SelectedItem.ToString();

      NTDesktopCollection objNTDesktopCollection = null;

      if (rbnMain.Checked)
          objNTDesktopCollection = objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection;

      if (rbnRemote.Checked)
      {
          objNTDesktopCollection = objNTWorkspaceCollection.RemoteBaseWorkspace.DesktopCollection;
          label1.Text += " (R)";
      }

      if (rbn.Checked)
      {
          objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_Standart();
      }
    }

    private void rbnStandart3_CheckedChanged(object sender, EventArgs e)
    {
      if (lstInstruments.SelectedItem == null)
          return;

      RadioButton rbn = (RadioButton)sender;
      String strDesktopName = lstInstruments.SelectedItem.ToString();
      label1.Text = lstInstruments.SelectedItem.ToString();

      NTDesktopCollection objNTDesktopCollection = null;

      if (rbnMain.Checked)
          objNTDesktopCollection = objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection;

      if (rbnRemote.Checked)
      {
          objNTDesktopCollection = objNTWorkspaceCollection.RemoteBaseWorkspace.DesktopCollection;
          label1.Text += " (R)";
      }

      if (rbn.Checked)
      {
          objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_Standart3();
      }
    }

    private void rbnStandart4_CheckedChanged(object sender, EventArgs e)
    {
      if (lstInstruments.SelectedItem == null)
        return;

      RadioButton rbn = (RadioButton)sender;
      String strDesktopName = lstInstruments.SelectedItem.ToString();
      label1.Text = lstInstruments.SelectedItem.ToString();

      NTDesktopCollection objNTDesktopCollection = null;

      if (rbnMain.Checked)
        objNTDesktopCollection = objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection;

      if (rbnRemote.Checked)
      {
        objNTDesktopCollection = objNTWorkspaceCollection.RemoteBaseWorkspace.DesktopCollection;
        label1.Text += " (R)";
      }

      if (rbn.Checked)
      {
        objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_Standart4();
      }
    }

    private void rbnStandart5_CheckedChanged(object sender, EventArgs e)
    {
      if (lstInstruments.SelectedItem == null)
        return;

      RadioButton rbn = (RadioButton)sender;
      String strDesktopName = lstInstruments.SelectedItem.ToString();
      label1.Text = lstInstruments.SelectedItem.ToString();

      NTDesktopCollection objNTDesktopCollection = null;

      if (rbnMain.Checked)
        objNTDesktopCollection = objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection;

      if (rbnRemote.Checked)
      {
        objNTDesktopCollection = objNTWorkspaceCollection.RemoteBaseWorkspace.DesktopCollection;
        label1.Text += " (R)";
      }

      if (rbn.Checked)
      {
        objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_Standart5();
      }
    }

    private void rbnStandart6_CheckedChanged(object sender, EventArgs e)
    {
      if (lstInstruments.SelectedItem == null)
          return;

      RadioButton rbn = (RadioButton)sender;
      String strDesktopName = lstInstruments.SelectedItem.ToString();
      label1.Text = lstInstruments.SelectedItem.ToString();

      NTDesktopCollection objNTDesktopCollection = null;

      if (rbnMain.Checked)
          objNTDesktopCollection = objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection;

      if (rbnRemote.Checked)
      {
          objNTDesktopCollection = objNTWorkspaceCollection.RemoteBaseWorkspace.DesktopCollection;
          label1.Text += " (R)";
      }

      if (rbn.Checked)
      {
          objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_Standart6();
      }
    }

    private void rbn5MinAnd60Min_CheckedChanged(object sender, EventArgs e)
    {
      if (lstInstruments.SelectedItem == null)
          return;

      RadioButton rbn = (RadioButton)sender;
      String strDesktopName = lstInstruments.SelectedItem.ToString();
      label1.Text           = lstInstruments.SelectedItem.ToString();

      NTDesktopCollection objNTDesktopCollection = null;

      if (rbnMain.Checked)
          objNTDesktopCollection = objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection;

      if (rbnRemote.Checked)
      {
          objNTDesktopCollection = objNTWorkspaceCollection.RemoteBaseWorkspace.DesktopCollection;
          label1.Text += " (R)";
      }
      
      if (rbn.Checked)
      {
          objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_5MinAnd60Min();
      }
    }

    private void rbn5MinFullScreen_CheckedChanged(object sender, EventArgs e)
    {
      if (lstInstruments.SelectedItem == null)
          return;

      RadioButton rbn = (RadioButton)sender;
      String strDesktopName = lstInstruments.SelectedItem.ToString();
      label1.Text           = lstInstruments.SelectedItem.ToString();

      NTDesktopCollection objNTDesktopCollection = null;

      if (rbnMain.Checked)
          objNTDesktopCollection = objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection;

      if (rbnRemote.Checked)
      {
          objNTDesktopCollection = objNTWorkspaceCollection.RemoteBaseWorkspace.DesktopCollection;
          label1.Text += " (R)";
      }

      if (rbn.Checked)
      {
          objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_5MinFullScreen();
      }
    }

    private void rbn1440MinFullScreen_CheckedChanged(object sender, EventArgs e)
    {
      if (lstInstruments.SelectedItem == null)
        return;

      RadioButton rbn = (RadioButton)sender;
      String strDesktopName = lstInstruments.SelectedItem.ToString();
      label1.Text = lstInstruments.SelectedItem.ToString();

      NTDesktopCollection objNTDesktopCollection = null;

      if (rbnMain.Checked)
        objNTDesktopCollection = objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection;

      if (rbnRemote.Checked)
      {
        objNTDesktopCollection = objNTWorkspaceCollection.RemoteBaseWorkspace.DesktopCollection;
        label1.Text += " (R)";
      }

      if (rbn.Checked)
      {
        objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_1440MinFullScreen();
      }

    }

    private void rbnWEEKFullScreen_CheckedChanged(object sender, EventArgs e)
    {
      if (lstInstruments.SelectedItem == null)
        return;

      RadioButton rbn = (RadioButton)sender;
      String strDesktopName = lstInstruments.SelectedItem.ToString();
      label1.Text = lstInstruments.SelectedItem.ToString();

      NTDesktopCollection objNTDesktopCollection = null;

      if (rbnMain.Checked)
        objNTDesktopCollection = objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection;

      if (rbnRemote.Checked)
      {
        objNTDesktopCollection = objNTWorkspaceCollection.RemoteBaseWorkspace.DesktopCollection;
        label1.Text += " (R)";
      }

      if (rbn.Checked)
      {
        objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_WEEKFullScreen();
      }
    }

    private void rbn60MinFullScreen_CheckedChanged(object sender, EventArgs e)
    {
      if (lstInstruments.SelectedItem == null)
          return;

      RadioButton rbn = (RadioButton)sender;
      String strDesktopName = lstInstruments.SelectedItem.ToString();
      label1.Text           = lstInstruments.SelectedItem.ToString();

      NTDesktopCollection objNTDesktopCollection = null;

      if (rbnMain.Checked)
          objNTDesktopCollection = objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection;

      if (rbnRemote.Checked)
      {
          objNTDesktopCollection = objNTWorkspaceCollection.RemoteBaseWorkspace.DesktopCollection;
          label1.Text += " (R)";
      }

      if (rbn.Checked)
      {
          objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_60MinFullScreen();
      }
    }

    private void rbn5MinLeftFootRight_CheckedChanged(object sender, EventArgs e)
    {
      if (lstInstruments.SelectedItem == null)
          return;

      RadioButton rbn = (RadioButton)sender;
      String strDesktopName = lstInstruments.SelectedItem.ToString();
      label1.Text           = lstInstruments.SelectedItem.ToString();

      NTDesktopCollection objNTDesktopCollection = null;

      if (rbnMain.Checked)
          objNTDesktopCollection = objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection;

      if (rbnRemote.Checked)
      {
          objNTDesktopCollection = objNTWorkspaceCollection.RemoteBaseWorkspace.DesktopCollection;
          label1.Text += " (R)";
      }

      if (rbn.Checked)
      {
          objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_5MinLeftFootRight();
      }
    }

    private void rbnFootLeft5MinRight_CheckedChanged(object sender, EventArgs e)
    {
      if (lstInstruments.SelectedItem == null)
          return;

      RadioButton rbn = (RadioButton)sender;
      String strDesktopName = lstInstruments.SelectedItem.ToString();
      label1.Text           = lstInstruments.SelectedItem.ToString();

      NTDesktopCollection objNTDesktopCollection = null;

      if (rbnMain.Checked)
          objNTDesktopCollection = objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection;

      if (rbnRemote.Checked)
      {
          objNTDesktopCollection = objNTWorkspaceCollection.RemoteBaseWorkspace.DesktopCollection;
          label1.Text += " (R)";
      }

      if (rbn.Checked)
      {
          objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_FootLeft5MinRight();
      }
    }

    private void frm_main_Load(object sender, EventArgs e)
    {
      try
      {
        // TODO: This line of code loads data into the 'ninjaTraderCustomWorkspacesDataSet1.tbl_Alerts' table. You can move, or remove it, as needed.
        this.tbl_AlertsTableAdapter.Fill(this.ninjaTraderCustomWorkspacesDataSet1.tbl_Alerts);
        // TODO: This line of code loads data into the 'ninjaTraderCustomWorkspacesDataSet.tbl_InstrumentInfo' table. You can move, or remove it, as needed.
        this.tbl_InstrumentInfoTableAdapter.Fill(this.ninjaTraderCustomWorkspacesDataSet.tbl_InstrumentInfo);
        // TODO: This line of code loads data into the 'ninjaTraderCustomWorkspacesDataSet2.tbl_ImportantNews' table. You can move, or remove it, as needed.
        this.tbl_ImportantNewsTableAdapter.Fill(this.ninjaTraderCustomWorkspacesDataSet2.tbl_ImportantNews);

        if (Program.IsNTMainProccessRuning())
        {
            Process CurrentProcess = Process.GetCurrentProcess();
            Process NTProcess = Process.GetProcessById(Program.NTProcessID);

            if (NTProcess.PriorityClass != ProcessPriorityClass.Normal)
                NTProcess.PriorityClass = ProcessPriorityClass.Normal;
        }

        StartWork();

        grdDesktop.ReadOnly = true;
        grdDesktop.SelectionMode = DataGridViewSelectionMode.RowHeaderSelect;
        grdDesktop.RowsDefaultCellStyle.SelectionBackColor = Color.LightGray;

        this.Size = Program.FormSmallSize;
        this.Text = DateTime.Now.ToLongDateString() + ", " + DateTime.Now.ToString("dddd") + ".";

        LoadATRInfo();
        SetToolTips();
      }
      catch (Exception ex)
      {
        Application.Exit();
      }

    }

    private void SetToolTips()
    {
      //bntDZListForCode
      //toolTip1.SetToolTip(this.button1, "My button1");
      //toolTip1.SetToolTip(this.checkBox1, "My checkBox1");
      ToolTip toolTip1 = new ToolTip();
      String strText   = "Для экспорта – Экспорт, в файл, с табуляцией windows,"    + "\r\n "   +
                         "поля = тема+заметки, файл с заменой, имя = exportDZ.TXT." + "\r\n "   +
                         "Файл положить в D:\\__АЛГОРИТМ\\AUTOSCREENS"              + "\r\n"    +
                         "файл результата = ResultDZListForCode.txt";

      toolTip1.SetToolTip(this.bntDZListForCode, strText);
    }

    private void LoadATRInfo()
    {
      if (!File.Exists(Program.ATRFileName))
      {
        txtATR.Text = "File " + Program.ATRFileName + " not found in program's directory.";
        return;
      }

      String strValue = String.Empty;

      using (StreamReader sr = File.OpenText(Program.ATRFileName))
      {
        strValue = sr.ReadToEnd();
      }

      txtATR.Text = strValue;
    }

    private void btnAddRow_Click(object sender, EventArgs e)
    {
      if (grdDesktop.Columns.Count == 0)
      {
          DataGridViewTextBoxColumn titleColumn = new DataGridViewTextBoxColumn();
          titleColumn.AutoSizeMode              = DataGridViewAutoSizeColumnMode.Fill;
          
          grdDesktop.Columns.Add(titleColumn);
      }
      else
      {
          DataGridViewTextBoxCell firstCell_grdUpAsk = new DataGridViewTextBoxCell();
          DataGridViewRow row_grdUpAsk               = new DataGridViewRow();

          row_grdUpAsk.Cells.Add(firstCell_grdUpAsk);
          grdDesktop.Rows.Add(row_grdUpAsk);
      }

      foreach (DataGridViewRow objDataGridViewRow in grdDesktop.Rows)
        objDataGridViewRow.Height = 30;
    }

    private void btnDeleteRow_Click(object sender, EventArgs e)
    {
      if (grdDesktop.Rows.Count > 1)
          grdDesktop.Rows.RemoveAt(grdDesktop.Rows[0].Index);
    }

    private void btnAddColomn_Click(object sender, EventArgs e)
    {
      DataGridViewTextBoxColumn titleColumn = new DataGridViewTextBoxColumn();
      titleColumn.AutoSizeMode              = DataGridViewAutoSizeColumnMode.Fill;

      grdDesktop.Columns.Add(titleColumn);

      foreach (DataGridViewRow objDataGridViewRow in grdDesktop.Rows)
        objDataGridViewRow.Height = 30;

    }

    private void frm_main_FormClosed(object sender, FormClosedEventArgs e)
    {
      try
      {
        objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection.HideChartsOnFirstScreen();
        objNTWorkspaceCollection.RemoteBaseWorkspace.DesktopCollection.HideChartsOnFirstScreen();
        Program.SetNTProcessPriority(ProcessPriorityClass.Idle);

        //if (!Program.NTUnSubscribe())
        //    Program.ShowErrorMessage("Error during UnSubscribe NinjaTrader.");
      }
      catch (Exception Ex)
      {
      }
    }

    private void btnDeleteColomn_Click(object sender, EventArgs e)
    {
      if (grdDesktop.Columns.Count > 1)
          grdDesktop.Columns.RemoveAt(grdDesktop.Columns[grdDesktop.Columns.Count-1].Index);
    }

    private void grdDesktop_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
    {
      if (e.Button == System.Windows.Forms.MouseButtons.Left)
      {
          if (lstConfigurationInstruments.SelectedItem               != null && 
              grdDesktop.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == null)
          {
              for (int i = 0; i < grdDesktop.Rows.Count; i++)
                for (int j = 0; j < grdDesktop.Columns.Count; j++)
                  if (grdDesktop.Rows[i].Cells[j].Value != null)
                      if (lstConfigurationInstruments.SelectedItem.ToString() == grdDesktop.Rows[i].Cells[j].Value.ToString())
                          return;

              grdDesktop.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = lstConfigurationInstruments.SelectedItem.ToString();              
          }
      }

      if (e.Button == System.Windows.Forms.MouseButtons.Right)
          grdDesktop.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = null;

    }

    private void btnClear_Click(object sender, EventArgs e)
    {
      foreach (DataGridViewRow objRow in grdDesktop.Rows)
        foreach (DataGridViewTextBoxCell objCell in objRow.Cells)
          objCell.Value = null;
    }

    private void lstConfigurationInstruments_DrawItem(object sender, DrawItemEventArgs e)
    {
      String strText = ((ListBox)sender).Items[e.Index].ToString();
      Color color    = e.ForeColor;
      Font font      = new Font(Font.FontFamily, Font.Size, Font.Style, Font.Unit);

      if (strText.IndexOf("5 Min") != -1   || strText.IndexOf("2 Min") != -1
         || strText.IndexOf("1 Min") != -1 || strText.IndexOf("1440 Min") != -1)
      {
          color = Color.Red;
          font = new Font(Font.FontFamily, Font.Size, FontStyle.Bold, Font.Unit);
      }

      SolidBrush brBack = new SolidBrush(e.BackColor);
      SolidBrush brText = new SolidBrush(color);

      e.Graphics.FillRectangle(brBack, e.Bounds);
      e.Graphics.DrawString(strText, font, brText, e.Bounds);
    }

    private void tabControl_Selected(object sender, TabControlEventArgs e)
    {
      if (/* e.TabPageIndex == 0*/ e.TabPage.Name == "tabPageBase" || e.TabPage.Name == "tabPageScreens")
          this.Size = Program.FormSmallSize;
      else
          this.Size = Program.FormLargeSize;
    }

    private void btnHide_Click(object sender, EventArgs e)
    {
      objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection.HideChartsOnFirstScreen();
      objNTWorkspaceCollection.RemoteBaseWorkspace.DesktopCollection.HideChartsOnFirstScreen();
    }

    private void frm_main_Deactivate(object sender, EventArgs e)
    {
      //if (this.WindowState == FormWindowState.Minimized)
      //{
      //    objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection.HideChartsOnFirstScreen();
      //    objNTWorkspaceCollection.RemoteBaseWorkspace.DesktopCollection.HideChartsOnFirstScreen();
      //    Program.SetNTProcessPriority(ProcessPriorityClass.Idle);
      //}
    }

    private void rbnFootRight5MinLeftOneScreen_CheckedChanged(object sender, EventArgs e)
    {
      if (lstInstruments.SelectedItem == null)
          return;

      RadioButton rbn = (RadioButton)sender;
      String strDesktopName = lstInstruments.SelectedItem.ToString();
      label1.Text           = lstInstruments.SelectedItem.ToString();

      NTDesktopCollection objNTDesktopCollection = null;

      if (rbnMain.Checked)
          objNTDesktopCollection = objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection;

      if (rbnRemote.Checked)
      {
          objNTDesktopCollection = objNTWorkspaceCollection.RemoteBaseWorkspace.DesktopCollection;
          label1.Text += " (R)";
      }

      if (rbn.Checked)
      {
          objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_FootRight5MinLeftOneScreen();
      }
    }

    private void rbn5Min60minLeftFootRightOneScreen_CheckedChanged(object sender, EventArgs e)
    {
      if (lstInstruments.SelectedItem == null)
          return;

      RadioButton rbn       = (RadioButton)sender;
      String strDesktopName = lstInstruments.SelectedItem.ToString();
      label1.Text           = lstInstruments.SelectedItem.ToString();

      NTDesktopCollection objNTDesktopCollection = null;

      if (rbnMain.Checked)
          objNTDesktopCollection = objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection;

      if (rbnRemote.Checked)
      {
          objNTDesktopCollection = objNTWorkspaceCollection.RemoteBaseWorkspace.DesktopCollection;
          label1.Text += " (R)";
      }

      if (rbn.Checked)
      {
          //objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_FootLeft5MinRightOneScreen();
        objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_5Min60minLeftFootRightOneScreen();
      }
    }

    private void rbnFullScreens60MinLeft5MinRight_CheckedChanged(object sender, EventArgs e)
    {
      if (lstInstruments.SelectedItem == null)
          return;

      RadioButton rbn = (RadioButton)sender;
      String strDesktopName = lstInstruments.SelectedItem.ToString();
      label1.Text           = lstInstruments.SelectedItem.ToString();

      NTDesktopCollection objNTDesktopCollection = null;

      if (rbnMain.Checked)
          objNTDesktopCollection = objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection;

      if (rbnRemote.Checked)
      {
          objNTDesktopCollection = objNTWorkspaceCollection.RemoteBaseWorkspace.DesktopCollection;
          label1.Text += " (R)";
      }

      if (rbn.Checked)
      {
          objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_FullScreens60MinLeft5MinRight();
      }
    }

    private void btnViewDesktop_Click(object sender, EventArgs e)
    {
      NTDesktop objNTViewDesktop = new NTDesktop(-1, "ViewDesktop", Program.SelectedMonitor, grdDesktop, 
                                                 objNTWorkspaceCollection.MainCharts, 
                                                 objNTWorkspaceCollection.RemoteCharts);

      objNTViewDesktop.DisposeChartsOnScreen(Program.SelectedMonitor.ScreenBounds);
      objNTViewDesktop.Show();
    }

    private void tbl_InstrumentInfoBindingNavigatorSaveItem_Click(object sender, EventArgs e)
    {
      this.Validate();
      this.tbl_InstrumentInfoBindingSource.EndEdit();
      this.tableAdapterManager.UpdateAll(this.ninjaTraderCustomWorkspacesDataSet);
      this.tbl_InstrumentInfoTableAdapter.Fill(this.ninjaTraderCustomWorkspacesDataSet.tbl_InstrumentInfo);

    }

    private void toolStripButton7_Click(object sender, EventArgs e)
    {
      this.Validate();
      this.tbl_AlertsBindingSource.EndEdit();
      this.tableAdapterManager1.UpdateAll(this.ninjaTraderCustomWorkspacesDataSet1);
      this.tbl_AlertsTableAdapter.Fill(this.ninjaTraderCustomWorkspacesDataSet1.tbl_Alerts);
    }

    private void tmCurrentTime_Tick(object sender, EventArgs e)
    {
      lblCurrentTime.Text = DateTime.Now.ToLongTimeString();

      if (DateTime.Now.Second == 0)
      {
          DateTime dtCurrentPlus = (new DateTime(
                                                  DateTime.Now.Year, 
                                                  DateTime.Now.Month, 
                                                  DateTime.Now.Day,
                                                  DateTime.Now.Hour, 
                                                  DateTime.Now.Minute,
                                                  DateTime.Now.Second, 
                                                  0)
                                    ).AddMinutes((Double)nmrMinutesBefore.Value);

          for (int i = 0; i < grdImportantNews.Rows.Count; i++)
          {
            DateTime dt = DateTime.Parse(grdImportantNews.Rows[i].Cells[1].Value.ToString());

            if (dtCurrentPlus == dt)
            {
                ShowAlertWindow("NEWS !!! " + dt.TimeOfDay.ToString() + "  " + grdImportantNews.Rows[i].Cells[2].Value.ToString());
            }
          }
      }
    }

    /*
     можно сделать более быстрый механизм с помощью массива хеш-таблиц
     при каждом дублировании инструмента заводить новую хеш-таблицу и добавлять ее в массив
     а выставление значения сведется к простому пробеганию по массиву хеш-таблиц
     не думаю, что нужны будут одновременно больше двух-трех точек алерта на инструмент
     возможная проблема - потеря индексации при сортировке грида
    */
    //private void tmNTMarketData_Tick(object sender, EventArgs e)
    //{
    //  Boolean blnIsAlert = false;

    //  foreach (String strInstrument in Program.NTDistinctInstruments)
    //  {
    //    if (!objHashTableNTMarketData.ContainsKey(strInstrument))
    //        continue;
  
    //    Double dblMarketData           = Program.objNTClient.MarketData(strInstrument, 0);
    //    Int32 igrdNTMarketDataRowIndex = (Int32)objHashTableNTMarketData[strInstrument];
        
    //    grdNTMarketData.Rows[igrdNTMarketDataRowIndex].Cells[1].Value = dblMarketData.ToString();

    //    for (int i = 0; i < grdAlerts.Rows.Count; i++)
    //    {
    //      if (grdAlerts.Rows[i].Cells[1].Value.ToString() == strInstrument)
    //          grdAlerts.Rows[i].Cells[4].Value = dblMarketData.ToString();

    //      if (grdAlerts.Rows[i].Cells[1].Value == null || grdAlerts.Rows[i].Cells[2].Value == null ||
    //          grdAlerts.Rows[i].Cells[3].Value == null || grdAlerts.Rows[i].Cells[4].Value == null)
    //          continue;

    //      if (strInstrument != grdAlerts.Rows[i].Cells[1].Value.ToString())
    //          continue;

    //      DataGridViewCheckBoxCell chkYN = (DataGridViewCheckBoxCell)grdAlerts.Rows[i].Cells[5];
    //      Double dblAlertPrice           = 0.0;
    //      Double dblSetPrice             = 0.0;
    //      String[] arrAlertPrice         = grdAlerts.Rows[i].Cells[3].Value.ToString().Split(Char.Parse("-"));

    //      try
    //      {
    //        String strAlertPrice   = arrAlertPrice[0];
    //        String strSetPrice     = grdAlerts.Rows[i].Cells[2].Value.ToString().Split(Char.Parse("-"))[0];

    //        dblAlertPrice          = Convert.ToDouble(strAlertPrice.Trim().Replace(',', '.'), objFormatProvider);
    //        dblSetPrice            = Convert.ToDouble(strSetPrice.Trim().Replace(',', '.'), objFormatProvider);
    //      }
    //      catch (Exception Ex) 
    //      { }

    //      if (dblAlertPrice > dblSetPrice)
    //      {
    //          if (dblMarketData >= dblAlertPrice)
    //          {
    //              chkYN.Value = (object)true;

    //              if (arrAlertPrice.Length == 1)
    //              {
    //                  grdAlerts.Rows[i].Cells[3].Value = dblAlertPrice.ToString() + " - " + DateTime.Now.ToShortTimeString();
    //                  toolStripButton7_Click(this, new EventArgs());

    //                  if (chkAlertMessage.Checked)
    //                      ShowAlertWindow("Alert! : [" + strInstrument + "] " + DateTime.Now.ToShortTimeString());
    //              }
    //          }
    //      }

    //      if (dblAlertPrice < dblSetPrice)
    //      {
    //          if (dblMarketData <= dblAlertPrice)
    //          {
    //              chkYN.Value = (object)true;

    //              if (arrAlertPrice.Length == 1)
    //              {
    //                  grdAlerts.Rows[i].Cells[3].Value = dblAlertPrice.ToString() + " - " + DateTime.Now.ToShortTimeString();
    //                  toolStripButton7_Click(this, new EventArgs());
                      
    //                  if (chkAlertMessage.Checked)
    //                      ShowAlertWindow("Alert! : [" + strInstrument + "] " + DateTime.Now.ToShortTimeString());
    //              }
    //          }
    //      }

    //      if (Convert.ToInt32(grdAlerts.Rows[i].Cells[0].Value.ToString()) < 0)
    //          continue;

    //      if ((Boolean)chkYN.Value == true)
    //          blnIsAlert = true;
    //    }
    //  }

    //  if (!blnIsAlert && tmAlert.Enabled)
    //      btnAlert.BackColor = Color.Transparent;

    //  tmAlert.Enabled = blnIsAlert;   
    //}

    private void grdAlertsDataGridView_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
    {
      if (grdAlerts.Rows[e.RowIndex].Cells[5].Value == null)
          grdAlerts.Rows[e.RowIndex].Cells[5].Value = (object)false;
    }

    private void grdAlerts_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
    {
      grdAlerts.Sort(grdAlerts.Columns[1], ListSortDirection.Ascending);
    }

    private void toolStripButton1_Click(object sender, EventArgs e)
    {
      Int32 igrdAlertsDataGridView_RowIndex = grdAlerts.SelectedCells[0].RowIndex;
      Int32 igrdNTMarketData_RowIndex       = grdNTMarketData.SelectedCells[0].RowIndex;

      if (grdNTMarketData.Rows[igrdNTMarketData_RowIndex].Cells[1].Value == null)
          return;

      grdAlerts.Rows[igrdAlertsDataGridView_RowIndex].Cells[1].Value = grdNTMarketData.Rows[igrdNTMarketData_RowIndex].Cells[0].Value;
      grdAlerts.Rows[igrdAlertsDataGridView_RowIndex].Cells[2].Value = grdNTMarketData.Rows[igrdNTMarketData_RowIndex].Cells[1].Value.ToString() + " - " + DateTime.Now.ToShortTimeString();
      grdAlerts.Rows[igrdAlertsDataGridView_RowIndex].Cells[3].Value = null;

      DataGridViewCell cell      = this.grdAlerts.Rows[igrdAlertsDataGridView_RowIndex].Cells[3];
      this.grdAlerts.CurrentCell = cell;
      this.grdAlerts.BeginEdit(false);
    }

    private void toolStripButton2_Click(object sender, EventArgs e)
    {

    }

    private void btnAlert_Click(object sender, EventArgs e)
    {
      tabControl.SelectedIndex = 2;
    }

    private void tmAlert_Tick(object sender, EventArgs e)
    {
      if (btnAlert.BackColor == Color.Transparent)
      {
          btnAlert.BackColor = Color.Orange;
      }
      else
      {
          btnAlert.BackColor = Color.Transparent;
      }
    }

    private void button1_Click(object sender, EventArgs e)
    {
    }

    private void grdAlerts_MouseDoubleClick(object sender, MouseEventArgs e)
    {
      DataGridView objGrd  = (DataGridView)sender;
      String strInstrument = objGrd.CurrentCell.Value.ToString();

      if (objGrd.CurrentCell.ColumnIndex != 1)
          return;

      if (!objHashTableNTMarketData.ContainsKey(strInstrument))
          return;

      objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection[strInstrument].ChartCollection.BaseShow_Standart();      
    }

    private void ShowAlertWindow(String strMessage)
    {
      Form frmAlert = new frm_Alert(strMessage);
      frmAlert.Show(this);
    }

    private void ShowDzWindow()
    {
      Form frmAMGDz = new frm_AMGDz();
      frmAMGDz.Show(this);
    }


    private void rbnMain_CheckedChanged(object sender, EventArgs e)
    {
      RadioButton objRadioButton = (RadioButton)sender;

      if (objRadioButton.Checked)
          Fill_lstInstruments(objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection);
    }

    private void rbnRemote_CheckedChanged(object sender, EventArgs e)
    {
      RadioButton objRadioButton = (RadioButton)sender;

      if (objRadioButton.Checked)
          Fill_lstInstruments(objNTWorkspaceCollection.RemoteBaseWorkspace.DesktopCollection);

      rbn5Min60minLeftFootRightOneScreen.Checked = true;
    }

    private void btnFujitsu1_Click(object sender, EventArgs e)
    {
      MonitorButtonsClick((Button)sender);
    }

    private void btnFujitsu2_Click(object sender, EventArgs e)
    {
      MonitorButtonsClick((Button)sender);
    }

    private void btnLenovo1_Click(object sender, EventArgs e)
    {
      MonitorButtonsClick((Button)sender);
    }

    private void btnLenovo2_Click(object sender, EventArgs e)
    {
      MonitorButtonsClick((Button)sender);
    }

    private void MonitorButtonsClick(Button objButton)
    {
      Color clrNotSelected        = Color.DarkGray;
      Color clrSelected           = Color.DodgerBlue;

      btnFujitsu1.BackColor       = clrNotSelected;
      btnFujitsu2.BackColor       = clrNotSelected;
      btnLenovo1.BackColor        = clrNotSelected;
      btnLenovo2.BackColor        = clrNotSelected;
      objButton.BackColor         = clrSelected;

      if (Program.SelectedMonitor.ChartLocation != Program.MonitorCollection[objButton.Text].ChartLocation)
      {
          grdDesktop.Rows.Clear();
          grdDesktop.Columns.Clear();
      }

      Program.SelectedMonitorName = objButton.Text;

      if (Program.SelectedMonitor.ChartLocation == Enums.ChartLocation.Main)
      {
          Fill_lstConfigurationInstruments(objNTWorkspaceCollection.MainCharts);
          lblChartLocation.Text = "Main";
      }

      if (Program.SelectedMonitor.ChartLocation == Enums.ChartLocation.Remote)
      {
          Fill_lstConfigurationInstruments(objNTWorkspaceCollection.RemoteCharts);
          lblChartLocation.Text = "Remote";
      }
    }

    private void btnDesktopDeleteRowsAndCells_Click(object sender, EventArgs e)
    {
      grdDesktop.Rows.Clear();
      grdDesktop.Columns.Clear();
    }

    private void btnNewWorkspace_Click(object sender, EventArgs e)
    {
      NTWorkspace objNTWorkspace = new NTWorkspace();
      
      objNTWorkspace.Create(txtConfigurationWorkspace.Text);
      Fill_lstConfigurationWorkspaces();
    }

    private void btnUpdateWorkspace_Click(object sender, EventArgs e)
    {
      if (lstConfigurationWorkspaces.Items.Count == 0)
          return;

      String strSelectedWorkspaceName = lstConfigurationWorkspaces.SelectedItem.ToString();
      NTWorkspace objNTWorkspace      = objNTWorkspaceCollection[strSelectedWorkspaceName];

      if (objNTWorkspace == null)
          return;

      objNTWorkspace.Update(txtConfigurationWorkspace.Text);

      Fill_lstConfigurationWorkspaces();
    }

    private void btnDeleteWorkspace_Click(object sender, EventArgs e)
    {
      if (lstConfigurationWorkspaces.Items.Count == 0)
          return;
      
      String strSelectedWorkspaceName = lstConfigurationWorkspaces.SelectedItem.ToString();
      NTWorkspace objNTWorkspace      = objNTWorkspaceCollection[strSelectedWorkspaceName];

      if (objNTWorkspace == null)
          return;

      objNTWorkspace.Delete();

      Fill_lstConfigurationWorkspaces();
      txtConfigurationWorkspace.Text = String.Empty;
    }

    private void lstConfigurationWorkspaces_Click(object sender, EventArgs e)
    {
      //ListBox objListBox             = (ListBox)sender;
      //txtConfigurationWorkspace.Text = objListBox.SelectedItem.ToString();
    }

    private void btnSaveDesktop_Click(object sender, EventArgs e)
    {
      if (lstConfigurationWorkspaces.SelectedItem == null)
          return;

      for (int i = 0; i < grdDesktop.Rows.Count; i++)
      {
        for (int j = 0; j < grdDesktop.Columns.Count; j++)
        {
          if (grdDesktop.Rows[i].Cells[j].Value == null)
              return;
          else
              if (grdDesktop.Rows[i].Cells[j].Value.ToString() == String.Empty)
                  return;
        }
      }

      String strSelectedWorkspaceName = lstConfigurationWorkspaces.SelectedItem.ToString();
      Int32 iSelectedWorkspaceID      = objNTWorkspaceCollection[strSelectedWorkspaceName].ID;
      String strSelectedDesktopName   = lstConfigurationWorkspaces.SelectedItem.ToString() + "_" + Program.SelectedMonitor.Name;
      NTDesktop objNTDesktop          = objNTWorkspaceCollection[strSelectedWorkspaceName].DesktopCollection[strSelectedDesktopName];

      if (objNTDesktop != null)
      {
          objNTDesktop.Delete();
          objNTWorkspaceCollection[strSelectedWorkspaceName].DesktopCollection.Remove(objNTDesktop);
      }

      objNTDesktop = new NTDesktop(iSelectedWorkspaceID, strSelectedDesktopName, Program.SelectedMonitor, grdDesktop,
                                   objNTWorkspaceCollection.MainCharts,
                                   objNTWorkspaceCollection.RemoteCharts);

      objNTDesktop.DisposeChartsOnScreen(Program.SelectedMonitor.ScreenBounds);
      objNTDesktop.Create();

      objNTWorkspaceCollection[strSelectedWorkspaceName].DesktopCollection.Add(objNTDesktop);
      Fill_lstConfigurationDesktops(strSelectedWorkspaceName);
      objNTDesktop.DisposeChartsOnGrid(grdDesktop);
      objNTDesktop.Show();
    }

    private void btnDeleteDesktop_Click(object sender, EventArgs e)
    {
      if (lstConfigurationDesktops.SelectedItem == null)
          return;

      String strSelectedWorkspaceName = lstConfigurationWorkspaces.SelectedItem.ToString();
      String strSelectedDesktopName   = lstConfigurationDesktops.SelectedItem.ToString();
      NTDesktop objNTDesktop          = objNTWorkspaceCollection[strSelectedWorkspaceName].DesktopCollection[strSelectedDesktopName];

      if (objNTDesktop == null)
          return;

      objNTDesktop.Delete();
      objNTWorkspaceCollection[strSelectedWorkspaceName].DesktopCollection.Remove(objNTDesktop);
      Fill_lstConfigurationDesktops(strSelectedWorkspaceName);
    }

    private void lstConfigurationWorkspaces_SelectedIndexChanged(object sender, EventArgs e)
    {
      ListBox objListBox              = (ListBox)sender;
      String strSelectedWorkspaceName = objListBox.SelectedItem.ToString();
      txtConfigurationWorkspace.Text  = strSelectedWorkspaceName;

      Fill_lstConfigurationDesktops(strSelectedWorkspaceName);
    }

    private void lstInstruments_SelectedIndexChanged(object sender, EventArgs e)
    {
      ListBox lst           = (ListBox)sender;
      String strDesktopName = lst.SelectedItem.ToString();
      label1.Text           = lst.SelectedItem.ToString();

      if (chkHomework.Checked)
      {
          if (objDaysHomework[strDesktopName].Location == Enums.ChartLocation.Main)
          {
              objDaysHomework[strDesktopName].SetPositionFullScreen(Program.ScreensResolution.MainSecondScreenBounds);
              objDaysHomework[strDesktopName].Show();
          }

          if (objDaysHomework[strDesktopName].Location == Enums.ChartLocation.Remote)
          {
              objDaysHomework[strDesktopName].SetPositionFullScreen(Program.ScreensResolution.RemoteSecondScreenBounds);
              objDaysHomework[strDesktopName].Show();
          }

          return;
      }

      NTDesktopCollection objNTDesktopCollection = null;

      if (rbnMain.Checked)
          objNTDesktopCollection = objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection;

      if (rbnRemote.Checked)
      {
          objNTDesktopCollection = objNTWorkspaceCollection.RemoteBaseWorkspace.DesktopCollection;
          label1.Text += " (R)";
      }

      if (rbnStandart.Checked)
      {
          objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_Standart();
          this.Activate();
          return;
      }

      if (rbnStandart2.Checked)
      {
          objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_Standart2();
          this.Activate();
          return;
      }

      if (rbnStandart3.Checked)
      {
        objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_Standart3();
        this.Activate();
        return;
      }

      if (rbnStandart4.Checked)
      {
        objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_Standart4();
        this.Activate();
        return;
      }

      if (rbnStandart5.Checked)
      {
        objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_Standart5();
        this.Activate();
        return;
      }

      if (rbnStandart6.Checked)
      {
        objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_Standart6();
        this.Activate();
        return;
      }

      if (rbn60MinAnd1440Min.Checked)
      {
        objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_1440And60FullScreen();
        this.Activate();
        return;
      }

      if (rbn5MinAnd60Min.Checked)
      {
          objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_5MinAnd60Min();
          this.Activate();
          return;
      }

      if (rbn5MinFullScreen.Checked)
      {
          objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_5MinFullScreen();
          this.Activate();
          return;
      }

      if (rbn1440MinFullScreen.Checked)
      {
        objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_1440MinFullScreen();
        this.Activate();
        return;
      }

      if (rbnWEEKFullScreen.Checked)
      {
        objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_WEEKFullScreen();
        this.Activate();
        return;
      }

      if (rbn60MinFullScreen.Checked)
      {
          objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_60MinFullScreen();
          this.Activate();
          return;
      }

      if (rbn5MinLeftFootRight.Checked)
      {
          objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_5MinLeftFootRight();
          this.Activate();
          return;
      }

      if (rbnFootLeft5MinRight.Checked)
      {
          objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_FootLeft5MinRight();
          this.Activate();
          return;
      }

      if (rbnFootRight5MinLeftOneScreen.Checked)
      {
          objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_FootRight5MinLeftOneScreen();
          this.Activate();
          return;
      }

      if (rbn5Min60minLeftFootRightOneScreen.Checked)
      {
          //objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_FootLeft5MinRightOneScreen();
          objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_5Min60minLeftFootRightOneScreen();

          this.Activate();
          return;
      }

      if (rbnFullScreens60MinLeft5MinRight.Checked)
      {
          objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_FullScreens60MinLeft5MinRight();
          this.Activate();
          return;
      }
      
      if (rbnFullScreens60MinRight5MinLeft.Checked)
      {
          objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_FullScreens60MinRight5MinLeft();
          this.Activate();
          return;
      }
    }

    private void lstConfigurationDesktops_SelectedIndexChanged(object sender, EventArgs e)
    {
      String strSelectedWorkspaceName = lstConfigurationWorkspaces.SelectedItem.ToString();
      String strSelectedDesktopName   = lstConfigurationDesktops.SelectedItem.ToString();
      NTDesktop objNTDesktop          = objNTWorkspaceCollection[strSelectedWorkspaceName].DesktopCollection[strSelectedDesktopName];

      if (objNTDesktop == null)
          return;

      switch (objNTDesktop.Name.Split('_')[1])
      {
        case "Fujitsu 1": MonitorButtonsClick(btnFujitsu1); break;
        case "Fujitsu 2": MonitorButtonsClick(btnFujitsu2); break;
        case "Lenovo 1": MonitorButtonsClick(btnLenovo1); break;
        case "Lenovo 2": MonitorButtonsClick(btnLenovo2); break;
      }

      objNTDesktop.DisposeChartsOnGrid(grdDesktop);
    }

    private void btnShowSelectedDesktop_Click(object sender, EventArgs e)
    {
      if (lstConfigurationWorkspaces.SelectedItem == null)
          return;

      String strSelectedWorkspaceName = lstConfigurationWorkspaces.SelectedItem.ToString();
      String strSelectedDesktopName   = lstConfigurationDesktops.SelectedItem.ToString();
      NTDesktop objNTDesktop          = objNTWorkspaceCollection[strSelectedWorkspaceName].DesktopCollection[strSelectedDesktopName];

      if (objNTDesktop == null)
          return;

      objNTDesktop.SetPositionAndShow();
    }

    private void btnShowSelectedWorkspace_Click(object sender, EventArgs e)
    {
      if (lstConfigurationWorkspaces.SelectedItem == null)
          return;

      String strSelectedWorkspaceName = lstConfigurationWorkspaces.SelectedItem.ToString();
      NTWorkspace objNTWorkspace      = objNTWorkspaceCollection[strSelectedWorkspaceName];

      if (objNTWorkspace == null)
          return;

      objNTWorkspace.SetPositionAndShow();
    }

    private void lstWorkspaces_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (lstWorkspaces.SelectedItem == null)
          return;

      String strSelectedWorkspaceName = lstWorkspaces.SelectedItem.ToString();
      NTWorkspace objNTWorkspace      = objNTWorkspaceCollection[strSelectedWorkspaceName];

      if (objNTWorkspace == null)
          return;

      if (objNTWorkspace.DesktopCollection.Count > 0)
          objNTWorkspace.SetPositionAndShow();
    }

    private void rbnFullScreens60MinRight5MinLeft_CheckedChanged(object sender, EventArgs e)
    {
      if (lstInstruments.SelectedItem == null)
          return;

      RadioButton rbn = (RadioButton)sender;
      String strDesktopName = lstInstruments.SelectedItem.ToString();
      label1.Text           = lstInstruments.SelectedItem.ToString();

      NTDesktopCollection objNTDesktopCollection = null;

      if (rbnMain.Checked)
          objNTDesktopCollection = objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection;

      if (rbnRemote.Checked)
      {
          objNTDesktopCollection = objNTWorkspaceCollection.RemoteBaseWorkspace.DesktopCollection;
          label1.Text += " (R)";
      }

      if (rbn.Checked)
      {
          objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_FullScreens60MinRight5MinLeft();
      }
    }

    private void rbnNTPrioritetLow_CheckedChanged(object sender, EventArgs e)
    {
      RadioButton rbn = (RadioButton)sender;

      if (rbn.Checked)
          Program.SetNTProcessPriority(ProcessPriorityClass.Idle);
    }

    private void rbnNTPrioritetNormal_CheckedChanged(object sender, EventArgs e)
    {
      RadioButton rbn = (RadioButton)sender;

      if (rbn.Checked)
          Program.SetNTProcessPriority(ProcessPriorityClass.Normal);
    }

    private void tbl_ImportantNewsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
    {
      this.Validate();
      this.tbl_ImportantNewsBindingSource.EndEdit();
      this.tableAdapterManager2.UpdateAll(this.ninjaTraderCustomWorkspacesDataSet2);
      this.tbl_ImportantNewsTableAdapter.Fill(this.ninjaTraderCustomWorkspacesDataSet2.tbl_ImportantNews);

    }

    private void toolStripButton8_Click(object sender, EventArgs e)
    {
      Int32 igrdImportantNews_RowIndex = grdImportantNews.SelectedCells[0].RowIndex;

      DateTime dtNewsDateTime = new DateTime(dtpicNewsDate.Value.Year,
                                             dtpicNewsDate.Value.Month,
                                             dtpicNewsDate.Value.Day,
                                             dtpicNewsTime.Value.Hour,
                                             dtpicNewsTime.Value.Minute,
                                             0,
                                             0);

      DateTime dt = DateTime.Parse(dtNewsDateTime.ToString());

      grdImportantNews.Rows[igrdImportantNews_RowIndex].Cells[1].Value = dtNewsDateTime.ToString();

      DataGridViewCell cell             = this.grdImportantNews.Rows[igrdImportantNews_RowIndex].Cells[2];
      this.grdImportantNews.CurrentCell = cell;
      this.grdImportantNews.BeginEdit(false);

    }

    private void nmrMinutesBefore_ValueChanged(object sender, EventArgs e)
    {
      NumericUpDown nmr = (NumericUpDown)sender;

      if (nmr.Value < 1)
          nmr.Value = 1;

    }

    private void rbnStandartFor60List_CheckedChanged(object sender, EventArgs e)
    {
      if (lstInstruments.SelectedItem == null)
          return;

      RadioButton rbn       = (RadioButton)sender;
      String strDesktopName = lstInstruments.SelectedItem.ToString();
      label1.Text           = lstInstruments.SelectedItem.ToString();

      NTDesktopCollection objNTDesktopCollection = null;

      if (rbnMain.Checked)
          objNTDesktopCollection = objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection;
      else
          return;

      if (rbn.Checked)
      {
          objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_1440And60FullScreen();
      }

    }

    private void rbnStandart2_CheckedChanged(object sender, EventArgs e)
    {
      if (lstInstruments.SelectedItem == null)
          return;

      RadioButton rbn       = (RadioButton)sender;
      String strDesktopName = lstInstruments.SelectedItem.ToString();
      label1.Text           = lstInstruments.SelectedItem.ToString();

      NTDesktopCollection objNTDesktopCollection = null;

      if (rbnMain.Checked)
          objNTDesktopCollection = objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection;

      if (rbnRemote.Checked)
      {
          objNTDesktopCollection = objNTWorkspaceCollection.RemoteBaseWorkspace.DesktopCollection;
          label1.Text += " (R)";
      }

      if (rbn.Checked)
      {
          objNTDesktopCollection[strDesktopName].ChartCollection.BaseShow_Standart2();
      }

    }

    private void btnAlerts_Click(object sender, EventArgs e)
    {
      if (rbnMain.Checked)
      {
          Emulations.Window.RestoreAndActivate(Program.NTAlertWindow);
          Emulations.Window.ShowTopMost(Program.NTAlertWindow);

          Emulations.Window.RestoreAndActivate(Program.NTOutputWindow);
          Emulations.Window.ShowTopMost(Program.NTOutputWindow);
      }

      if (rbnRemote.Checked)
      {
          Program.SendMessageToNTServer("RestoreAndActivate|Alerts");
          Program.SendMessageToNTServer("RestoreAndActivate|Output");
      }
    }

    private void btnControlCenter_Click(object sender, EventArgs e)
    {
      if (rbnMain.Checked)
      {
          Emulations.Window.RestoreAndActivate(Program.NTControlCenterWindow);
      }

      if (rbnRemote.Checked)
      {
          Program.SendMessageToNTServer("RestoreAndActivate|ControlCenter");
      }
    }

    private void btnDayChart_Click(object sender, EventArgs e)
    {
      if (rbnMain.Checked)
      {
          Emulations.Window.SetWindowSizeAndMoveTo(Program.NTDayChart, Program.ScreensResolution.MainSecondScreenBounds);
          Emulations.Window.RestoreAndActivate(Program.NTDayChart);
          Emulations.Window.ShowTopMost(Program.NTDayChart);
      }

      if (rbnRemote.Checked)
      {
          String strMessage = "ActivateDayChart|" +
                              Emulations.Convert.Functions.ToString(Program.ScreensResolution.RemoteSecondScreenBounds);

          Program.SendMessageToNTServer(strMessage);
      }
    }

    private void chkHomework_CheckedChanged(object sender, EventArgs e)
    {
      CheckBox chkHomework = (CheckBox)sender;

      foreach (Control objControl in tabControl.TabPages["tabPageBase"].Controls)
      {
        if (objControl is RadioButton)
            ((RadioButton)objControl).Enabled = !chkHomework.Checked;
      }

      rbnMain.Enabled              = !chkHomework.Checked;
      rbnRemote.Enabled            = !chkHomework.Checked;
      rbnNTPrioritetLow.Enabled    = !chkHomework.Checked;
      rbnNTPrioritetNormal.Enabled = !chkHomework.Checked;

      //Fill_lstInstruments(objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection);
      //objDaysHomework
      //objNTChart.ShortName

      if (chkHomework.Checked)
      {
          if (rbnMain.Checked)
              objDaysHomework.FillMain();

          if (rbnRemote.Checked)
              objDaysHomework.FillRemote();

          Int32 objDaysHomeworkCount = objDaysHomework.Count;

          for (int i = 0; i < objDaysHomeworkCount; i++)
          {
            foreach (NTChart objNTChart in objDaysHomework)
            {
              if (objNTChart.ChartWindowCaption.IndexOf("1440") == -1)
              {
                  objDaysHomework.Remove(objNTChart);
                  break;
              }
            }
          }

          Fill_lstInstrumentsDaysHomework(objDaysHomework);
      }
      else
      {
          objDaysHomework.Clear();

          if (rbnMain.Checked)
              Fill_lstInstruments(objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection);

          if (rbnRemote.Checked)
              Fill_lstInstruments(objNTWorkspaceCollection.RemoteBaseWorkspace.DesktopCollection);
      }
    }

    public Bitmap GetControlScreenshot(Control objControl)
    {
      Bitmap objBitmapReturnValue = null;

      objBitmapReturnValue = new Bitmap(objControl.Width, objControl.Height);
      Rectangle rect       = new Rectangle(Point.Empty, objControl.Size);

      objControl.DrawToBitmap(objBitmapReturnValue, rect);

      return objBitmapReturnValue;
    }

    private void SaveFormScreenshot(Form frm, String strChartName)
    {
	    ImageFormat enmScreenshotFormat = ImageFormat.Png;
      
      Bitmap objBitmap   = GetControlScreenshot(frm);
      string strFileName = @"d:\" + strChartName + "__" + DateTime.Now.ToString().Replace(':', '_') + "." + enmScreenshotFormat.ToString();

      objBitmap.Save(strFileName, enmScreenshotFormat);
    }

    private void SavePringScreen()
    {
      // разобраться, Form frm = (Form)Control.FromHandle(objNTChart.Handle); возвращает null
      // потому что перекрыт доступ к формам других процессов, здесь как-то по-другому нужно получать доступ.
      if (objDaysHomework.Count == 0)
        return;

      foreach (NTChart objNTChart in objDaysHomework)
      {
        Process pr = Process.GetProcessById(Program.NTProcessID);

        //Form frm = (Form)Form.FromHandle(objNTChart.Handle);
        Form frm = (Form)Control.FromHandle(objNTChart.Handle);

        if (frm == null)
          continue;

        SaveFormScreenshot(frm, objNTChart.Name);

      }
    }

    private void btnView60MinChart_Click(object sender, EventArgs e)
    {
      try
      {
        NTChartCollection objNTChartCollection = null;
        String strDesktopName = lstInstruments.SelectedItem.ToString();

        if (rbnMain.Checked)
        {
          objNTChartCollection = objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection[strDesktopName].ChartCollection;
        }

        if (rbnRemote.Checked)
        {
          objNTChartCollection = objNTWorkspaceCollection.RemoteBaseWorkspace.DesktopCollection[strDesktopName].ChartCollection;
        }

        foreach (NTChart objNTChart in objNTChartCollection)
        {
          if (objNTChart.Name == strDesktopName && objNTChart.Is60MinChart)
          {
            if (objNTChart.Location == Enums.ChartLocation.Main)
            {
              objNTChart.SetPositionFullScreen(Program.ScreensResolution.MainFirstScreenBounds);
              objNTChart.Show();
            }

            if (objNTChart.Location == Enums.ChartLocation.Remote)
            {
              objNTChart.SetPositionFullScreen(Program.ScreensResolution.RemoteSecondScreenBounds);
              objNTChart.Show();
            }
          }
        }
      }
      catch (Exception ex)
      { }
    }
    /*
    private void btnReloadChart_Click(object sender, EventArgs e)
    {
      try
      {
        if (chkAllInstruments.Checked)
        {
          for (int i = 0; i < objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection.Count; i++)
          {
            objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection[i].ChartCollection.BaseShow_5MinFullScreen();

            System.Threading.Thread.Sleep(2000);

            System.Windows.Forms.SendKeys.SendWait("^+9");

            System.Threading.Thread.Sleep(3000);
          }

          return;
        }

        List<String> lst = new List<String>();

        if (!chkAllInstruments.Checked)
        {
            lst.Add("6M");
            //lst.Add("6N");

            //lst.Add("HG");
            //lst.Add("HO");
            //lst.Add("PA");
            //lst.Add("PL");

            //lst.Add("CC");
            //lst.Add("CT");
            //lst.Add("KC");
            //lst.Add("OJ");
            //lst.Add("SB");
        }

        for (int i = 0; i < objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection.Count; i++)
        {
          for (int j = 0; j < lst.Count; j++)
          {
            if (objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection[i].ChartCollection.Count > 0)
            {
                if (objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection[i].ChartCollection[0].ShortName == lst[j])
                {
                    objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection[i].ChartCollection.BaseShow_5MinFullScreen();
                
                    System.Threading.Thread.Sleep(2000);

                    //Emulations.Window.SetForegroundWindow(objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection[i].ChartCollection.Get5minChartByShortName("6M").Handle);
                    // System.Windows.Forms.SendKeys.SendWait("^+9");
                    System.Windows.Forms.SendKeys.SendWait("^{END}");

                    
                    System.Threading.Thread.Sleep(3000);
                }
            }
          }
        }
      }
      catch (Exception ex)
      { 
      }
    }
    */

    private void btnReloadChart_Click(object sender, EventArgs e)
    {
      try
      {
        List<String> lst = new List<String>();

        lst.Add("6M");
        //lst.Add("6N");

        //lst.Add("HG");
        //lst.Add("HO");
        //lst.Add("PA");
        //lst.Add("PL");

        //lst.Add("CC");
        //lst.Add("CT");
        //lst.Add("KC");
        //lst.Add("OJ");
        //lst.Add("SB");

        for (int i = 0; i < objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection.Count; i++)
        {
          for (int j = 0; j < lst.Count; j++)
          {
            if (objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection[i].ChartCollection.Count > 0)
            {
              if (objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection[i].ChartCollection[0].ShortName == lst[j])
              {
                objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection[i].ChartCollection.BaseShow_5MinFullScreen();

                System.Threading.Thread.Sleep(2000);

                //Clipboard.Clear();
//                System.Windows.Forms.SendKeys.SendWait("^+9");
                System.Windows.Forms.SendKeys.SendWait("^{m}");

                System.Threading.Thread.Sleep(3000);
              }
            }
          }
        }
      }
      catch (Exception ex)
      {
      }
    }

    private void btnPrepareDZ_Click(object sender, EventArgs e)
    {
      try
      {
        DeleteCheckBoxOnpnlDZInstruments();
        
        CheckBox objCheckBox;
        
        Panel objPanel   = this.pnlDZInstruments;

        Int32 iX         = objPanel.Bounds.X + 10;
        Int32 iY         = objPanel.Bounds.Y + 5;
        Int32 iCounter   = 1;
        Int32 iWX        = objPanel.Bounds.X;

        foreach (NTDesktop objNTDesktop in objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection)
        {
          objCheckBox             = new CheckBox();
          objCheckBox.Location    = new Point(iX, iY);
          objCheckBox.Name        = "chkDZInstrumentName_" + objNTDesktop.Name + "_" + iCounter.ToString();
          objCheckBox.Text        = objNTDesktop.Name;
          objCheckBox.Width       = 80;
          objPanel.Controls.Add(objCheckBox);

          iY += objCheckBox.Height;
          iCounter++;
        }
      }
      catch (Exception ex)
      {
      }
    }

    private void btnPrepearForNTIndicatorSoloATR1Percents_Click(object sender, EventArgs e)
    {
      String strDirectory             = @"c:\FFOutput\";

      String strResult_5              = String.Empty;
      String strResult_60             = String.Empty;
      String strResult_1440           = String.Empty;
      
      String strCurrentInstrumentName = String.Empty;

      //Пример из индикатора:
      //Add("6A 06-16", PeriodType.Minute, 5);
      //Add("6A 06-16", PeriodType.Minute, 60);
      //Add("6A 06-16", PeriodType.Minute, 1440);

      try
      {
        for (int i = 0; i < objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection.Count; i++)
        {
          strCurrentInstrumentName  = objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection[i].ChartCollection[0].Name;
          
          strResult_5               += "Add(\"" + strCurrentInstrumentName + "\", PeriodType.Minute, 5);"     + "\r\n";
          strResult_60              += "Add(\"" + strCurrentInstrumentName + "\", PeriodType.Minute, 60);"    + "\r\n";
          strResult_1440            += "Add(\"" + strCurrentInstrumentName + "\", PeriodType.Minute, 1440);"  + "\r\n";
        }

        if (!Directory.Exists(strDirectory))
            Directory.CreateDirectory(strDirectory);

        using (StreamWriter sw = File.CreateText(strDirectory + "PrepareData" + DateTime.Now.ToString().Replace(':', '_') + ".txt"))
        {
          sw.WriteLine(strResult_5 + "\r\n\r\n" + strResult_60 + "\r\n\r\n" + strResult_1440);
        }
      }
      catch (Exception ex)
      {
      }

    }

    private void btnPrepareResult_Click(object sender, EventArgs e)
    {
      try
      {
        DeleteCheckBoxOnpnlDZInstruments();

        CheckBox objCheckBox;

        Panel objPanel = this.pnlDZInstruments;

        Int32 iX = objPanel.Bounds.X + 10;
        Int32 iY = objPanel.Bounds.Y + 5;
        Int32 iCounter = 1;
        Int32 iWX = objPanel.Bounds.X;

        foreach (NTDesktop objNTDesktop in objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection)
        {
          objCheckBox = new CheckBox();
          objCheckBox.Location = new Point(iX, iY);
          objCheckBox.Name = "chkDZInstrumentName_" + iCounter.ToString();
          objCheckBox.Text = objNTDesktop.Name;
          objCheckBox.Width = 80;
          objCheckBox.CheckedChanged += new System.EventHandler(this.chkInstrumentForScreen_CheckedChanged);
          objPanel.Controls.Add(objCheckBox);

          iWX = objCheckBox.Location.X + objCheckBox.Width + 15;

          objCheckBox = new CheckBox();
          objCheckBox.Location = new Point(iWX, iY);
          objCheckBox.Name = "chkDZInstrumentIsTP_" + iCounter.ToString();
          objCheckBox.Text = "TP";
          objCheckBox.Width = 40;
          objCheckBox.ForeColor = Color.Green;
          objCheckBox.Enabled = false;
          objCheckBox.CheckedChanged += new System.EventHandler(this.chkInstrumentTPForScreen_CheckedChanged);
          objPanel.Controls.Add(objCheckBox); 

          iWX = objCheckBox.Location.X + objCheckBox.Width + 5;

          objCheckBox = new CheckBox();
          objCheckBox.Location = new Point(iWX, iY);
          objCheckBox.Name = "chkDZInstrumentIsSL_" + iCounter.ToString();
          objCheckBox.Text = "SL";
          objCheckBox.Width = 40;
          objCheckBox.ForeColor = Color.Red;
          objCheckBox.Enabled = false;
          objCheckBox.CheckedChanged += new System.EventHandler(this.chkInstrumentSLForScreen_CheckedChanged);
          objPanel.Controls.Add(objCheckBox);

          iY += objCheckBox.Height;
          iCounter++;
        }

      }
      catch (Exception ex)
      {
      }

    }

    private void btnSaveDZ_Click(object sender, EventArgs e)
    {
      try
      {
        List<String> lstCheckedInstruments = new List<String>();

        foreach (Control objControl in pnlDZInstruments.Controls)
        {
          if (objControl is CheckBox)
          {
              CheckBox objCheckBox = (CheckBox)objControl;

              String[] arr = objCheckBox.Name.Split('_');

              if (arr[0] == "chkDZInstrumentName" && objCheckBox.Checked)
                  lstCheckedInstruments.Add((objControl as CheckBox).Text);
          }
        }

        String strCheckedInstruments = "strCheckedInstruments : " + "\r\n\r\n";

        for (int i = 0; i < lstCheckedInstruments.Count; i++)
        {
          strCheckedInstruments += lstCheckedInstruments[i] + "\r\n";
        }

        const string message  = "Is the HomeWork window on the first screen?";
        const string caption  = "Save DZ charts";

        if (!chkDZ60mAnd1440m.Checked)
        {
            var result = MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.No)
                return;
        }

        //if (result == DialogResult.Yes)
        //    MessageBox.Show(strCheckedInstruments + "\r\n" + Properties.Settings.Default.ScreensFilePath);

        // СОХРАНЕНИЕ СКРИНШОТА ГРАФИКА ДНЕВКИ ИЗ ДОМАШКИ:

        this.WindowState = FormWindowState.Minimized;

        String strDirectory = Properties.Settings.Default.ScreensFilePath + @"\" + DateTime.Now.ToShortDateString();

        if (!Directory.Exists(strDirectory))
            Directory.CreateDirectory(strDirectory);

        for (int i = 0; i < lstCheckedInstruments.Count; i++)
        {
          String strInstrumentName    = lstCheckedInstruments[i];
          ImageFormat enmImageFormat  = ImageFormat.Png;
          String strFileName          = strDirectory + @"\" +
                                        strInstrumentName + "__" + DateTime.Now.ToShortDateString() + "__" + "DZ." + enmImageFormat.ToString();

          if (chkDZ60mAnd1440m.Checked)
              objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection[strInstrumentName].ChartCollection.BaseShow_1440And60FullScreen();
          else
              objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection[strInstrumentName].ChartCollection.BaseShow_1440MinFullScreen();
          
          System.Threading.Thread.Sleep(2000);

          Emulations.Graphics.SaveDesktopScreens(Screen.AllScreens, strFileName, enmImageFormat);
        }

        this.WindowState = FormWindowState.Normal;
      }
      catch (Exception ex)
      {
      }
    }

    private void btnSaveResult_Click(object sender, EventArgs e)
    {
      // написано 10.11.2016, сразу после победы Трампа, интересно посмотреть из будущего сюда и посмотреть
      // чего достиг из запланированного. Сейчас главное - запуск системы и стабильный ежемесячный (пусть небольшой) доход
      // от торговли либо через Gerchik&Co, либо через ForexClub
      try
      {
        Dictionary<String, String> dicCheckedInstruments = new Dictionary<String, String>();

        foreach (Control objControl in pnlDZInstruments.Controls)
        {
          if (objControl is CheckBox)
          {
              CheckBox objCheckBox      = (CheckBox)objControl;
              String[] arr              = objCheckBox.Name.Split('_');
              String strResultFileName  = String.Empty;

              if (arr[0] == "chkDZInstrumentName" && objCheckBox.Checked)
              { 
                  String strSLCheckBoxName  = "chkDZInstrumentIsSL_" + arr[1];
                  CheckBox chkSL            = (CheckBox)pnlDZInstruments.Controls[strSLCheckBoxName];
                  String strTPCheckBoxName  = "chkDZInstrumentIsTP_" + arr[1];
                  CheckBox chkTP            = (CheckBox)pnlDZInstruments.Controls[strTPCheckBoxName];

                  if ((chkSL != null && chkTP != null) && (chkSL.Checked || chkTP.Checked))
                  {
                      strResultFileName += objCheckBox.Text + "__" + DateTime.Now.ToShortDateString();

                      if (chkSL.Checked) strResultFileName += "__RESULT__STOPLOSS";
                      if (chkTP.Checked) strResultFileName += "__RESULT__TAKEPROFFIT";

                      dicCheckedInstruments.Add(objCheckBox.Text, strResultFileName);
                  }
              }
          }
        }

        String strCheckedInstruments = "strCheckedInstruments and FileName : " + "\r\n\r\n";

        foreach (KeyValuePair<String, String> kvp in dicCheckedInstruments)
        {
          strCheckedInstruments += kvp.Key + "   " + kvp.Value + "\r\n";
        }

        const string message  = "Are you ready?";
        const string caption  = "Save Result charts";
        var result            = MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

        //if (result == DialogResult.Yes)        
        //    MessageBox.Show(strCheckedInstruments);

        if (result == DialogResult.No)
            return;

        // СОХРАНЕНИЕ СКРИНШОТА ГРАФИКА ДНЕВКИ ИЗ ДОМАШКИ:

        this.WindowState    = FormWindowState.Minimized;
        
        String strDirectory = Properties.Settings.Default.ScreensFilePath + @"\" + DateTime.Now.ToShortDateString();

        if (!Directory.Exists(strDirectory))
            Directory.CreateDirectory(strDirectory);

        foreach (KeyValuePair<String, String> kvp in dicCheckedInstruments)
        {
          String strInstrumentName    = kvp.Value;
          ImageFormat enmImageFormat  = ImageFormat.Png;
          String strFileName          = strDirectory + @"\" + strInstrumentName + "." + enmImageFormat.ToString();

          objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection[kvp.Key].ChartCollection.BaseShow_Standart3();
          System.Threading.Thread.Sleep(2000);

          Emulations.Graphics.SaveDesktopScreens(Screen.AllScreens, strFileName, enmImageFormat);
        }

        this.WindowState = FormWindowState.Normal;

      }
      catch (Exception ex)
      {
      }
    }

    private void DeleteCheckBoxOnpnlDZInstruments()
    {
      try
      {
        List<String> lst = new List<String>();

        foreach (Control objControl in pnlDZInstruments.Controls)
        {
          if (objControl is CheckBox)
          {
            String[] arr = objControl.Name.Split('_');

            if (arr[0] == "chkDZInstrumentName" || arr[0] == "chkDZInstrumentIsSL" || arr[0] == "chkDZInstrumentIsTP")
              lst.Add((objControl as CheckBox).Name);
          }
        }

        for (int i = 0; i < lst.Count; i++)
        {
          pnlDZInstruments.Controls.RemoveByKey(lst[i]);
        }

      }
      catch (Exception ex)
      {
      }
    }

    private void chkInstrumentForScreen_CheckedChanged(object sender, EventArgs e)
    {
      try
      {
        CheckBox objCheckBox  = (CheckBox)sender;
        String[] arr          = objCheckBox.Name.Split('_');

        if (arr[0] == "chkDZInstrumentName")
        {
            String strSLCheckBoxName  = "chkDZInstrumentIsSL_" + arr[1];
            CheckBox chkSL            = (CheckBox)pnlDZInstruments.Controls[strSLCheckBoxName];

            String strTPCheckBoxName  = "chkDZInstrumentIsTP_" + arr[1];
            CheckBox chkTP            = (CheckBox)pnlDZInstruments.Controls[strTPCheckBoxName];

            if (chkSL != null && chkTP != null)
            {
                chkSL.Enabled = objCheckBox.Checked;
                chkTP.Enabled = objCheckBox.Checked;

                if (!objCheckBox.Checked)
                {
                    chkSL.Checked = false;
                    chkTP.Checked = false;
                }
            }
        }

      }
      catch (Exception ex)
      {
      }
    }

    private void chkInstrumentTPForScreen_CheckedChanged(object sender, EventArgs e)
    {
      try
      {
        CheckBox objCheckBox        = (CheckBox)sender;
        String[] arr                = objCheckBox.Name.Split('_');

        if (arr[0] == "chkDZInstrumentIsTP")
        {
          String strSLCheckBoxName  = "chkDZInstrumentIsSL_" + arr[1];
          CheckBox chkSL            = (CheckBox)pnlDZInstruments.Controls[strSLCheckBoxName];

          if (objCheckBox.Checked) 
              chkSL.Checked = false;
        }

      }
      catch (Exception ex)
      {
      }
    }

    private void chkInstrumentSLForScreen_CheckedChanged(object sender, EventArgs e)
    {
      try
      {
        CheckBox objCheckBox = (CheckBox)sender;
        String[] arr = objCheckBox.Name.Split('_');

        if (arr[0] == "chkDZInstrumentIsSL")
        {
          String strTPCheckBoxName = "chkDZInstrumentIsTP_" + arr[1];
          CheckBox chkTP = (CheckBox)pnlDZInstruments.Controls[strTPCheckBoxName];

          if (objCheckBox.Checked)
            chkTP.Checked = false;
        }

      }
      catch (Exception ex)
      {
      }
    }

    private void ParseDZOutLookExport_Real()
    {
      try
      {
        #region Examples
        //String[] stringSeparatorsRegExpKey = new string[] { @"\d\d.\d\d.\d\d\d\d" };
        // простой сплит, не рег. выражением - https://msdn.microsoft.com/ru-ru/library/1bwe3zdy(v=vs.110).aspx
        /*
          перевод символов в код:
          byte[] b = Encoding.Default.GetBytes("\"6A");
          MessageBox.Show(b[0].ToString()); // выводит 34
        */
        #endregion

        String strDirectory = @"D:\__АЛГОРИТМ\AUTOSCREENS";//Properties.Settings.Default.ScreensFilePath;
        String strResultFile = "ResultDZListForCode.txt";
        String strSourceFile = "exportDZ.txt";
        String strSourceText = String.Empty;
        String strResultText = String.Empty;
        String stringSeparatorsRegExpKey = @"\d\d.\d\d.\d\d\d\d";

        String strDZ_Date = String.Empty;
        String strDZ_Instrument = String.Empty;
        String strDZ_Price = String.Empty;
        String strDZ_LongShort = String.Empty;

        #region charsToTrim
        /*  собираем все двойные кавычки:
            Двойная кавычка	"	&#034;	&quot;
            Двойная левая кавычка	“	&#147;	&ldquo;
            Двойная правая кавычка	”	&#148;	&rdquo;
            Левая двойная угловая кавычка	«	&#171;	&laquo;
            Правая двойная угловая кавычка	&#187	&#187;	&raquo;
        */
        #endregion
        char[] charsToTrim = System.Text.Encoding.GetEncoding(1251).GetChars(new byte[] { 34, 147, 148, 171, 187, 32, 160, 9 });

        #region chars
        /*
         * СОБИРАЕМ РЕГ-ВЫРАЖЕНИЕ ДЛЯ ВСЕХ ВАРИАНТОВ ТИРЕ:
           Минус	-	&#045;
         * Короткий дефис	–	&#150;	&ndash;
         * Длинный дефис	—	&#151;	&mdash;
         */
        #endregion
        char[] chars = System.Text.Encoding.GetEncoding(1251).GetChars(new byte[] { 45, 150, 151 });
        String stringSeparatorsTire = chars[0].ToString() + "|" + chars[1].ToString() + "|" + chars[2].ToString();

        if (!Directory.Exists(strDirectory))
          Directory.CreateDirectory(strDirectory);

        if (File.Exists(strDirectory + @"\" + strSourceFile))
          strSourceText = System.IO.File.ReadAllText(strDirectory + @"\" + strSourceFile, System.Text.Encoding.GetEncoding(1251));

        String[] arrDZDays = Regex.Split(strSourceText, stringSeparatorsRegExpKey);
        Regex rgx = new Regex(stringSeparatorsRegExpKey);
        Match m = rgx.Match(strSourceText);
        Match mm = rgx.Match(strSourceText);

        for (int i = 1; i < /* 3 */arrDZDays.Length; i++)
        {
          if (m.Success)
          {
            strDZ_Date = strSourceText.Substring(m.Index, 10);
            m = m.NextMatch();
          }

          String[] arrLines = arrDZDays[i].Split(new String[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

          for (int j = 0; j < arrLines.Length; j++)
          {
            Int32 iIndexLong = arrLines[j].IndexOf("лонг", StringComparison.OrdinalIgnoreCase);
            Int32 iIndexShort = arrLines[j].IndexOf("шорт", StringComparison.OrdinalIgnoreCase);

            if (iIndexLong != -1)
            {
              String strSubstrLong = arrLines[j].Substring(0, iIndexLong - 1);
              String[] arrNamePrice = Regex.Split(strSubstrLong, stringSeparatorsTire);

              if (arrNamePrice.Length == 2)
              {
                strDZ_Instrument = arrNamePrice[0].Trim(charsToTrim);
                strDZ_Price = arrNamePrice[1].Trim(charsToTrim).Replace(',', '.');
                strDZ_LongShort = "лонг";

                // ПРИМЕР ДЗ ИЗ КОДА
                //              this.Add(new Solo_InstrumentDZ("17.10.2016", "6A", 0.7315, SoloInstrumentEnums.MarketPosition.Long, "Solo_15", SoloInstrumentEnums.SetupDirection.Trend, SoloInstrumentEnums.InstrumentRisk.Risk01));

                //strResultText += "Инструмент: " + strDZ_Instrument + " цена: " + strDZ_Price + " направление: " + strDZ_LongShort + "\r\n";
                strResultText += "this.Add(new Solo_InstrumentDZ(" +
                                 "\"" + strDZ_Date + "\", " +
                                 "\"" + strDZ_Instrument + "\", " +
                                 strDZ_Price.ToString() + ", " +
                                 "SoloInstrumentEnums.MarketPosition.Long, " +
                                 "\"" + "Solo_15" + "\", " +
                                 "SoloInstrumentEnums.SetupDirection.Trend, SoloInstrumentEnums.InstrumentRisk.Risk01));" +
                                 "\r\n";
              }
            }

            if (iIndexShort != -1)
            {
              String strSubstrShort = arrLines[j].Substring(0, iIndexShort - 1);
              String[] arrNamePrice = Regex.Split(strSubstrShort, stringSeparatorsTire);

              if (arrNamePrice.Length == 2)
              {
                strDZ_Instrument = arrNamePrice[0].Trim(charsToTrim);
                strDZ_Price = arrNamePrice[1].Trim(charsToTrim).Replace(',', '.');
                strDZ_LongShort = "шорт";

                strResultText += "this.Add(new Solo_InstrumentDZ(" +
                                 "\"" + strDZ_Date + "\", " +
                                 "\"" + strDZ_Instrument + "\", " +
                                 strDZ_Price.ToString() + ", " +
                                 "SoloInstrumentEnums.MarketPosition.Short, " +
                                 "\"" + "Solo_15" + "\", " +
                                 "SoloInstrumentEnums.SetupDirection.Trend, SoloInstrumentEnums.InstrumentRisk.Risk01));" +
                                 "\r\n";

                //strResultText += "Инструмент: " + strDZ_Instrument + " цена: " + strDZ_Price + " направление: " + strDZ_LongShort + "\r\n";
              }
            }
          }

        }

        using (StreamWriter sw = File.CreateText(strDirectory + "\\" + strResultFile))
        {
          sw.WriteLine(strResultText);
        }

      }
      catch (Exception ex)
      {
      }
    }

    private void bntDZListForCode_Click(object sender, EventArgs e)
    {
      ParseDZOutLookExport_Real();
    }

    private void btnChartsStatistic_Click(object sender, EventArgs e)
    {
      try
      {
        String strChartsInfo = "Name" + "\t\t" + "5min" + "\t" + "60min" + "\t" + "1440min" + "\t" + "Weekly" + "\t" + "Foot" + "\r\n" + "\r\n";

        foreach (NTDesktop objNTDesktop in objNTWorkspaceCollection.MainBaseWorkspace.DesktopCollection)
        {
          strChartsInfo +=  objNTDesktop.Name + "\t\t" +
                            objNTDesktop.ChartCollection.IsCharExist(Enums.ChartType.Time5Min)    + "\t"    +
                            objNTDesktop.ChartCollection.IsCharExist(Enums.ChartType.Time60Min)   + "\t"    +
                            objNTDesktop.ChartCollection.IsCharExist(Enums.ChartType.Time1440Min) + "\t"    +
                            objNTDesktop.ChartCollection.IsCharExist(Enums.ChartType.TimeWeekly)  + "\t"    +
                            objNTDesktop.ChartCollection.IsCharExist(Enums.ChartType.FootPrint)   + "\r\n";
        }

        txtATR.Text = strChartsInfo;

      }
      catch (Exception ex)
      {
      }
    }

    private void btnDz_Click(object sender, EventArgs e)
    {
      try
      {
        ShowDzWindow();
      }
      catch (Exception ex)
      {
      }
    }

  }
}
