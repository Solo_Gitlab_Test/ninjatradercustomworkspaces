﻿namespace NinjaTraderCustomWorkspaces
{
  partial class frm_AMGDz
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      this.grdDZList = new System.Windows.Forms.DataGridView();
      this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.panel1 = new System.Windows.Forms.Panel();
      this.label2 = new System.Windows.Forms.Label();
      this.dtFilterToDate = new System.Windows.Forms.DateTimePicker();
      this.btnCreateCode = new System.Windows.Forms.Button();
      this.btnFilterViewList = new System.Windows.Forms.Button();
      this.label1 = new System.Windows.Forms.Label();
      this.dtFilterFromDate = new System.Windows.Forms.DateTimePicker();
      this.panel2 = new System.Windows.Forms.Panel();
      this.panel3 = new System.Windows.Forms.Panel();
      this.panel5 = new System.Windows.Forms.Panel();
      this.label18 = new System.Windows.Forms.Label();
      this.cbDZNews = new System.Windows.Forms.ComboBox();
      this.label16 = new System.Windows.Forms.Label();
      this.cbInstrumentRisk = new System.Windows.Forms.ComboBox();
      this.label15 = new System.Windows.Forms.Label();
      this.txtComment = new System.Windows.Forms.TextBox();
      this.label14 = new System.Windows.Forms.Label();
      this.gbAddNews = new System.Windows.Forms.GroupBox();
      this.btnDeleteNews = new System.Windows.Forms.Button();
      this.cbNewsInstrument = new System.Windows.Forms.ComboBox();
      this.dtAddNewsTime = new System.Windows.Forms.DateTimePicker();
      this.btnAddNews = new System.Windows.Forms.Button();
      this.lstNews = new System.Windows.Forms.ListBox();
      this.dtAddNewsDate = new System.Windows.Forms.DateTimePicker();
      this.btnSelectScreenFile = new System.Windows.Forms.Button();
      this.label13 = new System.Windows.Forms.Label();
      this.txtScreenFile = new System.Windows.Forms.TextBox();
      this.label12 = new System.Windows.Forms.Label();
      this.nmrQuantity = new System.Windows.Forms.NumericUpDown();
      this.label11 = new System.Windows.Forms.Label();
      this.txtExitPrice = new System.Windows.Forms.TextBox();
      this.label10 = new System.Windows.Forms.Label();
      this.txtEntryPrice = new System.Windows.Forms.TextBox();
      this.label9 = new System.Windows.Forms.Label();
      this.cbDirection = new System.Windows.Forms.ComboBox();
      this.label6 = new System.Windows.Forms.Label();
      this.cbPosition = new System.Windows.Forms.ComboBox();
      this.label8 = new System.Windows.Forms.Label();
      this.dtDZMainDate = new System.Windows.Forms.DateTimePicker();
      this.label7 = new System.Windows.Forms.Label();
      this.txtLevelPrice = new System.Windows.Forms.TextBox();
      this.label5 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.dtExitTimeTime = new System.Windows.Forms.DateTimePicker();
      this.dtExitTimeDate = new System.Windows.Forms.DateTimePicker();
      this.dtEntryTimeTime = new System.Windows.Forms.DateTimePicker();
      this.dtEntryTimeDate = new System.Windows.Forms.DateTimePicker();
      this.cbDZInstrument = new System.Windows.Forms.ComboBox();
      this.panel4 = new System.Windows.Forms.Panel();
      this.btnSetDefaultFormDateTime = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.dtFormDefaultDateTime = new System.Windows.Forms.DateTimePicker();
      this.label17 = new System.Windows.Forms.Label();
      this.btnEditDZ = new System.Windows.Forms.Button();
      this.btnNewDZ = new System.Windows.Forms.Button();
      this.btnSaveDZ = new System.Windows.Forms.Button();
      this.txtTest = new System.Windows.Forms.TextBox();
      ((System.ComponentModel.ISupportInitialize)(this.grdDZList)).BeginInit();
      this.panel1.SuspendLayout();
      this.panel2.SuspendLayout();
      this.panel3.SuspendLayout();
      this.panel5.SuspendLayout();
      this.gbAddNews.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nmrQuantity)).BeginInit();
      this.panel4.SuspendLayout();
      this.SuspendLayout();
      // 
      // grdDZList
      // 
      this.grdDZList.AllowUserToAddRows = false;
      this.grdDZList.AllowUserToDeleteRows = false;
      this.grdDZList.AllowUserToOrderColumns = true;
      this.grdDZList.AllowUserToResizeColumns = false;
      this.grdDZList.AllowUserToResizeRows = false;
      this.grdDZList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.grdDZList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id});
      this.grdDZList.Dock = System.Windows.Forms.DockStyle.Fill;
      this.grdDZList.Location = new System.Drawing.Point(0, 0);
      this.grdDZList.Name = "grdDZList";
      this.grdDZList.Size = new System.Drawing.Size(881, 171);
      this.grdDZList.TabIndex = 4;
      // 
      // id
      // 
      this.id.DataPropertyName = "id";
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
      dataGridViewCellStyle2.NullValue = null;
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
      this.id.DefaultCellStyle = dataGridViewCellStyle2;
      this.id.HeaderText = "id";
      this.id.Name = "id";
      this.id.ReadOnly = true;
      this.id.Width = 50;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.label2);
      this.panel1.Controls.Add(this.dtFilterToDate);
      this.panel1.Controls.Add(this.btnCreateCode);
      this.panel1.Controls.Add(this.btnFilterViewList);
      this.panel1.Controls.Add(this.label1);
      this.panel1.Controls.Add(this.dtFilterFromDate);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(881, 72);
      this.panel1.TabIndex = 5;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label2.Location = new System.Drawing.Point(287, 23);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(41, 23);
      this.label2.TabIndex = 13;
      this.label2.Text = "To:";
      // 
      // dtFilterToDate
      // 
      this.dtFilterToDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.dtFilterToDate.Location = new System.Drawing.Point(333, 17);
      this.dtFilterToDate.Name = "dtFilterToDate";
      this.dtFilterToDate.Size = new System.Drawing.Size(200, 31);
      this.dtFilterToDate.TabIndex = 12;
      // 
      // btnCreateCode
      // 
      this.btnCreateCode.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnCreateCode.Location = new System.Drawing.Point(676, 17);
      this.btnCreateCode.Name = "btnCreateCode";
      this.btnCreateCode.Size = new System.Drawing.Size(178, 31);
      this.btnCreateCode.TabIndex = 11;
      this.btnCreateCode.Text = "Create Code";
      this.btnCreateCode.UseVisualStyleBackColor = true;
      this.btnCreateCode.Click += new System.EventHandler(this.btnCreateCode_Click);
      // 
      // btnFilterViewList
      // 
      this.btnFilterViewList.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnFilterViewList.Location = new System.Drawing.Point(545, 17);
      this.btnFilterViewList.Name = "btnFilterViewList";
      this.btnFilterViewList.Size = new System.Drawing.Size(125, 31);
      this.btnFilterViewList.TabIndex = 10;
      this.btnFilterViewList.Text = "View List";
      this.btnFilterViewList.UseVisualStyleBackColor = true;
      this.btnFilterViewList.Click += new System.EventHandler(this.btnFilterViewList_Click);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label1.Location = new System.Drawing.Point(3, 23);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(68, 23);
      this.label1.TabIndex = 8;
      this.label1.Text = "From:";
      // 
      // dtFilterFromDate
      // 
      this.dtFilterFromDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.dtFilterFromDate.Location = new System.Drawing.Point(71, 17);
      this.dtFilterFromDate.Name = "dtFilterFromDate";
      this.dtFilterFromDate.Size = new System.Drawing.Size(200, 31);
      this.dtFilterFromDate.TabIndex = 6;
      // 
      // panel2
      // 
      this.panel2.Controls.Add(this.txtTest);
      this.panel2.Controls.Add(this.grdDZList);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel2.Location = new System.Drawing.Point(0, 72);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(881, 171);
      this.panel2.TabIndex = 6;
      // 
      // panel3
      // 
      this.panel3.Controls.Add(this.panel5);
      this.panel3.Controls.Add(this.panel4);
      this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel3.Location = new System.Drawing.Point(0, 243);
      this.panel3.Name = "panel3";
      this.panel3.Size = new System.Drawing.Size(881, 458);
      this.panel3.TabIndex = 6;
      // 
      // panel5
      // 
      this.panel5.Controls.Add(this.label18);
      this.panel5.Controls.Add(this.cbDZNews);
      this.panel5.Controls.Add(this.label16);
      this.panel5.Controls.Add(this.cbInstrumentRisk);
      this.panel5.Controls.Add(this.label15);
      this.panel5.Controls.Add(this.txtComment);
      this.panel5.Controls.Add(this.label14);
      this.panel5.Controls.Add(this.gbAddNews);
      this.panel5.Controls.Add(this.btnSelectScreenFile);
      this.panel5.Controls.Add(this.label13);
      this.panel5.Controls.Add(this.txtScreenFile);
      this.panel5.Controls.Add(this.label12);
      this.panel5.Controls.Add(this.nmrQuantity);
      this.panel5.Controls.Add(this.label11);
      this.panel5.Controls.Add(this.txtExitPrice);
      this.panel5.Controls.Add(this.label10);
      this.panel5.Controls.Add(this.txtEntryPrice);
      this.panel5.Controls.Add(this.label9);
      this.panel5.Controls.Add(this.cbDirection);
      this.panel5.Controls.Add(this.label6);
      this.panel5.Controls.Add(this.cbPosition);
      this.panel5.Controls.Add(this.label8);
      this.panel5.Controls.Add(this.dtDZMainDate);
      this.panel5.Controls.Add(this.label7);
      this.panel5.Controls.Add(this.txtLevelPrice);
      this.panel5.Controls.Add(this.label5);
      this.panel5.Controls.Add(this.label4);
      this.panel5.Controls.Add(this.label3);
      this.panel5.Controls.Add(this.dtExitTimeTime);
      this.panel5.Controls.Add(this.dtExitTimeDate);
      this.panel5.Controls.Add(this.dtEntryTimeTime);
      this.panel5.Controls.Add(this.dtEntryTimeDate);
      this.panel5.Controls.Add(this.cbDZInstrument);
      this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel5.Location = new System.Drawing.Point(0, 48);
      this.panel5.Name = "panel5";
      this.panel5.Size = new System.Drawing.Size(881, 410);
      this.panel5.TabIndex = 1;
      // 
      // label18
      // 
      this.label18.AutoSize = true;
      this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label18.Location = new System.Drawing.Point(456, 331);
      this.label18.Name = "label18";
      this.label18.Size = new System.Drawing.Size(63, 24);
      this.label18.TabIndex = 44;
      this.label18.Text = "News:";
      // 
      // cbDZNews
      // 
      this.cbDZNews.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbDZNews.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.cbDZNews.FormattingEnabled = true;
      this.cbDZNews.Location = new System.Drawing.Point(526, 328);
      this.cbDZNews.Name = "cbDZNews";
      this.cbDZNews.Size = new System.Drawing.Size(328, 32);
      this.cbDZNews.TabIndex = 43;
      // 
      // label16
      // 
      this.label16.AutoSize = true;
      this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label16.Location = new System.Drawing.Point(63, 162);
      this.label16.Name = "label16";
      this.label16.Size = new System.Drawing.Size(142, 24);
      this.label16.TabIndex = 42;
      this.label16.Text = "Instrument Risk:";
      // 
      // cbInstrumentRisk
      // 
      this.cbInstrumentRisk.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbInstrumentRisk.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.cbInstrumentRisk.FormattingEnabled = true;
      this.cbInstrumentRisk.Location = new System.Drawing.Point(219, 158);
      this.cbInstrumentRisk.Name = "cbInstrumentRisk";
      this.cbInstrumentRisk.Size = new System.Drawing.Size(168, 32);
      this.cbInstrumentRisk.TabIndex = 41;
      // 
      // label15
      // 
      this.label15.AutoSize = true;
      this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label15.Location = new System.Drawing.Point(422, 228);
      this.label15.Name = "label15";
      this.label15.Size = new System.Drawing.Size(97, 24);
      this.label15.TabIndex = 40;
      this.label15.Text = "Comment:";
      // 
      // txtComment
      // 
      this.txtComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.txtComment.Location = new System.Drawing.Point(526, 225);
      this.txtComment.Multiline = true;
      this.txtComment.Name = "txtComment";
      this.txtComment.Size = new System.Drawing.Size(328, 95);
      this.txtComment.TabIndex = 39;
      // 
      // label14
      // 
      this.label14.AutoSize = true;
      this.label14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
      this.label14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label14.ForeColor = System.Drawing.Color.Green;
      this.label14.Location = new System.Drawing.Point(3, 14);
      this.label14.Name = "label14";
      this.label14.Size = new System.Drawing.Size(90, 57);
      this.label14.TabIndex = 38;
      this.label14.Text = "DZ";
      // 
      // gbAddNews
      // 
      this.gbAddNews.Controls.Add(this.btnDeleteNews);
      this.gbAddNews.Controls.Add(this.cbNewsInstrument);
      this.gbAddNews.Controls.Add(this.dtAddNewsTime);
      this.gbAddNews.Controls.Add(this.btnAddNews);
      this.gbAddNews.Controls.Add(this.lstNews);
      this.gbAddNews.Controls.Add(this.dtAddNewsDate);
      this.gbAddNews.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.gbAddNews.Location = new System.Drawing.Point(7, 194);
      this.gbAddNews.Name = "gbAddNews";
      this.gbAddNews.Size = new System.Drawing.Size(394, 215);
      this.gbAddNews.TabIndex = 37;
      this.gbAddNews.TabStop = false;
      this.gbAddNews.Text = "ALL News";
      // 
      // btnDeleteNews
      // 
      this.btnDeleteNews.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnDeleteNews.Location = new System.Drawing.Point(306, 169);
      this.btnDeleteNews.Name = "btnDeleteNews";
      this.btnDeleteNews.Size = new System.Drawing.Size(73, 32);
      this.btnDeleteNews.TabIndex = 40;
      this.btnDeleteNews.Text = "Del";
      this.btnDeleteNews.UseVisualStyleBackColor = true;
      this.btnDeleteNews.Click += new System.EventHandler(this.btnDeleteNews_Click);
      // 
      // cbNewsInstrument
      // 
      this.cbNewsInstrument.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbNewsInstrument.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.cbNewsInstrument.FormattingEnabled = true;
      this.cbNewsInstrument.Location = new System.Drawing.Point(34, 169);
      this.cbNewsInstrument.Name = "cbNewsInstrument";
      this.cbNewsInstrument.Size = new System.Drawing.Size(168, 32);
      this.cbNewsInstrument.TabIndex = 43;
      // 
      // dtAddNewsTime
      // 
      this.dtAddNewsTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.dtAddNewsTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
      this.dtAddNewsTime.Location = new System.Drawing.Point(276, 133);
      this.dtAddNewsTime.Name = "dtAddNewsTime";
      this.dtAddNewsTime.Size = new System.Drawing.Size(103, 29);
      this.dtAddNewsTime.TabIndex = 39;
      // 
      // btnAddNews
      // 
      this.btnAddNews.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnAddNews.Location = new System.Drawing.Point(227, 169);
      this.btnAddNews.Name = "btnAddNews";
      this.btnAddNews.Size = new System.Drawing.Size(73, 32);
      this.btnAddNews.TabIndex = 38;
      this.btnAddNews.Text = "Add";
      this.btnAddNews.UseVisualStyleBackColor = true;
      this.btnAddNews.Click += new System.EventHandler(this.btnAddNews_Click);
      // 
      // lstNews
      // 
      this.lstNews.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.lstNews.FormattingEnabled = true;
      this.lstNews.ItemHeight = 24;
      this.lstNews.Location = new System.Drawing.Point(34, 26);
      this.lstNews.Name = "lstNews";
      this.lstNews.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
      this.lstNews.Size = new System.Drawing.Size(345, 100);
      this.lstNews.TabIndex = 36;
      // 
      // dtAddNewsDate
      // 
      this.dtAddNewsDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.dtAddNewsDate.Location = new System.Drawing.Point(34, 133);
      this.dtAddNewsDate.Name = "dtAddNewsDate";
      this.dtAddNewsDate.Size = new System.Drawing.Size(225, 29);
      this.dtAddNewsDate.TabIndex = 38;
      // 
      // btnSelectScreenFile
      // 
      this.btnSelectScreenFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnSelectScreenFile.Location = new System.Drawing.Point(655, 155);
      this.btnSelectScreenFile.Name = "btnSelectScreenFile";
      this.btnSelectScreenFile.Size = new System.Drawing.Size(199, 31);
      this.btnSelectScreenFile.TabIndex = 34;
      this.btnSelectScreenFile.Text = "Select Screen File";
      this.btnSelectScreenFile.UseVisualStyleBackColor = true;
      this.btnSelectScreenFile.Click += new System.EventHandler(this.btnSelectScreenFile_Click);
      // 
      // label13
      // 
      this.label13.AutoSize = true;
      this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label13.Location = new System.Drawing.Point(407, 193);
      this.label13.Name = "label13";
      this.label13.Size = new System.Drawing.Size(112, 24);
      this.label13.TabIndex = 33;
      this.label13.Text = "Screen File:";
      // 
      // txtScreenFile
      // 
      this.txtScreenFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.txtScreenFile.Location = new System.Drawing.Point(526, 190);
      this.txtScreenFile.Name = "txtScreenFile";
      this.txtScreenFile.ReadOnly = true;
      this.txtScreenFile.Size = new System.Drawing.Size(328, 29);
      this.txtScreenFile.TabIndex = 32;
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label12.Location = new System.Drawing.Point(436, 155);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(83, 24);
      this.label12.TabIndex = 31;
      this.label12.Text = "Quantity:";
      // 
      // nmrQuantity
      // 
      this.nmrQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.nmrQuantity.Location = new System.Drawing.Point(526, 155);
      this.nmrQuantity.Name = "nmrQuantity";
      this.nmrQuantity.ReadOnly = true;
      this.nmrQuantity.Size = new System.Drawing.Size(58, 29);
      this.nmrQuantity.TabIndex = 30;
      this.nmrQuantity.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label11.Location = new System.Drawing.Point(651, 122);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(94, 24);
      this.label11.TabIndex = 29;
      this.label11.Text = "Exit Price:";
      // 
      // txtExitPrice
      // 
      this.txtExitPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.txtExitPrice.Location = new System.Drawing.Point(751, 120);
      this.txtExitPrice.Name = "txtExitPrice";
      this.txtExitPrice.Size = new System.Drawing.Size(103, 29);
      this.txtExitPrice.TabIndex = 28;
      this.txtExitPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtExitPrice_KeyPress);
      this.txtExitPrice.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtExitPrice_KeyUp);
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label10.Location = new System.Drawing.Point(413, 123);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(106, 24);
      this.label10.TabIndex = 27;
      this.label10.Text = "Entry Price:";
      // 
      // txtEntryPrice
      // 
      this.txtEntryPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.txtEntryPrice.Location = new System.Drawing.Point(526, 119);
      this.txtEntryPrice.Name = "txtEntryPrice";
      this.txtEntryPrice.Size = new System.Drawing.Size(94, 29);
      this.txtEntryPrice.TabIndex = 26;
      this.txtEntryPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEntryPrice_KeyPress);
      this.txtEntryPrice.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtEntryPrice_KeyUp);
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label9.Location = new System.Drawing.Point(116, 123);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(89, 24);
      this.label9.TabIndex = 25;
      this.label9.Text = "Direction:";
      // 
      // cbDirection
      // 
      this.cbDirection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbDirection.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.cbDirection.FormattingEnabled = true;
      this.cbDirection.Location = new System.Drawing.Point(219, 120);
      this.cbDirection.Name = "cbDirection";
      this.cbDirection.Size = new System.Drawing.Size(168, 32);
      this.cbDirection.TabIndex = 24;
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label6.Location = new System.Drawing.Point(124, 88);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(81, 24);
      this.label6.TabIndex = 23;
      this.label6.Text = "Position:";
      // 
      // cbPosition
      // 
      this.cbPosition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.cbPosition.Location = new System.Drawing.Point(219, 85);
      this.cbPosition.Name = "cbPosition";
      this.cbPosition.Size = new System.Drawing.Size(168, 32);
      this.cbPosition.TabIndex = 22;
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label8.Location = new System.Drawing.Point(436, 18);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(83, 24);
      this.label8.TabIndex = 21;
      this.label8.Text = "DZ Date:";
      // 
      // dtDZMainDate
      // 
      this.dtDZMainDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.dtDZMainDate.Location = new System.Drawing.Point(526, 14);
      this.dtDZMainDate.Name = "dtDZMainDate";
      this.dtDZMainDate.Size = new System.Drawing.Size(219, 29);
      this.dtDZMainDate.TabIndex = 20;
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label7.Location = new System.Drawing.Point(97, 53);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(108, 24);
      this.label7.TabIndex = 19;
      this.label7.Text = "Level Price:";
      // 
      // txtLevelPrice
      // 
      this.txtLevelPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.txtLevelPrice.Location = new System.Drawing.Point(219, 50);
      this.txtLevelPrice.Name = "txtLevelPrice";
      this.txtLevelPrice.Size = new System.Drawing.Size(168, 29);
      this.txtLevelPrice.TabIndex = 18;
      this.txtLevelPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLevelPrice_KeyPress);
      this.txtLevelPrice.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtLevelPrice_KeyUp);
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label5.Location = new System.Drawing.Point(103, 18);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(102, 24);
      this.label5.TabIndex = 13;
      this.label5.Text = "Instrument:";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label4.Location = new System.Drawing.Point(433, 88);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(86, 24);
      this.label4.TabIndex = 12;
      this.label4.Text = "Exit time:";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label3.Location = new System.Drawing.Point(421, 53);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(98, 24);
      this.label3.TabIndex = 11;
      this.label3.Text = "Entry time:";
      // 
      // dtExitTimeTime
      // 
      this.dtExitTimeTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.dtExitTimeTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
      this.dtExitTimeTime.Location = new System.Drawing.Point(751, 84);
      this.dtExitTimeTime.Name = "dtExitTimeTime";
      this.dtExitTimeTime.Size = new System.Drawing.Size(103, 29);
      this.dtExitTimeTime.TabIndex = 10;
      // 
      // dtExitTimeDate
      // 
      this.dtExitTimeDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.dtExitTimeDate.Location = new System.Drawing.Point(526, 84);
      this.dtExitTimeDate.Name = "dtExitTimeDate";
      this.dtExitTimeDate.Size = new System.Drawing.Size(219, 29);
      this.dtExitTimeDate.TabIndex = 9;
      // 
      // dtEntryTimeTime
      // 
      this.dtEntryTimeTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.dtEntryTimeTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
      this.dtEntryTimeTime.Location = new System.Drawing.Point(751, 49);
      this.dtEntryTimeTime.Name = "dtEntryTimeTime";
      this.dtEntryTimeTime.Size = new System.Drawing.Size(103, 29);
      this.dtEntryTimeTime.TabIndex = 8;
      // 
      // dtEntryTimeDate
      // 
      this.dtEntryTimeDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.dtEntryTimeDate.Location = new System.Drawing.Point(526, 49);
      this.dtEntryTimeDate.Name = "dtEntryTimeDate";
      this.dtEntryTimeDate.Size = new System.Drawing.Size(219, 29);
      this.dtEntryTimeDate.TabIndex = 7;
      // 
      // cbDZInstrument
      // 
      this.cbDZInstrument.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbDZInstrument.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.cbDZInstrument.FormattingEnabled = true;
      this.cbDZInstrument.Location = new System.Drawing.Point(219, 15);
      this.cbDZInstrument.Name = "cbDZInstrument";
      this.cbDZInstrument.Size = new System.Drawing.Size(168, 32);
      this.cbDZInstrument.TabIndex = 0;
      // 
      // panel4
      // 
      this.panel4.Controls.Add(this.btnSetDefaultFormDateTime);
      this.panel4.Controls.Add(this.btnCancel);
      this.panel4.Controls.Add(this.dtFormDefaultDateTime);
      this.panel4.Controls.Add(this.label17);
      this.panel4.Controls.Add(this.btnEditDZ);
      this.panel4.Controls.Add(this.btnNewDZ);
      this.panel4.Controls.Add(this.btnSaveDZ);
      this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel4.Location = new System.Drawing.Point(0, 0);
      this.panel4.Name = "panel4";
      this.panel4.Size = new System.Drawing.Size(881, 48);
      this.panel4.TabIndex = 0;
      // 
      // btnSetDefaultFormDateTime
      // 
      this.btnSetDefaultFormDateTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnSetDefaultFormDateTime.Location = new System.Drawing.Point(322, 6);
      this.btnSetDefaultFormDateTime.Name = "btnSetDefaultFormDateTime";
      this.btnSetDefaultFormDateTime.Size = new System.Drawing.Size(65, 31);
      this.btnSetDefaultFormDateTime.TabIndex = 46;
      this.btnSetDefaultFormDateTime.Text = "Set";
      this.btnSetDefaultFormDateTime.UseVisualStyleBackColor = true;
      this.btnSetDefaultFormDateTime.Click += new System.EventHandler(this.btnSetDefaultFormDateTime_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnCancel.Location = new System.Drawing.Point(772, 6);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(82, 31);
      this.btnCancel.TabIndex = 44;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // dtFormDefaultDateTime
      // 
      this.dtFormDefaultDateTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.dtFormDefaultDateTime.Location = new System.Drawing.Point(101, 6);
      this.dtFormDefaultDateTime.Name = "dtFormDefaultDateTime";
      this.dtFormDefaultDateTime.Size = new System.Drawing.Size(214, 31);
      this.dtFormDefaultDateTime.TabIndex = 45;
      // 
      // label17
      // 
      this.label17.AutoSize = true;
      this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label17.Location = new System.Drawing.Point(5, 10);
      this.label17.Name = "label17";
      this.label17.Size = new System.Drawing.Size(86, 25);
      this.label17.TabIndex = 43;
      this.label17.Text = "Default:";
      // 
      // btnEditDZ
      // 
      this.btnEditDZ.Enabled = false;
      this.btnEditDZ.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnEditDZ.Location = new System.Drawing.Point(680, 6);
      this.btnEditDZ.Name = "btnEditDZ";
      this.btnEditDZ.Size = new System.Drawing.Size(65, 31);
      this.btnEditDZ.TabIndex = 12;
      this.btnEditDZ.Text = "Edit";
      this.btnEditDZ.UseVisualStyleBackColor = true;
      this.btnEditDZ.Click += new System.EventHandler(this.btnEditDZ_Click);
      // 
      // btnNewDZ
      // 
      this.btnNewDZ.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnNewDZ.Location = new System.Drawing.Point(526, 6);
      this.btnNewDZ.Name = "btnNewDZ";
      this.btnNewDZ.Size = new System.Drawing.Size(65, 31);
      this.btnNewDZ.TabIndex = 11;
      this.btnNewDZ.Text = "New";
      this.btnNewDZ.UseVisualStyleBackColor = true;
      this.btnNewDZ.Click += new System.EventHandler(this.btnNewDZ_Click);
      // 
      // btnSaveDZ
      // 
      this.btnSaveDZ.Enabled = false;
      this.btnSaveDZ.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnSaveDZ.Location = new System.Drawing.Point(605, 6);
      this.btnSaveDZ.Name = "btnSaveDZ";
      this.btnSaveDZ.Size = new System.Drawing.Size(65, 31);
      this.btnSaveDZ.TabIndex = 13;
      this.btnSaveDZ.Text = "Save";
      this.btnSaveDZ.UseVisualStyleBackColor = true;
      this.btnSaveDZ.Click += new System.EventHandler(this.btnSaveDZ_Click);
      // 
      // txtTest
      // 
      this.txtTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.txtTest.Location = new System.Drawing.Point(545, 18);
      this.txtTest.Name = "txtTest";
      this.txtTest.Size = new System.Drawing.Size(125, 29);
      this.txtTest.TabIndex = 19;
      this.txtTest.Text = "17";
      // 
      // frm_AMGDz
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(881, 701);
      this.Controls.Add(this.panel2);
      this.Controls.Add(this.panel3);
      this.Controls.Add(this.panel1);
      this.Name = "frm_AMGDz";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "- SOLO AMG DZ -";
      this.Load += new System.EventHandler(this.frm_Dz_Load);
      ((System.ComponentModel.ISupportInitialize)(this.grdDZList)).EndInit();
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      this.panel2.ResumeLayout(false);
      this.panel2.PerformLayout();
      this.panel3.ResumeLayout(false);
      this.panel5.ResumeLayout(false);
      this.panel5.PerformLayout();
      this.gbAddNews.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.nmrQuantity)).EndInit();
      this.panel4.ResumeLayout(false);
      this.panel4.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.DataGridView grdDZList;
    private System.Windows.Forms.DataGridViewTextBoxColumn id;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.Panel panel3;
    private System.Windows.Forms.Button btnFilterViewList;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.DateTimePicker dtFilterFromDate;
    private System.Windows.Forms.Panel panel5;
    private System.Windows.Forms.Panel panel4;
    private System.Windows.Forms.Button btnSaveDZ;
    private System.Windows.Forms.Button btnEditDZ;
    private System.Windows.Forms.Button btnNewDZ;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.DateTimePicker dtFilterToDate;
    private System.Windows.Forms.Button btnCreateCode;
    private System.Windows.Forms.ComboBox cbDZInstrument;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.DateTimePicker dtExitTimeTime;
    private System.Windows.Forms.DateTimePicker dtExitTimeDate;
    private System.Windows.Forms.DateTimePicker dtEntryTimeTime;
    private System.Windows.Forms.DateTimePicker dtEntryTimeDate;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.ComboBox cbDirection;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.ComboBox cbPosition;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.DateTimePicker dtDZMainDate;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.TextBox txtLevelPrice;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.TextBox txtExitPrice;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.TextBox txtEntryPrice;
    private System.Windows.Forms.Button btnSelectScreenFile;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.TextBox txtScreenFile;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.NumericUpDown nmrQuantity;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.GroupBox gbAddNews;
    private System.Windows.Forms.Button btnDeleteNews;
    private System.Windows.Forms.Button btnAddNews;
    private System.Windows.Forms.DateTimePicker dtAddNewsTime;
    private System.Windows.Forms.ListBox lstNews;
    private System.Windows.Forms.DateTimePicker dtAddNewsDate;
    private System.Windows.Forms.Label label15;
    private System.Windows.Forms.TextBox txtComment;
    private System.Windows.Forms.Label label16;
    private System.Windows.Forms.ComboBox cbInstrumentRisk;
    private System.Windows.Forms.Label label17;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.ComboBox cbNewsInstrument;
    private System.Windows.Forms.Label label18;
    private System.Windows.Forms.ComboBox cbDZNews;
    private System.Windows.Forms.Button btnSetDefaultFormDateTime;
    private System.Windows.Forms.DateTimePicker dtFormDefaultDateTime;
    private System.Windows.Forms.TextBox txtTest;
  }
}