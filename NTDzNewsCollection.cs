﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.OleDb;
using System.Globalization;

namespace NinjaTraderCustomWorkspaces
{
  public class NTDzNewsCollection : System.Collections.CollectionBase
  {
    private String[] arrInstrumentsThatDependentOnUSDNews = new String[] { "6A", "6B", "6C", "6E", "6J", "6M", "6N", "6S", "ES", "YM", "NQ", "TF" };

    public NTDzNewsCollection()
    {
    }

    public void EnumerateItems()
    {
      try
      {
        Int32 n = 0;

        foreach(NTDzNews objNTDzNews in this)
        {
          n++;
          objNTDzNews.NumberInCollection = n;
        }
      }
      catch (Exception ex)
      {
      }}

    public void AnalyzeAndClear(String strInstrumentName)
    {
      try
      {
        Boolean blnIncludeUSDNews = arrInstrumentsThatDependentOnUSDNews.Contains(strInstrumentName);

        for(int i = this.Count - 1; i >= 0; i--)
        {
          if (blnIncludeUSDNews)
          {
              if (this[i].InstrumentName == strInstrumentName || this[i].InstrumentName == "USD")
                  continue;
          }
          else
          {
              if (this[i].InstrumentName == strInstrumentName)
                  continue;
          }

          this.RemoveAt(i);
        }
      }
      catch (Exception ex)
      {
      }
    }

    public void FillByDateTimeInterval(DateTime dtFromDate, DateTime dtToDate)
    {
      try
      {
        if (this.Count > 0)
            this.Clear();

        DateTime dtFromDateTime   = new DateTime(dtFromDate.Year, dtFromDate.Month, dtFromDate.Day, 0,0,0);
        DateTime dtToDateTime     = new DateTime(dtToDate.Year, dtToDate.Month, dtToDate.Day, 23,59,59);

        using (OleDbConnection objConnection = new OleDbConnection(Program.SoloAMG_DZConnectionString))
        {
          objConnection.Open();
          OleDbCommand objCommand = objConnection.CreateCommand();
          StringBuilder sbQuery   = new StringBuilder();

          sbQuery.Append("SELECT News.ID, Instrument.ID, Instrument.Name, News.NewsDateTime ");
          sbQuery.Append("FROM Instrument INNER JOIN News ON Instrument.ID = News.InstrumentID ");
          sbQuery.Append("WHERE News.NewsDateTime >= #" + dtFromDateTime.ToString("yyyy.MM.dd HH:mm:ss").Replace('.', '-') + "# ");
          sbQuery.Append("AND News.NewsDateTime <= #"   + dtToDateTime.ToString("yyyy.MM.dd HH:mm:ss").Replace('.', '-') + "# ");
          sbQuery.Append("ORDER BY News.ID;");

          objCommand.CommandText = sbQuery.ToString();
      
          using (OleDbDataReader objReader = objCommand.ExecuteReader())
          {
            while (objReader.Read())
            {
              this.Add(new NTDzNews((Int32)objReader[0], 
                                    (Int32)objReader[1], 
                                    (String)objReader[2],
                                    DateTime.Parse(objReader[3].ToString())));
            }
          }
        }

        this.EnumerateItems();
      }
      catch (Exception ex)
      {
      }
    }

    public void FillByDZ_ID(Int32 iNTDZ_ID)
    {
      try
      {
        if (this.Count > 0)
            this.Clear();

        using (OleDbConnection objConnection = new OleDbConnection(Program.SoloAMG_DZConnectionString))
        {
          objConnection.Open();
          OleDbCommand objCommand = objConnection.CreateCommand();
          StringBuilder sbQuery   = new StringBuilder();

          sbQuery.Append("SELECT News.ID, Instrument.ID, Instrument.Name, News.NewsDateTime ");
          sbQuery.Append("FROM Instrument INNER JOIN (News INNER JOIN DZ_NEWS ON News.ID = DZ_NEWS.NewsID) ON Instrument.ID = News.InstrumentID ");
          sbQuery.Append("WHERE DZ_NEWS.DZID = " + iNTDZ_ID.ToString());

          objCommand.CommandText = sbQuery.ToString();
      
          using (OleDbDataReader objReader = objCommand.ExecuteReader())
          {
            while (objReader.Read())
            {
              this.Add(new NTDzNews((Int32)objReader[0], 
                                    (Int32)objReader[1], 
                                    (String)objReader[2],
                                    DateTime.Parse(objReader[3].ToString())/*DateTime.ParseExact((String)objReader[3], "yyyy.MM.dd HH:mm:ss", CultureInfo.InvariantCulture)*/));
            }
          }
        }

        this.EnumerateItems();
      }
      catch (Exception ex)
      {
      }
    }


    #region Standart Functions & indexator
    public Int32 Add(NTDzNews objNTDzNews)
    {
      return (List.Add(objNTDzNews));
    }

    public Int32 IndexOf(NTDzNews objNTDzNews)
    {
      return (List.IndexOf(objNTDzNews));
    }

    public void Insert(Int32 index, NTDzNews objNTDzNews)
    {
      List.Insert(index, objNTDzNews);
    }

    public void Remove(NTDzNews objNTDzNews)
    {
      List.Remove(objNTDzNews);
    }

    public NTDzNews this[int index]
    {
      get
      {
        return (List[index] as NTDzNews);
      }
      set
      {
        List[index] = value;
      }
    }

    //public NTDzNews this[String strNewsID]
    //{
    //  get
    //  {
    //    return (List[index] as NTDzNews);
    //  }
    //}

    #endregion

  }
}
