﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;

namespace NinjaTraderCustomWorkspaces
{
  public class NTDz
  {
    private Int32                     m_iID                     = 0;
    private Int32                     m_iInstrumentID           = 0;
    private String                    m_strInstrumentName       = String.Empty;
    private Double                    m_dblLevelPrice           = 0;
    private Enums.DZPosition          m_enmDZPosition           = Enums.DZPosition.Empty;
    private Enums.DZDirection         m_enmDZDirection          = Enums.DZDirection.Empty;
    private Enums.InstrumentRisk      m_enmInstrumentRisk       = Enums.InstrumentRisk.Empty;
    private DateTime                  m_dtMainDate              = DateTime.MinValue;
    private DateTime                  m_dtEntryDatetime         = DateTime.MinValue;
    private DateTime                  m_dtExitDatetime          = DateTime.MinValue;
    private Double                    m_dblEntryPrice           = 0;
    private Double                    m_dblExitPrice            = 0;
    private Int32                     m_iQuantity               = 0;
    private String                    m_strSelectScreenFile     = String.Empty;
    private String                    m_strComment              = String.Empty;

    private NTDzNewsCollection        m_objNTDZNews             = new NTDzNewsCollection();

    public NTDz()
    {
    }

    public NTDz(Int32 iInstrumentID, String strInstrumentName, Double dblLevelPrice, Enums.DZPosition enmDZPosition,
                Enums.DZDirection enmDZDirection,Enums.InstrumentRisk enmInstrumentRisk,
                DateTime dtMainDate, String strComment)
    {
      m_iInstrumentID       = iInstrumentID;
      m_strInstrumentName   = strInstrumentName;
      m_dblLevelPrice       = dblLevelPrice;
      m_enmDZPosition       = enmDZPosition;
      m_enmDZDirection      = enmDZDirection;
      m_enmInstrumentRisk   = enmInstrumentRisk;
      m_dtMainDate          = dtMainDate;
      m_strComment          = strComment;
    }

    public Boolean Create()
    {
      using (OleDbConnection objConnection = new OleDbConnection(Program.SoloAMG_DZConnectionString))
      {
        OleDbCommand objCommand            = objConnection.CreateCommand();
        OleDbTransaction objTransaction    = null;
        StringBuilder sbInsertDesktopQuery = new StringBuilder();

        sbInsertDesktopQuery.Append("INSERT INTO AMGDZ (InstrumentID, LevelPrice, [Position], Direction, InstrumentRisk, MainDate, Comment) ");
        sbInsertDesktopQuery.Append("VALUES (");
        sbInsertDesktopQuery.Append(this.m_iInstrumentID.ToString()                     + ", ");
        sbInsertDesktopQuery.Append("'" + m_dblLevelPrice.ToString()                    + "', ");
        sbInsertDesktopQuery.Append("'" + m_enmDZPosition.ToString()                    + "', ");
        sbInsertDesktopQuery.Append("'" + m_enmDZDirection.ToString()                   + "', ");
        sbInsertDesktopQuery.Append("'" + m_enmInstrumentRisk.ToString()                + "', ");
        sbInsertDesktopQuery.Append("'" + m_dtMainDate.ToString("yyyy.MM.dd HH:mm:ss")  + "', ");
        sbInsertDesktopQuery.Append("'" + m_strComment                                  + "');");

        try
        {
          objConnection.Open();

          objTransaction         = objConnection.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
          objCommand.Transaction = objTransaction;

          objCommand.CommandText = sbInsertDesktopQuery.ToString();
          objCommand.ExecuteNonQuery();

          objCommand.CommandText = "select @@identity";
          this.m_iID             = int.Parse(objCommand.ExecuteScalar().ToString());


          NTDzNewsCollection objAllNewsCollection = new NTDzNewsCollection();

          objAllNewsCollection.FillByDateTimeInterval(this.MainDate, this.MainDate);
          objAllNewsCollection.AnalyzeAndClear(this.InstrumentName);
            
          if (objAllNewsCollection.Count > 0)
          {
              foreach (NTDzNews objNTDzNews in objAllNewsCollection)
              {
                StringBuilder sbInsertChartQuery = new StringBuilder();

                sbInsertChartQuery.Append("INSERT INTO DZ_NEWS (DZID, NewsID) ");
                sbInsertChartQuery.Append("VALUES (" + this.m_iID.ToString() + "," + objNTDzNews.NewsID.ToString() + ")");

                objCommand.CommandText = sbInsertChartQuery.ToString();
                objCommand.ExecuteNonQuery();
              }
          }


          objTransaction.Commit();

          return true;
        }
        catch(Exception ex)
        {
          try
          {
            Program.ShowErrorMessage(ex.Message);
            objTransaction.Rollback();
          }
          catch
          { 
          }
        }
      }
 
      return false;
    }

    public bool Regenerate()
    {
      try
      {
      }
      catch (Exception ex)
      {
      }

      return false;
    }

    public bool Update()
    {
      try
      {
      }
      catch (Exception ex)
      {
      }

      return false;
    }

    public bool Delete()
    {
      try
      {
      }
      catch (Exception ex)
      {
      }

      return false;
    }

    public Int32 ID
    {
      get { return m_iID; } set { m_iID = value; }
    }

    public Int32 InstrumentID
    {
      get { return m_iInstrumentID; } set { m_iInstrumentID = value; }
    }

    public String InstrumentName
    {
      get { return m_strInstrumentName; } set { m_strInstrumentName = value; }
    }

    public Double LevelPrice
    {
      get { return m_dblLevelPrice; } set { m_dblLevelPrice = value; }
    }

    public Enums.DZPosition DZPosition
    {
      get { return m_enmDZPosition; } set { m_enmDZPosition = value; }
    }

    public Enums.DZDirection DZDirection
    {
      get { return m_enmDZDirection; } set { m_enmDZDirection = value; }
    }

    public Enums.InstrumentRisk InstrumentRisk
    {
      get { return m_enmInstrumentRisk; } set { m_enmInstrumentRisk = value; }
    }

    public NTDzNewsCollection NTDZNews
    {
      get { return m_objNTDZNews; } set { m_objNTDZNews = value; }
    }

    public DateTime MainDate
    {
      get { return m_dtMainDate; } set { m_dtMainDate = value; }
    }

    public DateTime EntryDatetime
    {
      get { return m_dtEntryDatetime; } set { m_dtEntryDatetime = value; }
    }

    public DateTime ExitDatetime
    {
      get { return m_dtExitDatetime; } set { m_dtExitDatetime = value; }
    }

    public Double EntryPrice
    {
      get { return m_dblEntryPrice; } set { m_dblEntryPrice = value; }
    }

    public Double ExitPrice
    {
      get { return m_dblExitPrice; } set { m_dblExitPrice = value; }
    }

    public Int32 Quantity
    {
      get { return m_iQuantity; } set { m_iQuantity = value; }
    }

    public String SelectScreenFile
    {
      get { return m_strSelectScreenFile; } set { m_strSelectScreenFile = value; }
    }

    public String Comment
    {
      get { return m_strComment; } set { m_strComment = value; }
    }

  }
}
