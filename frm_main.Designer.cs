﻿namespace NinjaTraderCustomWorkspaces
{
  partial class frm_main
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_main));
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
      this.btnFill = new System.Windows.Forms.Button();
      this.lstInstruments = new System.Windows.Forms.ListBox();
      this.tabControl = new System.Windows.Forms.TabControl();
      this.tabPageBase = new System.Windows.Forms.TabPage();
      this.chkAllInstruments = new System.Windows.Forms.CheckBox();
      this.btnReloadChart = new System.Windows.Forms.Button();
      this.rbnStandart6 = new System.Windows.Forms.RadioButton();
      this.rbnStandart5 = new System.Windows.Forms.RadioButton();
      this.rbnStandart4 = new System.Windows.Forms.RadioButton();
      this.rbnWEEKFullScreen = new System.Windows.Forms.RadioButton();
      this.rbnStandart3 = new System.Windows.Forms.RadioButton();
      this.rbn1440MinFullScreen = new System.Windows.Forms.RadioButton();
      this.btnView60MinChart = new System.Windows.Forms.Button();
      this.chkHomework = new System.Windows.Forms.CheckBox();
      this.btnDayChart = new System.Windows.Forms.Button();
      this.btnControlCenter = new System.Windows.Forms.Button();
      this.btnAlerts = new System.Windows.Forms.Button();
      this.rbnStandart2 = new System.Windows.Forms.RadioButton();
      this.rbn60MinAnd1440Min = new System.Windows.Forms.RadioButton();
      this.grbNTProcessPrioritet = new System.Windows.Forms.GroupBox();
      this.rbnNTPrioritetNormal = new System.Windows.Forms.RadioButton();
      this.rbnNTPrioritetLow = new System.Windows.Forms.RadioButton();
      this.rbnFullScreens60MinRight5MinLeft = new System.Windows.Forms.RadioButton();
      this.rbnStandart = new System.Windows.Forms.RadioButton();
      this.rbn5MinFullScreen = new System.Windows.Forms.RadioButton();
      this.groupBox3 = new System.Windows.Forms.GroupBox();
      this.rbnRemote = new System.Windows.Forms.RadioButton();
      this.rbnMain = new System.Windows.Forms.RadioButton();
      this.rbn5MinLeftFootRight = new System.Windows.Forms.RadioButton();
      this.rbnFootLeft5MinRight = new System.Windows.Forms.RadioButton();
      this.rbn60MinFullScreen = new System.Windows.Forms.RadioButton();
      this.chkAlertMessage = new System.Windows.Forms.CheckBox();
      this.rbn5MinAnd60Min = new System.Windows.Forms.RadioButton();
      this.chkAlertSound = new System.Windows.Forms.CheckBox();
      this.rbnFootRight5MinLeftOneScreen = new System.Windows.Forms.RadioButton();
      this.btnAlert = new System.Windows.Forms.Button();
      this.rbn5Min60minLeftFootRightOneScreen = new System.Windows.Forms.RadioButton();
      this.grbTime = new System.Windows.Forms.GroupBox();
      this.lblCurrentTime = new System.Windows.Forms.Label();
      this.rbnFullScreens60MinLeft5MinRight = new System.Windows.Forms.RadioButton();
      this.grbMainRule = new System.Windows.Forms.GroupBox();
      this.btnDz = new System.Windows.Forms.Button();
      this.label10 = new System.Windows.Forms.Label();
      this.lblMainRule = new System.Windows.Forms.Label();
      this.label9 = new System.Windows.Forms.Label();
      this.lstWorkspaces = new System.Windows.Forms.ListBox();
      this.btnHide = new System.Windows.Forms.Button();
      this.label1 = new System.Windows.Forms.Label();
      this.tabPageDesktops = new System.Windows.Forms.TabPage();
      this.btnSaveDesktopAsIs = new System.Windows.Forms.Button();
      this.btnShowSelectedWorkspace = new System.Windows.Forms.Button();
      this.btnShowSelectedDesktop = new System.Windows.Forms.Button();
      this.lstConfigurationWorkspaces = new System.Windows.Forms.ListBox();
      this.btnDesktopDeleteRowsAndCells = new System.Windows.Forms.Button();
      this.lblChartLocation = new System.Windows.Forms.Label();
      this.lblChartLocationText = new System.Windows.Forms.Label();
      this.btnClear = new System.Windows.Forms.Button();
      this.btnNewWorkspace = new System.Windows.Forms.Button();
      this.btnViewDesktop = new System.Windows.Forms.Button();
      this.btnUpdateWorkspace = new System.Windows.Forms.Button();
      this.btnDeleteWorkspace = new System.Windows.Forms.Button();
      this.btnSaveDesktop = new System.Windows.Forms.Button();
      this.btnDeleteDesktop = new System.Windows.Forms.Button();
      this.txtConfigurationWorkspace = new System.Windows.Forms.TextBox();
      this.label6 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.lstConfigurationDesktops = new System.Windows.Forms.ListBox();
      this.lstConfigurationInstruments = new System.Windows.Forms.ListBox();
      this.btnFujitsu2 = new System.Windows.Forms.Button();
      this.btnLenovo1 = new System.Windows.Forms.Button();
      this.btnLenovo2 = new System.Windows.Forms.Button();
      this.label3 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.btnFujitsu1 = new System.Windows.Forms.Button();
      this.btnDeleteColomn = new System.Windows.Forms.Button();
      this.btnDeleteRow = new System.Windows.Forms.Button();
      this.btnAddColomn = new System.Windows.Forms.Button();
      this.btnAddRow = new System.Windows.Forms.Button();
      this.grdDesktop = new System.Windows.Forms.DataGridView();
      this.tabPageNews = new System.Windows.Forms.TabPage();
      this.splitContainer4 = new System.Windows.Forms.SplitContainer();
      this.grbImportantNews = new System.Windows.Forms.GroupBox();
      this.grdImportantNews = new System.Windows.Forms.DataGridView();
      this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.tbl_ImportantNewsBindingSource = new System.Windows.Forms.BindingSource(this.components);
      this.ninjaTraderCustomWorkspacesDataSet2 = new NinjaTraderCustomWorkspaces.NinjaTraderCustomWorkspacesDataSet2();
      this.lblMinutesBefore = new System.Windows.Forms.Label();
      this.nmrMinutesBefore = new System.Windows.Forms.NumericUpDown();
      this.dtpicNewsTime = new System.Windows.Forms.DateTimePicker();
      this.dtpicNewsDate = new System.Windows.Forms.DateTimePicker();
      this.lblNe = new System.Windows.Forms.Label();
      this.tbl_ImportantNewsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
      this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
      this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
      this.toolStripButton9 = new System.Windows.Forms.ToolStripButton();
      this.toolStripButton10 = new System.Windows.Forms.ToolStripButton();
      this.toolStripButton11 = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
      this.toolStripTextBox2 = new System.Windows.Forms.ToolStripTextBox();
      this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
      this.toolStripButton12 = new System.Windows.Forms.ToolStripButton();
      this.toolStripButton13 = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
      this.tbl_ImportantNewsBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
      this.tabPageATR = new System.Windows.Forms.TabPage();
      this.btnPrepearForNTIndicatorSoloATR1Percents = new System.Windows.Forms.Button();
      this.lblATRInfo = new System.Windows.Forms.Label();
      this.txtATR = new System.Windows.Forms.TextBox();
      this.tabPageInfo = new System.Windows.Forms.TabPage();
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      this.tbl_InstrumentInfoDataGridView = new System.Windows.Forms.DataGridView();
      this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.tbl_InstrumentInfoBindingSource = new System.Windows.Forms.BindingSource(this.components);
      this.ninjaTraderCustomWorkspacesDataSet = new NinjaTraderCustomWorkspaces.NinjaTraderCustomWorkspacesDataSet();
      this.tbl_InstrumentInfoBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
      this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
      this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
      this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
      this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
      this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
      this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
      this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
      this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
      this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
      this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
      this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
      this.tbl_InstrumentInfoBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
      this.tabPageAlerts = new System.Windows.Forms.TabPage();
      this.splitContainer2 = new System.Windows.Forms.SplitContainer();
      this.splitContainer3 = new System.Windows.Forms.SplitContainer();
      this.grbAlerts = new System.Windows.Forms.GroupBox();
      this.grdAlerts = new System.Windows.Forms.DataGridView();
      this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
      this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.tbl_AlertsBindingSource = new System.Windows.Forms.BindingSource(this.components);
      this.ninjaTraderCustomWorkspacesDataSet1 = new NinjaTraderCustomWorkspaces.NinjaTraderCustomWorkspacesDataSet1();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.button1 = new System.Windows.Forms.Button();
      this.grdNTMarketData = new System.Windows.Forms.DataGridView();
      this.clInstrument = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.clPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.tbl_AlertsBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
      this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
      this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
      this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
      this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
      this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
      this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
      this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
      this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
      this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
      this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
      this.tabPageScreens = new System.Windows.Forms.TabPage();
      this.pnlDZInstruments = new System.Windows.Forms.Panel();
      this.btnChartsStatistic = new System.Windows.Forms.Button();
      this.bntDZListForCode = new System.Windows.Forms.Button();
      this.lblDZScreensTitle = new System.Windows.Forms.Label();
      this.panel1 = new System.Windows.Forms.Panel();
      this.chkDZ60mAnd1440m = new System.Windows.Forms.CheckBox();
      this.btnPrepareDZ = new System.Windows.Forms.Button();
      this.btnSaveResult = new System.Windows.Forms.Button();
      this.btnSaveDZ = new System.Windows.Forms.Button();
      this.btnPrepareResult = new System.Windows.Forms.Button();
      this.tbl_InstrumentInfoTableAdapter = new NinjaTraderCustomWorkspaces.NinjaTraderCustomWorkspacesDataSetTableAdapters.tbl_InstrumentInfoTableAdapter();
      this.tableAdapterManager = new NinjaTraderCustomWorkspaces.NinjaTraderCustomWorkspacesDataSetTableAdapters.TableAdapterManager();
      this.tbl_AlertsTableAdapter = new NinjaTraderCustomWorkspaces.NinjaTraderCustomWorkspacesDataSet1TableAdapters.tbl_AlertsTableAdapter();
      this.tableAdapterManager1 = new NinjaTraderCustomWorkspaces.NinjaTraderCustomWorkspacesDataSet1TableAdapters.TableAdapterManager();
      this.tmCurrentTime = new System.Windows.Forms.Timer(this.components);
      this.tmNTMarketData = new System.Windows.Forms.Timer(this.components);
      this.tmAlert = new System.Windows.Forms.Timer(this.components);
      this.tbl_ImportantNewsTableAdapter = new NinjaTraderCustomWorkspaces.NinjaTraderCustomWorkspacesDataSet2TableAdapters.tbl_ImportantNewsTableAdapter();
      this.tableAdapterManager2 = new NinjaTraderCustomWorkspaces.NinjaTraderCustomWorkspacesDataSet2TableAdapters.TableAdapterManager();
      this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
      this.tabControl.SuspendLayout();
      this.tabPageBase.SuspendLayout();
      this.grbNTProcessPrioritet.SuspendLayout();
      this.groupBox3.SuspendLayout();
      this.grbTime.SuspendLayout();
      this.grbMainRule.SuspendLayout();
      this.tabPageDesktops.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.grdDesktop)).BeginInit();
      this.tabPageNews.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
      this.splitContainer4.Panel1.SuspendLayout();
      this.splitContainer4.Panel2.SuspendLayout();
      this.splitContainer4.SuspendLayout();
      this.grbImportantNews.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.grdImportantNews)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.tbl_ImportantNewsBindingSource)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.ninjaTraderCustomWorkspacesDataSet2)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nmrMinutesBefore)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.tbl_ImportantNewsBindingNavigator)).BeginInit();
      this.tbl_ImportantNewsBindingNavigator.SuspendLayout();
      this.tabPageATR.SuspendLayout();
      this.tabPageInfo.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.tbl_InstrumentInfoDataGridView)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.tbl_InstrumentInfoBindingSource)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.ninjaTraderCustomWorkspacesDataSet)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.tbl_InstrumentInfoBindingNavigator)).BeginInit();
      this.tbl_InstrumentInfoBindingNavigator.SuspendLayout();
      this.tabPageAlerts.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
      this.splitContainer2.Panel1.SuspendLayout();
      this.splitContainer2.Panel2.SuspendLayout();
      this.splitContainer2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
      this.splitContainer3.Panel1.SuspendLayout();
      this.splitContainer3.Panel2.SuspendLayout();
      this.splitContainer3.SuspendLayout();
      this.grbAlerts.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.grdAlerts)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.tbl_AlertsBindingSource)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.ninjaTraderCustomWorkspacesDataSet1)).BeginInit();
      this.groupBox1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.grdNTMarketData)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.tbl_AlertsBindingNavigator)).BeginInit();
      this.tbl_AlertsBindingNavigator.SuspendLayout();
      this.tabPageScreens.SuspendLayout();
      this.pnlDZInstruments.SuspendLayout();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // btnFill
      // 
      this.btnFill.Location = new System.Drawing.Point(263, 374);
      this.btnFill.Name = "btnFill";
      this.btnFill.Size = new System.Drawing.Size(37, 41);
      this.btnFill.TabIndex = 0;
      this.btnFill.Text = "FILL";
      this.btnFill.UseVisualStyleBackColor = true;
      this.btnFill.Click += new System.EventHandler(this.btnFill_Click);
      // 
      // lstInstruments
      // 
      this.lstInstruments.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.lstInstruments.ForeColor = System.Drawing.SystemColors.WindowText;
      this.lstInstruments.FormattingEnabled = true;
      this.lstInstruments.ItemHeight = 16;
      this.lstInstruments.Location = new System.Drawing.Point(12, 103);
      this.lstInstruments.Name = "lstInstruments";
      this.lstInstruments.Size = new System.Drawing.Size(107, 372);
      this.lstInstruments.TabIndex = 1;
      this.lstInstruments.SelectedIndexChanged += new System.EventHandler(this.lstInstruments_SelectedIndexChanged);
      // 
      // tabControl
      // 
      this.tabControl.Controls.Add(this.tabPageBase);
      this.tabControl.Controls.Add(this.tabPageDesktops);
      this.tabControl.Controls.Add(this.tabPageNews);
      this.tabControl.Controls.Add(this.tabPageATR);
      this.tabControl.Controls.Add(this.tabPageInfo);
      this.tabControl.Controls.Add(this.tabPageAlerts);
      this.tabControl.Controls.Add(this.tabPageScreens);
      this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tabControl.Location = new System.Drawing.Point(0, 0);
      this.tabControl.Name = "tabControl";
      this.tabControl.SelectedIndex = 0;
      this.tabControl.Size = new System.Drawing.Size(778, 513);
      this.tabControl.TabIndex = 2;
      this.tabControl.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl_Selected);
      // 
      // tabPageBase
      // 
      this.tabPageBase.Controls.Add(this.chkAllInstruments);
      this.tabPageBase.Controls.Add(this.btnReloadChart);
      this.tabPageBase.Controls.Add(this.rbnStandart6);
      this.tabPageBase.Controls.Add(this.rbnStandart5);
      this.tabPageBase.Controls.Add(this.rbnStandart4);
      this.tabPageBase.Controls.Add(this.rbnWEEKFullScreen);
      this.tabPageBase.Controls.Add(this.rbnStandart3);
      this.tabPageBase.Controls.Add(this.rbn1440MinFullScreen);
      this.tabPageBase.Controls.Add(this.btnView60MinChart);
      this.tabPageBase.Controls.Add(this.chkHomework);
      this.tabPageBase.Controls.Add(this.btnDayChart);
      this.tabPageBase.Controls.Add(this.btnControlCenter);
      this.tabPageBase.Controls.Add(this.btnAlerts);
      this.tabPageBase.Controls.Add(this.rbnStandart2);
      this.tabPageBase.Controls.Add(this.rbn60MinAnd1440Min);
      this.tabPageBase.Controls.Add(this.grbNTProcessPrioritet);
      this.tabPageBase.Controls.Add(this.rbnFullScreens60MinRight5MinLeft);
      this.tabPageBase.Controls.Add(this.rbnStandart);
      this.tabPageBase.Controls.Add(this.rbn5MinFullScreen);
      this.tabPageBase.Controls.Add(this.groupBox3);
      this.tabPageBase.Controls.Add(this.rbn5MinLeftFootRight);
      this.tabPageBase.Controls.Add(this.rbnFootLeft5MinRight);
      this.tabPageBase.Controls.Add(this.rbn60MinFullScreen);
      this.tabPageBase.Controls.Add(this.chkAlertMessage);
      this.tabPageBase.Controls.Add(this.rbn5MinAnd60Min);
      this.tabPageBase.Controls.Add(this.chkAlertSound);
      this.tabPageBase.Controls.Add(this.rbnFootRight5MinLeftOneScreen);
      this.tabPageBase.Controls.Add(this.btnAlert);
      this.tabPageBase.Controls.Add(this.rbn5Min60minLeftFootRightOneScreen);
      this.tabPageBase.Controls.Add(this.grbTime);
      this.tabPageBase.Controls.Add(this.rbnFullScreens60MinLeft5MinRight);
      this.tabPageBase.Controls.Add(this.grbMainRule);
      this.tabPageBase.Controls.Add(this.lstWorkspaces);
      this.tabPageBase.Controls.Add(this.btnHide);
      this.tabPageBase.Controls.Add(this.label1);
      this.tabPageBase.Controls.Add(this.lstInstruments);
      this.tabPageBase.Controls.Add(this.btnFill);
      this.tabPageBase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.tabPageBase.Location = new System.Drawing.Point(4, 22);
      this.tabPageBase.Name = "tabPageBase";
      this.tabPageBase.Padding = new System.Windows.Forms.Padding(3);
      this.tabPageBase.Size = new System.Drawing.Size(770, 487);
      this.tabPageBase.TabIndex = 0;
      this.tabPageBase.Text = "BASE";
      this.tabPageBase.UseVisualStyleBackColor = true;
      // 
      // chkAllInstruments
      // 
      this.chkAllInstruments.AutoSize = true;
      this.chkAllInstruments.Location = new System.Drawing.Point(306, 374);
      this.chkAllInstruments.Name = "chkAllInstruments";
      this.chkAllInstruments.Size = new System.Drawing.Size(69, 17);
      this.chkAllInstruments.TabIndex = 52;
      this.chkAllInstruments.Text = "All reload";
      this.chkAllInstruments.UseVisualStyleBackColor = true;
      // 
      // btnReloadChart
      // 
      this.btnReloadChart.BackColor = System.Drawing.Color.RoyalBlue;
      this.btnReloadChart.ForeColor = System.Drawing.Color.White;
      this.btnReloadChart.Location = new System.Drawing.Point(302, 390);
      this.btnReloadChart.Name = "btnReloadChart";
      this.btnReloadChart.Size = new System.Drawing.Size(71, 25);
      this.btnReloadChart.TabIndex = 51;
      this.btnReloadChart.Text = "ctrl + shift 9";
      this.btnReloadChart.UseVisualStyleBackColor = false;
      this.btnReloadChart.Click += new System.EventHandler(this.btnReloadChart_Click);
      // 
      // rbnStandart6
      // 
      this.rbnStandart6.AutoSize = true;
      this.rbnStandart6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.rbnStandart6.ForeColor = System.Drawing.Color.Blue;
      this.rbnStandart6.Location = new System.Drawing.Point(125, 197);
      this.rbnStandart6.Name = "rbnStandart6";
      this.rbnStandart6.Size = new System.Drawing.Size(132, 17);
      this.rbnStandart6.TabIndex = 50;
      this.rbnStandart6.Text = "Standart => (W+60+D)";
      this.rbnStandart6.UseVisualStyleBackColor = true;
      this.rbnStandart6.CheckedChanged += new System.EventHandler(this.rbnStandart6_CheckedChanged);
      // 
      // rbnStandart5
      // 
      this.rbnStandart5.AutoSize = true;
      this.rbnStandart5.Checked = true;
      this.rbnStandart5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.rbnStandart5.ForeColor = System.Drawing.Color.Blue;
      this.rbnStandart5.Location = new System.Drawing.Point(125, 179);
      this.rbnStandart5.Name = "rbnStandart5";
      this.rbnStandart5.Size = new System.Drawing.Size(147, 17);
      this.rbnStandart5.TabIndex = 49;
      this.rbnStandart5.TabStop = true;
      this.rbnStandart5.Text = "Standart => (D+5+60)";
      this.rbnStandart5.UseVisualStyleBackColor = true;
      this.rbnStandart5.CheckedChanged += new System.EventHandler(this.rbnStandart5_CheckedChanged);
      // 
      // rbnStandart4
      // 
      this.rbnStandart4.AutoSize = true;
      this.rbnStandart4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.rbnStandart4.ForeColor = System.Drawing.Color.Blue;
      this.rbnStandart4.Location = new System.Drawing.Point(125, 156);
      this.rbnStandart4.Name = "rbnStandart4";
      this.rbnStandart4.Size = new System.Drawing.Size(138, 17);
      this.rbnStandart4.TabIndex = 48;
      this.rbnStandart4.Text = "Standart <=> (W+60+D)";
      this.rbnStandart4.UseVisualStyleBackColor = true;
      this.rbnStandart4.CheckedChanged += new System.EventHandler(this.rbnStandart4_CheckedChanged);
      // 
      // rbnWEEKFullScreen
      // 
      this.rbnWEEKFullScreen.AutoSize = true;
      this.rbnWEEKFullScreen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.rbnWEEKFullScreen.ForeColor = System.Drawing.Color.Blue;
      this.rbnWEEKFullScreen.Location = new System.Drawing.Point(125, 345);
      this.rbnWEEKFullScreen.Name = "rbnWEEKFullScreen";
      this.rbnWEEKFullScreen.Size = new System.Drawing.Size(126, 17);
      this.rbnWEEKFullScreen.TabIndex = 47;
      this.rbnWEEKFullScreen.Text = "WEEK Full screen =>";
      this.rbnWEEKFullScreen.UseVisualStyleBackColor = true;
      this.rbnWEEKFullScreen.CheckedChanged += new System.EventHandler(this.rbnWEEKFullScreen_CheckedChanged);
      // 
      // rbnStandart3
      // 
      this.rbnStandart3.AutoSize = true;
      this.rbnStandart3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.rbnStandart3.ForeColor = System.Drawing.Color.Blue;
      this.rbnStandart3.Location = new System.Drawing.Point(125, 139);
      this.rbnStandart3.Name = "rbnStandart3";
      this.rbnStandart3.Size = new System.Drawing.Size(133, 17);
      this.rbnStandart3.TabIndex = 46;
      this.rbnStandart3.Text = "Standart <=> (D+5+60)";
      this.rbnStandart3.UseVisualStyleBackColor = true;
      this.rbnStandart3.CheckedChanged += new System.EventHandler(this.rbnStandart3_CheckedChanged);
      // 
      // rbn1440MinFullScreen
      // 
      this.rbn1440MinFullScreen.AutoSize = true;
      this.rbn1440MinFullScreen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.rbn1440MinFullScreen.ForeColor = System.Drawing.Color.Blue;
      this.rbn1440MinFullScreen.Location = new System.Drawing.Point(125, 326);
      this.rbn1440MinFullScreen.Name = "rbn1440MinFullScreen";
      this.rbn1440MinFullScreen.Size = new System.Drawing.Size(157, 17);
      this.rbn1440MinFullScreen.TabIndex = 45;
      this.rbn1440MinFullScreen.Text = "1440Min Full screen =>";
      this.rbn1440MinFullScreen.UseVisualStyleBackColor = true;
      this.rbn1440MinFullScreen.CheckedChanged += new System.EventHandler(this.rbn1440MinFullScreen_CheckedChanged);
      // 
      // btnView60MinChart
      // 
      this.btnView60MinChart.BackColor = System.Drawing.Color.LightGray;
      this.btnView60MinChart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnView60MinChart.Location = new System.Drawing.Point(529, 434);
      this.btnView60MinChart.Name = "btnView60MinChart";
      this.btnView60MinChart.Size = new System.Drawing.Size(77, 30);
      this.btnView60MinChart.TabIndex = 43;
      this.btnView60MinChart.Text = "60 Min";
      this.btnView60MinChart.UseVisualStyleBackColor = false;
      this.btnView60MinChart.Click += new System.EventHandler(this.btnView60MinChart_Click);
      // 
      // chkHomework
      // 
      this.chkHomework.AutoSize = true;
      this.chkHomework.Location = new System.Drawing.Point(529, 467);
      this.chkHomework.Name = "chkHomework";
      this.chkHomework.Size = new System.Drawing.Size(41, 17);
      this.chkHomework.TabIndex = 42;
      this.chkHomework.Text = "DZ";
      this.chkHomework.UseVisualStyleBackColor = true;
      this.chkHomework.CheckedChanged += new System.EventHandler(this.chkHomework_CheckedChanged);
      // 
      // btnDayChart
      // 
      this.btnDayChart.BackColor = System.Drawing.Color.LightGray;
      this.btnDayChart.Location = new System.Drawing.Point(263, 68);
      this.btnDayChart.Name = "btnDayChart";
      this.btnDayChart.Size = new System.Drawing.Size(35, 23);
      this.btnDayChart.TabIndex = 41;
      this.btnDayChart.Text = "D";
      this.btnDayChart.UseVisualStyleBackColor = false;
      this.btnDayChart.Click += new System.EventHandler(this.btnDayChart_Click);
      // 
      // btnControlCenter
      // 
      this.btnControlCenter.Location = new System.Drawing.Point(338, 68);
      this.btnControlCenter.Name = "btnControlCenter";
      this.btnControlCenter.Size = new System.Drawing.Size(35, 23);
      this.btnControlCenter.TabIndex = 40;
      this.btnControlCenter.Text = "C";
      this.btnControlCenter.UseVisualStyleBackColor = true;
      this.btnControlCenter.Click += new System.EventHandler(this.btnControlCenter_Click);
      // 
      // btnAlerts
      // 
      this.btnAlerts.Location = new System.Drawing.Point(302, 68);
      this.btnAlerts.Name = "btnAlerts";
      this.btnAlerts.Size = new System.Drawing.Size(35, 23);
      this.btnAlerts.TabIndex = 39;
      this.btnAlerts.Text = "A";
      this.btnAlerts.UseVisualStyleBackColor = true;
      this.btnAlerts.Click += new System.EventHandler(this.btnAlerts_Click);
      // 
      // rbnStandart2
      // 
      this.rbnStandart2.AutoSize = true;
      this.rbnStandart2.Location = new System.Drawing.Point(125, 122);
      this.rbnStandart2.Name = "rbnStandart2";
      this.rbnStandart2.Size = new System.Drawing.Size(132, 17);
      this.rbnStandart2.TabIndex = 38;
      this.rbnStandart2.Text = "Standart (5-60) <=> FP";
      this.rbnStandart2.UseVisualStyleBackColor = true;
      this.rbnStandart2.CheckedChanged += new System.EventHandler(this.rbnStandart2_CheckedChanged);
      // 
      // rbn60MinAnd1440Min
      // 
      this.rbn60MinAnd1440Min.AutoSize = true;
      this.rbn60MinAnd1440Min.Location = new System.Drawing.Point(125, 225);
      this.rbn60MinAnd1440Min.Name = "rbn60MinAnd1440Min";
      this.rbn60MinAnd1440Min.Size = new System.Drawing.Size(101, 17);
      this.rbn60MinAnd1440Min.TabIndex = 37;
      this.rbn60MinAnd1440Min.Text = "60m <=> 1440m";
      this.rbn60MinAnd1440Min.UseVisualStyleBackColor = true;
      this.rbn60MinAnd1440Min.CheckedChanged += new System.EventHandler(this.rbnStandartFor60List_CheckedChanged);
      // 
      // grbNTProcessPrioritet
      // 
      this.grbNTProcessPrioritet.Controls.Add(this.rbnNTPrioritetNormal);
      this.grbNTProcessPrioritet.Controls.Add(this.rbnNTPrioritetLow);
      this.grbNTProcessPrioritet.Location = new System.Drawing.Point(263, 421);
      this.grbNTProcessPrioritet.Name = "grbNTProcessPrioritet";
      this.grbNTProcessPrioritet.Size = new System.Drawing.Size(109, 60);
      this.grbNTProcessPrioritet.TabIndex = 36;
      this.grbNTProcessPrioritet.TabStop = false;
      this.grbNTProcessPrioritet.Text = "NT Process Prioritet";
      // 
      // rbnNTPrioritetNormal
      // 
      this.rbnNTPrioritetNormal.AutoSize = true;
      this.rbnNTPrioritetNormal.Checked = true;
      this.rbnNTPrioritetNormal.Location = new System.Drawing.Point(51, 33);
      this.rbnNTPrioritetNormal.Name = "rbnNTPrioritetNormal";
      this.rbnNTPrioritetNormal.Size = new System.Drawing.Size(58, 17);
      this.rbnNTPrioritetNormal.TabIndex = 37;
      this.rbnNTPrioritetNormal.TabStop = true;
      this.rbnNTPrioritetNormal.Text = "Normal";
      this.rbnNTPrioritetNormal.UseVisualStyleBackColor = true;
      this.rbnNTPrioritetNormal.CheckedChanged += new System.EventHandler(this.rbnNTPrioritetNormal_CheckedChanged);
      // 
      // rbnNTPrioritetLow
      // 
      this.rbnNTPrioritetLow.AutoSize = true;
      this.rbnNTPrioritetLow.Location = new System.Drawing.Point(6, 33);
      this.rbnNTPrioritetLow.Name = "rbnNTPrioritetLow";
      this.rbnNTPrioritetLow.Size = new System.Drawing.Size(45, 17);
      this.rbnNTPrioritetLow.TabIndex = 36;
      this.rbnNTPrioritetLow.Text = "Low";
      this.rbnNTPrioritetLow.UseVisualStyleBackColor = true;
      this.rbnNTPrioritetLow.CheckedChanged += new System.EventHandler(this.rbnNTPrioritetLow_CheckedChanged);
      // 
      // rbnFullScreens60MinRight5MinLeft
      // 
      this.rbnFullScreens60MinRight5MinLeft.AutoSize = true;
      this.rbnFullScreens60MinRight5MinLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.rbnFullScreens60MinRight5MinLeft.ForeColor = System.Drawing.SystemColors.ControlText;
      this.rbnFullScreens60MinRight5MinLeft.Location = new System.Drawing.Point(125, 261);
      this.rbnFullScreens60MinRight5MinLeft.Name = "rbnFullScreens60MinRight5MinLeft";
      this.rbnFullScreens60MinRight5MinLeft.Size = new System.Drawing.Size(107, 17);
      this.rbnFullScreens60MinRight5MinLeft.TabIndex = 34;
      this.rbnFullScreens60MinRight5MinLeft.Text = "5Min <==> 60Min";
      this.rbnFullScreens60MinRight5MinLeft.UseVisualStyleBackColor = true;
      this.rbnFullScreens60MinRight5MinLeft.CheckedChanged += new System.EventHandler(this.rbnFullScreens60MinRight5MinLeft_CheckedChanged);
      // 
      // rbnStandart
      // 
      this.rbnStandart.AutoSize = true;
      this.rbnStandart.Location = new System.Drawing.Point(125, 104);
      this.rbnStandart.Name = "rbnStandart";
      this.rbnStandart.Size = new System.Drawing.Size(117, 17);
      this.rbnStandart.TabIndex = 13;
      this.rbnStandart.Text = "Standart (60+5+FP)";
      this.rbnStandart.UseVisualStyleBackColor = true;
      this.rbnStandart.CheckedChanged += new System.EventHandler(this.rbnStandart_CheckedChanged);
      // 
      // rbn5MinFullScreen
      // 
      this.rbn5MinFullScreen.AutoSize = true;
      this.rbn5MinFullScreen.Location = new System.Drawing.Point(125, 290);
      this.rbn5MinFullScreen.Name = "rbn5MinFullScreen";
      this.rbn5MinFullScreen.Size = new System.Drawing.Size(117, 17);
      this.rbn5MinFullScreen.TabIndex = 6;
      this.rbn5MinFullScreen.Text = "5Min Full screen =>";
      this.rbn5MinFullScreen.UseVisualStyleBackColor = true;
      this.rbn5MinFullScreen.CheckedChanged += new System.EventHandler(this.rbn5MinFullScreen_CheckedChanged);
      // 
      // groupBox3
      // 
      this.groupBox3.Controls.Add(this.rbnRemote);
      this.groupBox3.Controls.Add(this.rbnMain);
      this.groupBox3.Location = new System.Drawing.Point(117, 69);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new System.Drawing.Size(131, 29);
      this.groupBox3.TabIndex = 33;
      this.groupBox3.TabStop = false;
      // 
      // rbnRemote
      // 
      this.rbnRemote.AutoSize = true;
      this.rbnRemote.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.rbnRemote.ForeColor = System.Drawing.SystemColors.ControlText;
      this.rbnRemote.Location = new System.Drawing.Point(61, 11);
      this.rbnRemote.Name = "rbnRemote";
      this.rbnRemote.Size = new System.Drawing.Size(68, 17);
      this.rbnRemote.TabIndex = 32;
      this.rbnRemote.Text = "Remote";
      this.rbnRemote.UseVisualStyleBackColor = true;
      this.rbnRemote.CheckedChanged += new System.EventHandler(this.rbnRemote_CheckedChanged);
      // 
      // rbnMain
      // 
      this.rbnMain.AutoSize = true;
      this.rbnMain.Checked = true;
      this.rbnMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.rbnMain.Location = new System.Drawing.Point(8, 11);
      this.rbnMain.Name = "rbnMain";
      this.rbnMain.Size = new System.Drawing.Size(52, 17);
      this.rbnMain.TabIndex = 31;
      this.rbnMain.TabStop = true;
      this.rbnMain.Text = "Main";
      this.rbnMain.UseVisualStyleBackColor = true;
      this.rbnMain.CheckedChanged += new System.EventHandler(this.rbnMain_CheckedChanged);
      // 
      // rbn5MinLeftFootRight
      // 
      this.rbn5MinLeftFootRight.AutoSize = true;
      this.rbn5MinLeftFootRight.Location = new System.Drawing.Point(125, 395);
      this.rbn5MinLeftFootRight.Name = "rbn5MinLeftFootRight";
      this.rbn5MinLeftFootRight.Size = new System.Drawing.Size(107, 17);
      this.rbn5MinLeftFootRight.TabIndex = 7;
      this.rbn5MinLeftFootRight.Text = "5Min <==> FOOT";
      this.rbn5MinLeftFootRight.UseVisualStyleBackColor = true;
      this.rbn5MinLeftFootRight.CheckedChanged += new System.EventHandler(this.rbn5MinLeftFootRight_CheckedChanged);
      // 
      // rbnFootLeft5MinRight
      // 
      this.rbnFootLeft5MinRight.AutoSize = true;
      this.rbnFootLeft5MinRight.Location = new System.Drawing.Point(125, 412);
      this.rbnFootLeft5MinRight.Name = "rbnFootLeft5MinRight";
      this.rbnFootLeft5MinRight.Size = new System.Drawing.Size(107, 17);
      this.rbnFootLeft5MinRight.TabIndex = 8;
      this.rbnFootLeft5MinRight.Text = "FOOT <==> 5Min";
      this.rbnFootLeft5MinRight.UseVisualStyleBackColor = true;
      this.rbnFootLeft5MinRight.CheckedChanged += new System.EventHandler(this.rbnFootLeft5MinRight_CheckedChanged);
      // 
      // rbn60MinFullScreen
      // 
      this.rbn60MinFullScreen.AutoSize = true;
      this.rbn60MinFullScreen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.rbn60MinFullScreen.ForeColor = System.Drawing.SystemColors.ControlText;
      this.rbn60MinFullScreen.Location = new System.Drawing.Point(125, 308);
      this.rbn60MinFullScreen.Name = "rbn60MinFullScreen";
      this.rbn60MinFullScreen.Size = new System.Drawing.Size(123, 17);
      this.rbn60MinFullScreen.TabIndex = 15;
      this.rbn60MinFullScreen.Text = "60Min Full screen =>";
      this.rbn60MinFullScreen.UseVisualStyleBackColor = true;
      this.rbn60MinFullScreen.CheckedChanged += new System.EventHandler(this.rbn60MinFullScreen_CheckedChanged);
      // 
      // chkAlertMessage
      // 
      this.chkAlertMessage.AutoSize = true;
      this.chkAlertMessage.Checked = true;
      this.chkAlertMessage.CheckState = System.Windows.Forms.CheckState.Checked;
      this.chkAlertMessage.Location = new System.Drawing.Point(582, 411);
      this.chkAlertMessage.Name = "chkAlertMessage";
      this.chkAlertMessage.Size = new System.Drawing.Size(69, 17);
      this.chkAlertMessage.TabIndex = 30;
      this.chkAlertMessage.Text = "Message";
      this.chkAlertMessage.UseVisualStyleBackColor = true;
      // 
      // rbn5MinAnd60Min
      // 
      this.rbn5MinAnd60Min.AutoSize = true;
      this.rbn5MinAnd60Min.Location = new System.Drawing.Point(125, 367);
      this.rbn5MinAnd60Min.Name = "rbn5MinAnd60Min";
      this.rbn5MinAnd60Min.Size = new System.Drawing.Size(121, 17);
      this.rbn5MinAnd60Min.TabIndex = 16;
      this.rbn5MinAnd60Min.Text = "5Min AND 60Min =>";
      this.rbn5MinAnd60Min.UseVisualStyleBackColor = true;
      this.rbn5MinAnd60Min.CheckedChanged += new System.EventHandler(this.rbn5MinAnd60Min_CheckedChanged);
      // 
      // chkAlertSound
      // 
      this.chkAlertSound.AutoSize = true;
      this.chkAlertSound.Enabled = false;
      this.chkAlertSound.Location = new System.Drawing.Point(582, 393);
      this.chkAlertSound.Name = "chkAlertSound";
      this.chkAlertSound.Size = new System.Drawing.Size(57, 17);
      this.chkAlertSound.TabIndex = 29;
      this.chkAlertSound.Text = "Sound";
      this.chkAlertSound.UseVisualStyleBackColor = true;
      // 
      // rbnFootRight5MinLeftOneScreen
      // 
      this.rbnFootRight5MinLeftOneScreen.AutoSize = true;
      this.rbnFootRight5MinLeftOneScreen.Location = new System.Drawing.Point(125, 437);
      this.rbnFootRight5MinLeftOneScreen.Name = "rbnFootRight5MinLeftOneScreen";
      this.rbnFootRight5MinLeftOneScreen.Size = new System.Drawing.Size(102, 17);
      this.rbnFootRight5MinLeftOneScreen.TabIndex = 18;
      this.rbnFootRight5MinLeftOneScreen.Text = "5Min || FOOT =>";
      this.rbnFootRight5MinLeftOneScreen.UseVisualStyleBackColor = true;
      this.rbnFootRight5MinLeftOneScreen.CheckedChanged += new System.EventHandler(this.rbnFootRight5MinLeftOneScreen_CheckedChanged);
      // 
      // btnAlert
      // 
      this.btnAlert.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnAlert.Location = new System.Drawing.Point(529, 393);
      this.btnAlert.Name = "btnAlert";
      this.btnAlert.Size = new System.Drawing.Size(47, 24);
      this.btnAlert.TabIndex = 28;
      this.btnAlert.Text = "Alert!";
      this.btnAlert.UseVisualStyleBackColor = true;
      this.btnAlert.Click += new System.EventHandler(this.btnAlert_Click);
      // 
      // rbn5Min60minLeftFootRightOneScreen
      // 
      this.rbn5Min60minLeftFootRightOneScreen.AutoSize = true;
      this.rbn5Min60minLeftFootRightOneScreen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.rbn5Min60minLeftFootRightOneScreen.ForeColor = System.Drawing.Color.Blue;
      this.rbn5Min60minLeftFootRightOneScreen.Location = new System.Drawing.Point(125, 454);
      this.rbn5Min60minLeftFootRightOneScreen.Name = "rbn5Min60minLeftFootRightOneScreen";
      this.rbn5Min60minLeftFootRightOneScreen.Size = new System.Drawing.Size(138, 17);
      this.rbn5Min60minLeftFootRightOneScreen.TabIndex = 19;
      this.rbn5Min60minLeftFootRightOneScreen.Text = "5M_60M || FOOT =>";
      this.rbn5Min60minLeftFootRightOneScreen.UseVisualStyleBackColor = true;
      this.rbn5Min60minLeftFootRightOneScreen.CheckedChanged += new System.EventHandler(this.rbn5Min60minLeftFootRightOneScreen_CheckedChanged);
      // 
      // grbTime
      // 
      this.grbTime.Controls.Add(this.lblCurrentTime);
      this.grbTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.grbTime.ForeColor = System.Drawing.Color.Red;
      this.grbTime.Location = new System.Drawing.Point(271, 7);
      this.grbTime.Name = "grbTime";
      this.grbTime.Size = new System.Drawing.Size(101, 59);
      this.grbTime.TabIndex = 27;
      this.grbTime.TabStop = false;
      this.grbTime.Text = "Время";
      // 
      // lblCurrentTime
      // 
      this.lblCurrentTime.AutoSize = true;
      this.lblCurrentTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.lblCurrentTime.ForeColor = System.Drawing.Color.Blue;
      this.lblCurrentTime.Location = new System.Drawing.Point(1, 21);
      this.lblCurrentTime.Name = "lblCurrentTime";
      this.lblCurrentTime.Size = new System.Drawing.Size(64, 26);
      this.lblCurrentTime.TabIndex = 26;
      this.lblCurrentTime.Text = "Time";
      this.lblCurrentTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // rbnFullScreens60MinLeft5MinRight
      // 
      this.rbnFullScreens60MinLeft5MinRight.AutoSize = true;
      this.rbnFullScreens60MinLeft5MinRight.Location = new System.Drawing.Point(125, 243);
      this.rbnFullScreens60MinLeft5MinRight.Name = "rbnFullScreens60MinLeft5MinRight";
      this.rbnFullScreens60MinLeft5MinRight.Size = new System.Drawing.Size(107, 17);
      this.rbnFullScreens60MinLeft5MinRight.TabIndex = 20;
      this.rbnFullScreens60MinLeft5MinRight.Text = "60Min <==> 5Min";
      this.rbnFullScreens60MinLeft5MinRight.UseVisualStyleBackColor = true;
      this.rbnFullScreens60MinLeft5MinRight.CheckedChanged += new System.EventHandler(this.rbnFullScreens60MinLeft5MinRight_CheckedChanged);
      // 
      // grbMainRule
      // 
      this.grbMainRule.Controls.Add(this.btnDz);
      this.grbMainRule.Controls.Add(this.label10);
      this.grbMainRule.Controls.Add(this.lblMainRule);
      this.grbMainRule.Controls.Add(this.label9);
      this.grbMainRule.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.grbMainRule.ForeColor = System.Drawing.Color.Red;
      this.grbMainRule.Location = new System.Drawing.Point(11, 7);
      this.grbMainRule.Name = "grbMainRule";
      this.grbMainRule.Size = new System.Drawing.Size(254, 62);
      this.grbMainRule.TabIndex = 25;
      this.grbMainRule.TabStop = false;
      this.grbMainRule.Text = "Главные правила";
      // 
      // btnDz
      // 
      this.btnDz.BackColor = System.Drawing.Color.LightGray;
      this.btnDz.ForeColor = System.Drawing.Color.ForestGreen;
      this.btnDz.Location = new System.Drawing.Point(209, 24);
      this.btnDz.Name = "btnDz";
      this.btnDz.Size = new System.Drawing.Size(45, 23);
      this.btnDz.TabIndex = 53;
      this.btnDz.Text = "DZ";
      this.btnDz.UseVisualStyleBackColor = false;
      this.btnDz.Click += new System.EventHandler(this.btnDz_Click);
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.BackColor = System.Drawing.Color.White;
      this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label10.ForeColor = System.Drawing.Color.DarkGreen;
      this.label10.Location = new System.Drawing.Point(6, 43);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(99, 13);
      this.label10.TabIndex = 25;
      this.label10.Text = "Я - лишь цифра";
      // 
      // lblMainRule
      // 
      this.lblMainRule.AutoSize = true;
      this.lblMainRule.BackColor = System.Drawing.Color.White;
      this.lblMainRule.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.lblMainRule.ForeColor = System.Drawing.Color.DarkGreen;
      this.lblMainRule.Location = new System.Drawing.Point(6, 14);
      this.lblMainRule.Name = "lblMainRule";
      this.lblMainRule.Size = new System.Drawing.Size(178, 13);
      this.lblMainRule.TabIndex = 21;
      this.lblMainRule.Text = "Я - оператор своей системы";
      this.lblMainRule.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.BackColor = System.Drawing.Color.White;
      this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label9.ForeColor = System.Drawing.Color.DarkGreen;
      this.label9.Location = new System.Drawing.Point(6, 29);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(166, 13);
      this.label9.TabIndex = 24;
      this.label9.Text = "Я покупаю и продаю РИСК";
      // 
      // lstWorkspaces
      // 
      this.lstWorkspaces.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.lstWorkspaces.ForeColor = System.Drawing.SystemColors.WindowText;
      this.lstWorkspaces.FormattingEnabled = true;
      this.lstWorkspaces.HorizontalScrollbar = true;
      this.lstWorkspaces.ItemHeight = 16;
      this.lstWorkspaces.Location = new System.Drawing.Point(263, 92);
      this.lstWorkspaces.Name = "lstWorkspaces";
      this.lstWorkspaces.Size = new System.Drawing.Size(109, 276);
      this.lstWorkspaces.TabIndex = 22;
      this.lstWorkspaces.SelectedIndexChanged += new System.EventHandler(this.lstWorkspaces_SelectedIndexChanged);
      // 
      // btnHide
      // 
      this.btnHide.Location = new System.Drawing.Point(529, 360);
      this.btnHide.Name = "btnHide";
      this.btnHide.Size = new System.Drawing.Size(39, 24);
      this.btnHide.TabIndex = 17;
      this.btnHide.Text = "Hide";
      this.btnHide.UseVisualStyleBackColor = true;
      this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label1.Location = new System.Drawing.Point(8, 80);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(101, 18);
      this.label1.TabIndex = 2;
      this.label1.Text = "Instruments:";
      // 
      // tabPageDesktops
      // 
      this.tabPageDesktops.Controls.Add(this.btnSaveDesktopAsIs);
      this.tabPageDesktops.Controls.Add(this.btnShowSelectedWorkspace);
      this.tabPageDesktops.Controls.Add(this.btnShowSelectedDesktop);
      this.tabPageDesktops.Controls.Add(this.lstConfigurationWorkspaces);
      this.tabPageDesktops.Controls.Add(this.btnDesktopDeleteRowsAndCells);
      this.tabPageDesktops.Controls.Add(this.lblChartLocation);
      this.tabPageDesktops.Controls.Add(this.lblChartLocationText);
      this.tabPageDesktops.Controls.Add(this.btnClear);
      this.tabPageDesktops.Controls.Add(this.btnNewWorkspace);
      this.tabPageDesktops.Controls.Add(this.btnViewDesktop);
      this.tabPageDesktops.Controls.Add(this.btnUpdateWorkspace);
      this.tabPageDesktops.Controls.Add(this.btnDeleteWorkspace);
      this.tabPageDesktops.Controls.Add(this.btnSaveDesktop);
      this.tabPageDesktops.Controls.Add(this.btnDeleteDesktop);
      this.tabPageDesktops.Controls.Add(this.txtConfigurationWorkspace);
      this.tabPageDesktops.Controls.Add(this.label6);
      this.tabPageDesktops.Controls.Add(this.label5);
      this.tabPageDesktops.Controls.Add(this.lstConfigurationDesktops);
      this.tabPageDesktops.Controls.Add(this.lstConfigurationInstruments);
      this.tabPageDesktops.Controls.Add(this.btnFujitsu2);
      this.tabPageDesktops.Controls.Add(this.btnLenovo1);
      this.tabPageDesktops.Controls.Add(this.btnLenovo2);
      this.tabPageDesktops.Controls.Add(this.label3);
      this.tabPageDesktops.Controls.Add(this.label2);
      this.tabPageDesktops.Controls.Add(this.btnFujitsu1);
      this.tabPageDesktops.Controls.Add(this.btnDeleteColomn);
      this.tabPageDesktops.Controls.Add(this.btnDeleteRow);
      this.tabPageDesktops.Controls.Add(this.btnAddColomn);
      this.tabPageDesktops.Controls.Add(this.btnAddRow);
      this.tabPageDesktops.Controls.Add(this.grdDesktop);
      this.tabPageDesktops.Location = new System.Drawing.Point(4, 22);
      this.tabPageDesktops.Name = "tabPageDesktops";
      this.tabPageDesktops.Padding = new System.Windows.Forms.Padding(3);
      this.tabPageDesktops.Size = new System.Drawing.Size(770, 487);
      this.tabPageDesktops.TabIndex = 1;
      this.tabPageDesktops.Text = "DESK";
      this.tabPageDesktops.UseVisualStyleBackColor = true;
      // 
      // btnSaveDesktopAsIs
      // 
      this.btnSaveDesktopAsIs.BackColor = System.Drawing.Color.ForestGreen;
      this.btnSaveDesktopAsIs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnSaveDesktopAsIs.ForeColor = System.Drawing.Color.White;
      this.btnSaveDesktopAsIs.Location = new System.Drawing.Point(477, 357);
      this.btnSaveDesktopAsIs.Name = "btnSaveDesktopAsIs";
      this.btnSaveDesktopAsIs.Size = new System.Drawing.Size(131, 30);
      this.btnSaveDesktopAsIs.TabIndex = 36;
      this.btnSaveDesktopAsIs.Text = "SAVE DESK AS IS";
      this.btnSaveDesktopAsIs.UseVisualStyleBackColor = false;
      // 
      // btnShowSelectedWorkspace
      // 
      this.btnShowSelectedWorkspace.BackColor = System.Drawing.Color.DodgerBlue;
      this.btnShowSelectedWorkspace.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnShowSelectedWorkspace.ForeColor = System.Drawing.Color.White;
      this.btnShowSelectedWorkspace.Location = new System.Drawing.Point(611, 3);
      this.btnShowSelectedWorkspace.Name = "btnShowSelectedWorkspace";
      this.btnShowSelectedWorkspace.Size = new System.Drawing.Size(148, 23);
      this.btnShowSelectedWorkspace.TabIndex = 35;
      this.btnShowSelectedWorkspace.Text = "SHOW WORKSPACE";
      this.btnShowSelectedWorkspace.UseVisualStyleBackColor = false;
      this.btnShowSelectedWorkspace.Click += new System.EventHandler(this.btnShowSelectedWorkspace_Click);
      // 
      // btnShowSelectedDesktop
      // 
      this.btnShowSelectedDesktop.BackColor = System.Drawing.Color.DodgerBlue;
      this.btnShowSelectedDesktop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnShowSelectedDesktop.ForeColor = System.Drawing.Color.White;
      this.btnShowSelectedDesktop.Location = new System.Drawing.Point(659, 321);
      this.btnShowSelectedDesktop.Name = "btnShowSelectedDesktop";
      this.btnShowSelectedDesktop.Size = new System.Drawing.Size(100, 30);
      this.btnShowSelectedDesktop.TabIndex = 34;
      this.btnShowSelectedDesktop.Text = "SHOW DESK";
      this.btnShowSelectedDesktop.UseVisualStyleBackColor = false;
      this.btnShowSelectedDesktop.Click += new System.EventHandler(this.btnShowSelectedDesktop_Click);
      // 
      // lstConfigurationWorkspaces
      // 
      this.lstConfigurationWorkspaces.FormattingEnabled = true;
      this.lstConfigurationWorkspaces.Location = new System.Drawing.Point(452, 27);
      this.lstConfigurationWorkspaces.Name = "lstConfigurationWorkspaces";
      this.lstConfigurationWorkspaces.Size = new System.Drawing.Size(307, 160);
      this.lstConfigurationWorkspaces.TabIndex = 33;
      this.lstConfigurationWorkspaces.Click += new System.EventHandler(this.lstConfigurationWorkspaces_Click);
      this.lstConfigurationWorkspaces.SelectedIndexChanged += new System.EventHandler(this.lstConfigurationWorkspaces_SelectedIndexChanged);
      // 
      // btnDesktopDeleteRowsAndCells
      // 
      this.btnDesktopDeleteRowsAndCells.BackColor = System.Drawing.Color.Red;
      this.btnDesktopDeleteRowsAndCells.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnDesktopDeleteRowsAndCells.ForeColor = System.Drawing.Color.White;
      this.btnDesktopDeleteRowsAndCells.Location = new System.Drawing.Point(288, 325);
      this.btnDesktopDeleteRowsAndCells.Name = "btnDesktopDeleteRowsAndCells";
      this.btnDesktopDeleteRowsAndCells.Size = new System.Drawing.Size(53, 23);
      this.btnDesktopDeleteRowsAndCells.TabIndex = 32;
      this.btnDesktopDeleteRowsAndCells.Text = "Del";
      this.btnDesktopDeleteRowsAndCells.UseVisualStyleBackColor = false;
      this.btnDesktopDeleteRowsAndCells.Click += new System.EventHandler(this.btnDesktopDeleteRowsAndCells_Click);
      // 
      // lblChartLocation
      // 
      this.lblChartLocation.AutoSize = true;
      this.lblChartLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.lblChartLocation.ForeColor = System.Drawing.Color.ForestGreen;
      this.lblChartLocation.Location = new System.Drawing.Point(9, 32);
      this.lblChartLocation.Name = "lblChartLocation";
      this.lblChartLocation.Size = new System.Drawing.Size(93, 13);
      this.lblChartLocation.TabIndex = 31;
      this.lblChartLocation.Text = "[Main][Remote]";
      // 
      // lblChartLocationText
      // 
      this.lblChartLocationText.AutoSize = true;
      this.lblChartLocationText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.lblChartLocationText.Location = new System.Drawing.Point(9, 17);
      this.lblChartLocationText.Name = "lblChartLocationText";
      this.lblChartLocationText.Size = new System.Drawing.Size(94, 13);
      this.lblChartLocationText.TabIndex = 30;
      this.lblChartLocationText.Text = "Chart location :";
      // 
      // btnClear
      // 
      this.btnClear.BackColor = System.Drawing.Color.OldLace;
      this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnClear.Location = new System.Drawing.Point(229, 325);
      this.btnClear.Name = "btnClear";
      this.btnClear.Size = new System.Drawing.Size(53, 23);
      this.btnClear.TabIndex = 29;
      this.btnClear.Text = "Clear";
      this.btnClear.UseVisualStyleBackColor = false;
      this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
      // 
      // btnNewWorkspace
      // 
      this.btnNewWorkspace.BackColor = System.Drawing.Color.DodgerBlue;
      this.btnNewWorkspace.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnNewWorkspace.ForeColor = System.Drawing.Color.White;
      this.btnNewWorkspace.Location = new System.Drawing.Point(611, 191);
      this.btnNewWorkspace.Name = "btnNewWorkspace";
      this.btnNewWorkspace.Size = new System.Drawing.Size(44, 23);
      this.btnNewWorkspace.TabIndex = 27;
      this.btnNewWorkspace.Text = "New";
      this.btnNewWorkspace.UseVisualStyleBackColor = false;
      this.btnNewWorkspace.Click += new System.EventHandler(this.btnNewWorkspace_Click);
      // 
      // btnViewDesktop
      // 
      this.btnViewDesktop.BackColor = System.Drawing.Color.LimeGreen;
      this.btnViewDesktop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnViewDesktop.ForeColor = System.Drawing.Color.White;
      this.btnViewDesktop.Location = new System.Drawing.Point(229, 289);
      this.btnViewDesktop.Name = "btnViewDesktop";
      this.btnViewDesktop.Size = new System.Drawing.Size(112, 33);
      this.btnViewDesktop.TabIndex = 24;
      this.btnViewDesktop.Text = "PREVIEW DESK";
      this.btnViewDesktop.UseVisualStyleBackColor = false;
      this.btnViewDesktop.Click += new System.EventHandler(this.btnViewDesktop_Click);
      // 
      // btnUpdateWorkspace
      // 
      this.btnUpdateWorkspace.BackColor = System.Drawing.Color.LimeGreen;
      this.btnUpdateWorkspace.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnUpdateWorkspace.ForeColor = System.Drawing.Color.White;
      this.btnUpdateWorkspace.Location = new System.Drawing.Point(657, 191);
      this.btnUpdateWorkspace.Name = "btnUpdateWorkspace";
      this.btnUpdateWorkspace.Size = new System.Drawing.Size(56, 23);
      this.btnUpdateWorkspace.TabIndex = 23;
      this.btnUpdateWorkspace.Text = "Update";
      this.btnUpdateWorkspace.UseVisualStyleBackColor = false;
      this.btnUpdateWorkspace.Click += new System.EventHandler(this.btnUpdateWorkspace_Click);
      // 
      // btnDeleteWorkspace
      // 
      this.btnDeleteWorkspace.BackColor = System.Drawing.Color.Red;
      this.btnDeleteWorkspace.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnDeleteWorkspace.ForeColor = System.Drawing.Color.White;
      this.btnDeleteWorkspace.Location = new System.Drawing.Point(715, 191);
      this.btnDeleteWorkspace.Name = "btnDeleteWorkspace";
      this.btnDeleteWorkspace.Size = new System.Drawing.Size(44, 23);
      this.btnDeleteWorkspace.TabIndex = 22;
      this.btnDeleteWorkspace.Text = "Del";
      this.btnDeleteWorkspace.UseVisualStyleBackColor = false;
      this.btnDeleteWorkspace.Click += new System.EventHandler(this.btnDeleteWorkspace_Click);
      // 
      // btnSaveDesktop
      // 
      this.btnSaveDesktop.BackColor = System.Drawing.Color.LimeGreen;
      this.btnSaveDesktop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnSaveDesktop.ForeColor = System.Drawing.Color.White;
      this.btnSaveDesktop.Location = new System.Drawing.Point(452, 321);
      this.btnSaveDesktop.Name = "btnSaveDesktop";
      this.btnSaveDesktop.Size = new System.Drawing.Size(88, 30);
      this.btnSaveDesktop.TabIndex = 21;
      this.btnSaveDesktop.Text = "SAVE DESK";
      this.btnSaveDesktop.UseVisualStyleBackColor = false;
      this.btnSaveDesktop.Click += new System.EventHandler(this.btnSaveDesktop_Click);
      // 
      // btnDeleteDesktop
      // 
      this.btnDeleteDesktop.BackColor = System.Drawing.Color.Red;
      this.btnDeleteDesktop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnDeleteDesktop.ForeColor = System.Drawing.Color.White;
      this.btnDeleteDesktop.Location = new System.Drawing.Point(542, 321);
      this.btnDeleteDesktop.Name = "btnDeleteDesktop";
      this.btnDeleteDesktop.Size = new System.Drawing.Size(88, 30);
      this.btnDeleteDesktop.TabIndex = 20;
      this.btnDeleteDesktop.Text = "DEL DESK";
      this.btnDeleteDesktop.UseVisualStyleBackColor = false;
      this.btnDeleteDesktop.Click += new System.EventHandler(this.btnDeleteDesktop_Click);
      // 
      // txtConfigurationWorkspace
      // 
      this.txtConfigurationWorkspace.Location = new System.Drawing.Point(452, 192);
      this.txtConfigurationWorkspace.Name = "txtConfigurationWorkspace";
      this.txtConfigurationWorkspace.Size = new System.Drawing.Size(156, 20);
      this.txtConfigurationWorkspace.TabIndex = 17;
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label6.Location = new System.Drawing.Point(449, 10);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(81, 13);
      this.label6.TabIndex = 15;
      this.label6.Text = "Workspaces:";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label5.Location = new System.Drawing.Point(449, 218);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(64, 13);
      this.label5.TabIndex = 15;
      this.label5.Text = "Desktops:";
      // 
      // lstConfigurationDesktops
      // 
      this.lstConfigurationDesktops.FormattingEnabled = true;
      this.lstConfigurationDesktops.Location = new System.Drawing.Point(452, 234);
      this.lstConfigurationDesktops.Name = "lstConfigurationDesktops";
      this.lstConfigurationDesktops.Size = new System.Drawing.Size(307, 82);
      this.lstConfigurationDesktops.TabIndex = 14;
      this.lstConfigurationDesktops.SelectedIndexChanged += new System.EventHandler(this.lstConfigurationDesktops_SelectedIndexChanged);
      // 
      // lstConfigurationInstruments
      // 
      this.lstConfigurationInstruments.FormattingEnabled = true;
      this.lstConfigurationInstruments.Location = new System.Drawing.Point(8, 54);
      this.lstConfigurationInstruments.Name = "lstConfigurationInstruments";
      this.lstConfigurationInstruments.Size = new System.Drawing.Size(121, 290);
      this.lstConfigurationInstruments.TabIndex = 12;
      this.lstConfigurationInstruments.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.lstConfigurationInstruments_DrawItem);
      // 
      // btnFujitsu2
      // 
      this.btnFujitsu2.BackColor = System.Drawing.Color.DarkGray;
      this.btnFujitsu2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnFujitsu2.ForeColor = System.Drawing.Color.White;
      this.btnFujitsu2.Location = new System.Drawing.Point(339, 49);
      this.btnFujitsu2.Name = "btnFujitsu2";
      this.btnFujitsu2.Size = new System.Drawing.Size(92, 48);
      this.btnFujitsu2.TabIndex = 11;
      this.btnFujitsu2.Text = "Fujitsu 2";
      this.btnFujitsu2.UseVisualStyleBackColor = false;
      this.btnFujitsu2.Click += new System.EventHandler(this.btnFujitsu2_Click);
      // 
      // btnLenovo1
      // 
      this.btnLenovo1.BackColor = System.Drawing.Color.DarkGray;
      this.btnLenovo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnLenovo1.ForeColor = System.Drawing.Color.White;
      this.btnLenovo1.Location = new System.Drawing.Point(238, 27);
      this.btnLenovo1.Name = "btnLenovo1";
      this.btnLenovo1.Size = new System.Drawing.Size(91, 31);
      this.btnLenovo1.TabIndex = 10;
      this.btnLenovo1.Text = "Lenovo 1";
      this.btnLenovo1.UseVisualStyleBackColor = false;
      this.btnLenovo1.Click += new System.EventHandler(this.btnLenovo1_Click);
      // 
      // btnLenovo2
      // 
      this.btnLenovo2.BackColor = System.Drawing.Color.DarkGray;
      this.btnLenovo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnLenovo2.ForeColor = System.Drawing.Color.White;
      this.btnLenovo2.Location = new System.Drawing.Point(137, 49);
      this.btnLenovo2.Name = "btnLenovo2";
      this.btnLenovo2.Size = new System.Drawing.Size(90, 48);
      this.btnLenovo2.TabIndex = 9;
      this.btnLenovo2.Text = "Lenovo 2";
      this.btnLenovo2.UseVisualStyleBackColor = false;
      this.btnLenovo2.Click += new System.EventHandler(this.btnLenovo2_Click);
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label3.Location = new System.Drawing.Point(135, 293);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(42, 13);
      this.label3.TabIndex = 7;
      this.label3.Text = "Rows:";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.label2.Location = new System.Drawing.Point(354, 293);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(58, 13);
      this.label2.TabIndex = 6;
      this.label2.Text = "Columns:";
      // 
      // btnFujitsu1
      // 
      this.btnFujitsu1.BackColor = System.Drawing.Color.DarkGray;
      this.btnFujitsu1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnFujitsu1.ForeColor = System.Drawing.Color.White;
      this.btnFujitsu1.Location = new System.Drawing.Point(238, 66);
      this.btnFujitsu1.Name = "btnFujitsu1";
      this.btnFujitsu1.Size = new System.Drawing.Size(91, 32);
      this.btnFujitsu1.TabIndex = 5;
      this.btnFujitsu1.Text = "Fujitsu 1";
      this.btnFujitsu1.UseVisualStyleBackColor = false;
      this.btnFujitsu1.Click += new System.EventHandler(this.btnFujitsu1_Click);
      // 
      // btnDeleteColomn
      // 
      this.btnDeleteColomn.BackColor = System.Drawing.Color.Red;
      this.btnDeleteColomn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnDeleteColomn.ForeColor = System.Drawing.Color.White;
      this.btnDeleteColomn.Location = new System.Drawing.Point(398, 309);
      this.btnDeleteColomn.Name = "btnDeleteColomn";
      this.btnDeleteColomn.Size = new System.Drawing.Size(35, 25);
      this.btnDeleteColomn.TabIndex = 4;
      this.btnDeleteColomn.Text = "-";
      this.btnDeleteColomn.UseVisualStyleBackColor = false;
      this.btnDeleteColomn.Click += new System.EventHandler(this.btnDeleteColomn_Click);
      // 
      // btnDeleteRow
      // 
      this.btnDeleteRow.BackColor = System.Drawing.Color.Red;
      this.btnDeleteRow.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnDeleteRow.ForeColor = System.Drawing.Color.White;
      this.btnDeleteRow.Location = new System.Drawing.Point(179, 309);
      this.btnDeleteRow.Name = "btnDeleteRow";
      this.btnDeleteRow.Size = new System.Drawing.Size(35, 25);
      this.btnDeleteRow.TabIndex = 3;
      this.btnDeleteRow.Text = "-";
      this.btnDeleteRow.UseVisualStyleBackColor = false;
      this.btnDeleteRow.Click += new System.EventHandler(this.btnDeleteRow_Click);
      // 
      // btnAddColomn
      // 
      this.btnAddColomn.BackColor = System.Drawing.Color.DodgerBlue;
      this.btnAddColomn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnAddColomn.ForeColor = System.Drawing.Color.White;
      this.btnAddColomn.Location = new System.Drawing.Point(357, 309);
      this.btnAddColomn.Name = "btnAddColomn";
      this.btnAddColomn.Size = new System.Drawing.Size(35, 25);
      this.btnAddColomn.TabIndex = 2;
      this.btnAddColomn.Text = "+";
      this.btnAddColomn.UseVisualStyleBackColor = false;
      this.btnAddColomn.Click += new System.EventHandler(this.btnAddColomn_Click);
      // 
      // btnAddRow
      // 
      this.btnAddRow.BackColor = System.Drawing.Color.DodgerBlue;
      this.btnAddRow.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnAddRow.ForeColor = System.Drawing.Color.White;
      this.btnAddRow.Location = new System.Drawing.Point(138, 309);
      this.btnAddRow.Name = "btnAddRow";
      this.btnAddRow.Size = new System.Drawing.Size(35, 25);
      this.btnAddRow.TabIndex = 1;
      this.btnAddRow.Text = "+";
      this.btnAddRow.UseVisualStyleBackColor = false;
      this.btnAddRow.Click += new System.EventHandler(this.btnAddRow_Click);
      // 
      // grdDesktop
      // 
      this.grdDesktop.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.grdDesktop.ColumnHeadersVisible = false;
      this.grdDesktop.Location = new System.Drawing.Point(138, 107);
      this.grdDesktop.Name = "grdDesktop";
      this.grdDesktop.RowHeadersVisible = false;
      this.grdDesktop.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
      this.grdDesktop.Size = new System.Drawing.Size(293, 175);
      this.grdDesktop.TabIndex = 0;
      this.grdDesktop.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.grdDesktop_CellMouseClick);
      // 
      // tabPageNews
      // 
      this.tabPageNews.Controls.Add(this.splitContainer4);
      this.tabPageNews.Location = new System.Drawing.Point(4, 22);
      this.tabPageNews.Name = "tabPageNews";
      this.tabPageNews.Size = new System.Drawing.Size(770, 487);
      this.tabPageNews.TabIndex = 4;
      this.tabPageNews.Text = "News";
      this.tabPageNews.UseVisualStyleBackColor = true;
      // 
      // splitContainer4
      // 
      this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer4.Location = new System.Drawing.Point(0, 0);
      this.splitContainer4.Name = "splitContainer4";
      this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer4.Panel1
      // 
      this.splitContainer4.Panel1.Controls.Add(this.grbImportantNews);
      // 
      // splitContainer4.Panel2
      // 
      this.splitContainer4.Panel2.Controls.Add(this.lblMinutesBefore);
      this.splitContainer4.Panel2.Controls.Add(this.nmrMinutesBefore);
      this.splitContainer4.Panel2.Controls.Add(this.dtpicNewsTime);
      this.splitContainer4.Panel2.Controls.Add(this.dtpicNewsDate);
      this.splitContainer4.Panel2.Controls.Add(this.lblNe);
      this.splitContainer4.Panel2.Controls.Add(this.tbl_ImportantNewsBindingNavigator);
      this.splitContainer4.Size = new System.Drawing.Size(770, 487);
      this.splitContainer4.SplitterDistance = 387;
      this.splitContainer4.TabIndex = 4;
      // 
      // grbImportantNews
      // 
      this.grbImportantNews.Controls.Add(this.grdImportantNews);
      this.grbImportantNews.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.grbImportantNews.ForeColor = System.Drawing.Color.Red;
      this.grbImportantNews.Location = new System.Drawing.Point(8, 17);
      this.grbImportantNews.Name = "grbImportantNews";
      this.grbImportantNews.Size = new System.Drawing.Size(754, 330);
      this.grbImportantNews.TabIndex = 4;
      this.grbImportantNews.TabStop = false;
      this.grbImportantNews.Text = "Important News";
      // 
      // grdImportantNews
      // 
      this.grdImportantNews.AllowUserToAddRows = false;
      this.grdImportantNews.AllowUserToDeleteRows = false;
      this.grdImportantNews.AllowUserToOrderColumns = true;
      this.grdImportantNews.AllowUserToResizeColumns = false;
      this.grdImportantNews.AllowUserToResizeRows = false;
      this.grdImportantNews.AutoGenerateColumns = false;
      this.grdImportantNews.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.grdImportantNews.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13});
      this.grdImportantNews.DataSource = this.tbl_ImportantNewsBindingSource;
      this.grdImportantNews.Dock = System.Windows.Forms.DockStyle.Fill;
      this.grdImportantNews.Location = new System.Drawing.Point(3, 16);
      this.grdImportantNews.Name = "grdImportantNews";
      this.grdImportantNews.Size = new System.Drawing.Size(748, 311);
      this.grdImportantNews.TabIndex = 3;
      // 
      // id
      // 
      this.id.DataPropertyName = "id";
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
      dataGridViewCellStyle1.NullValue = null;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
      this.id.DefaultCellStyle = dataGridViewCellStyle1;
      this.id.HeaderText = "id";
      this.id.Name = "id";
      this.id.ReadOnly = true;
      this.id.Width = 50;
      // 
      // dataGridViewTextBoxColumn12
      // 
      this.dataGridViewTextBoxColumn12.DataPropertyName = "NewsTime";
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Blue;
      dataGridViewCellStyle2.Format = "t";
      dataGridViewCellStyle2.NullValue = null;
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
      this.dataGridViewTextBoxColumn12.DefaultCellStyle = dataGridViewCellStyle2;
      this.dataGridViewTextBoxColumn12.HeaderText = "NewsTime";
      this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
      this.dataGridViewTextBoxColumn12.Width = 180;
      // 
      // dataGridViewTextBoxColumn13
      // 
      this.dataGridViewTextBoxColumn13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
      this.dataGridViewTextBoxColumn13.DataPropertyName = "NewsText";
      dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
      dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
      this.dataGridViewTextBoxColumn13.DefaultCellStyle = dataGridViewCellStyle3;
      this.dataGridViewTextBoxColumn13.HeaderText = "NewsText";
      this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
      // 
      // tbl_ImportantNewsBindingSource
      // 
      this.tbl_ImportantNewsBindingSource.DataMember = "tbl_ImportantNews";
      this.tbl_ImportantNewsBindingSource.DataSource = this.ninjaTraderCustomWorkspacesDataSet2;
      // 
      // ninjaTraderCustomWorkspacesDataSet2
      // 
      this.ninjaTraderCustomWorkspacesDataSet2.DataSetName = "NinjaTraderCustomWorkspacesDataSet2";
      this.ninjaTraderCustomWorkspacesDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
      // 
      // lblMinutesBefore
      // 
      this.lblMinutesBefore.AutoSize = true;
      this.lblMinutesBefore.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.lblMinutesBefore.Location = new System.Drawing.Point(532, 15);
      this.lblMinutesBefore.Name = "lblMinutesBefore";
      this.lblMinutesBefore.Size = new System.Drawing.Size(161, 24);
      this.lblMinutesBefore.TabIndex = 24;
      this.lblMinutesBefore.Text = "Minutes before :";
      // 
      // nmrMinutesBefore
      // 
      this.nmrMinutesBefore.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.nmrMinutesBefore.Location = new System.Drawing.Point(698, 10);
      this.nmrMinutesBefore.Name = "nmrMinutesBefore";
      this.nmrMinutesBefore.Size = new System.Drawing.Size(58, 35);
      this.nmrMinutesBefore.TabIndex = 14;
      this.nmrMinutesBefore.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.nmrMinutesBefore.ValueChanged += new System.EventHandler(this.nmrMinutesBefore_ValueChanged);
      // 
      // dtpicNewsTime
      // 
      this.dtpicNewsTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.dtpicNewsTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
      this.dtpicNewsTime.Location = new System.Drawing.Point(359, 12);
      this.dtpicNewsTime.Name = "dtpicNewsTime";
      this.dtpicNewsTime.Size = new System.Drawing.Size(116, 31);
      this.dtpicNewsTime.TabIndex = 6;
      // 
      // dtpicNewsDate
      // 
      this.dtpicNewsDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.dtpicNewsDate.Location = new System.Drawing.Point(153, 12);
      this.dtpicNewsDate.Name = "dtpicNewsDate";
      this.dtpicNewsDate.Size = new System.Drawing.Size(200, 31);
      this.dtpicNewsDate.TabIndex = 5;
      // 
      // lblNe
      // 
      this.lblNe.AutoSize = true;
      this.lblNe.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.lblNe.ForeColor = System.Drawing.Color.Blue;
      this.lblNe.Location = new System.Drawing.Point(14, 15);
      this.lblNe.Name = "lblNe";
      this.lblNe.Size = new System.Drawing.Size(134, 25);
      this.lblNe.TabIndex = 4;
      this.lblNe.Text = "News Time:";
      // 
      // tbl_ImportantNewsBindingNavigator
      // 
      this.tbl_ImportantNewsBindingNavigator.AddNewItem = this.toolStripButton8;
      this.tbl_ImportantNewsBindingNavigator.BindingSource = this.tbl_ImportantNewsBindingSource;
      this.tbl_ImportantNewsBindingNavigator.CountItem = this.toolStripLabel2;
      this.tbl_ImportantNewsBindingNavigator.DeleteItem = this.toolStripButton9;
      this.tbl_ImportantNewsBindingNavigator.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.tbl_ImportantNewsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton10,
            this.toolStripButton11,
            this.toolStripSeparator4,
            this.toolStripTextBox2,
            this.toolStripLabel2,
            this.toolStripSeparator5,
            this.toolStripButton12,
            this.toolStripButton13,
            this.toolStripSeparator6,
            this.toolStripButton8,
            this.toolStripButton9,
            this.tbl_ImportantNewsBindingNavigatorSaveItem});
      this.tbl_ImportantNewsBindingNavigator.Location = new System.Drawing.Point(0, 71);
      this.tbl_ImportantNewsBindingNavigator.MoveFirstItem = this.toolStripButton10;
      this.tbl_ImportantNewsBindingNavigator.MoveLastItem = this.toolStripButton13;
      this.tbl_ImportantNewsBindingNavigator.MoveNextItem = this.toolStripButton12;
      this.tbl_ImportantNewsBindingNavigator.MovePreviousItem = this.toolStripButton11;
      this.tbl_ImportantNewsBindingNavigator.Name = "tbl_ImportantNewsBindingNavigator";
      this.tbl_ImportantNewsBindingNavigator.PositionItem = this.toolStripTextBox2;
      this.tbl_ImportantNewsBindingNavigator.Size = new System.Drawing.Size(770, 25);
      this.tbl_ImportantNewsBindingNavigator.TabIndex = 2;
      this.tbl_ImportantNewsBindingNavigator.Text = "bindingNavigator1";
      // 
      // toolStripButton8
      // 
      this.toolStripButton8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.toolStripButton8.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton8.Image")));
      this.toolStripButton8.Name = "toolStripButton8";
      this.toolStripButton8.RightToLeftAutoMirrorImage = true;
      this.toolStripButton8.Size = new System.Drawing.Size(23, 22);
      this.toolStripButton8.Text = "Add new";
      this.toolStripButton8.Click += new System.EventHandler(this.toolStripButton8_Click);
      // 
      // toolStripLabel2
      // 
      this.toolStripLabel2.Name = "toolStripLabel2";
      this.toolStripLabel2.Size = new System.Drawing.Size(35, 22);
      this.toolStripLabel2.Text = "of {0}";
      this.toolStripLabel2.ToolTipText = "Total number of items";
      // 
      // toolStripButton9
      // 
      this.toolStripButton9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.toolStripButton9.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton9.Image")));
      this.toolStripButton9.Name = "toolStripButton9";
      this.toolStripButton9.RightToLeftAutoMirrorImage = true;
      this.toolStripButton9.Size = new System.Drawing.Size(23, 22);
      this.toolStripButton9.Text = "Delete";
      // 
      // toolStripButton10
      // 
      this.toolStripButton10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.toolStripButton10.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton10.Image")));
      this.toolStripButton10.Name = "toolStripButton10";
      this.toolStripButton10.RightToLeftAutoMirrorImage = true;
      this.toolStripButton10.Size = new System.Drawing.Size(23, 22);
      this.toolStripButton10.Text = "Move first";
      // 
      // toolStripButton11
      // 
      this.toolStripButton11.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.toolStripButton11.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton11.Image")));
      this.toolStripButton11.Name = "toolStripButton11";
      this.toolStripButton11.RightToLeftAutoMirrorImage = true;
      this.toolStripButton11.Size = new System.Drawing.Size(23, 22);
      this.toolStripButton11.Text = "Move previous";
      // 
      // toolStripSeparator4
      // 
      this.toolStripSeparator4.Name = "toolStripSeparator4";
      this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
      // 
      // toolStripTextBox2
      // 
      this.toolStripTextBox2.AccessibleName = "Position";
      this.toolStripTextBox2.AutoSize = false;
      this.toolStripTextBox2.Name = "toolStripTextBox2";
      this.toolStripTextBox2.Size = new System.Drawing.Size(50, 21);
      this.toolStripTextBox2.Text = "0";
      this.toolStripTextBox2.ToolTipText = "Current position";
      // 
      // toolStripSeparator5
      // 
      this.toolStripSeparator5.Name = "toolStripSeparator5";
      this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
      // 
      // toolStripButton12
      // 
      this.toolStripButton12.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.toolStripButton12.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton12.Image")));
      this.toolStripButton12.Name = "toolStripButton12";
      this.toolStripButton12.RightToLeftAutoMirrorImage = true;
      this.toolStripButton12.Size = new System.Drawing.Size(23, 22);
      this.toolStripButton12.Text = "Move next";
      // 
      // toolStripButton13
      // 
      this.toolStripButton13.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.toolStripButton13.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton13.Image")));
      this.toolStripButton13.Name = "toolStripButton13";
      this.toolStripButton13.RightToLeftAutoMirrorImage = true;
      this.toolStripButton13.Size = new System.Drawing.Size(23, 22);
      this.toolStripButton13.Text = "Move last";
      // 
      // toolStripSeparator6
      // 
      this.toolStripSeparator6.Name = "toolStripSeparator6";
      this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
      // 
      // tbl_ImportantNewsBindingNavigatorSaveItem
      // 
      this.tbl_ImportantNewsBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tbl_ImportantNewsBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("tbl_ImportantNewsBindingNavigatorSaveItem.Image")));
      this.tbl_ImportantNewsBindingNavigatorSaveItem.Name = "tbl_ImportantNewsBindingNavigatorSaveItem";
      this.tbl_ImportantNewsBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
      this.tbl_ImportantNewsBindingNavigatorSaveItem.Text = "Save Data";
      this.tbl_ImportantNewsBindingNavigatorSaveItem.Click += new System.EventHandler(this.tbl_ImportantNewsBindingNavigatorSaveItem_Click);
      // 
      // tabPageATR
      // 
      this.tabPageATR.Controls.Add(this.btnPrepearForNTIndicatorSoloATR1Percents);
      this.tabPageATR.Controls.Add(this.lblATRInfo);
      this.tabPageATR.Controls.Add(this.txtATR);
      this.tabPageATR.Location = new System.Drawing.Point(4, 22);
      this.tabPageATR.Name = "tabPageATR";
      this.tabPageATR.Size = new System.Drawing.Size(770, 487);
      this.tabPageATR.TabIndex = 5;
      this.tabPageATR.Text = "ATR";
      this.tabPageATR.UseVisualStyleBackColor = true;
      // 
      // btnPrepearForNTIndicatorSoloATR1Percents
      // 
      this.btnPrepearForNTIndicatorSoloATR1Percents.BackColor = System.Drawing.Color.RoyalBlue;
      this.btnPrepearForNTIndicatorSoloATR1Percents.ForeColor = System.Drawing.Color.White;
      this.btnPrepearForNTIndicatorSoloATR1Percents.Location = new System.Drawing.Point(505, 12);
      this.btnPrepearForNTIndicatorSoloATR1Percents.Name = "btnPrepearForNTIndicatorSoloATR1Percents";
      this.btnPrepearForNTIndicatorSoloATR1Percents.Size = new System.Drawing.Size(244, 23);
      this.btnPrepearForNTIndicatorSoloATR1Percents.TabIndex = 20;
      this.btnPrepearForNTIndicatorSoloATR1Percents.Text = "Prepear for NT indicator SoloATR1Percents";
      this.btnPrepearForNTIndicatorSoloATR1Percents.UseVisualStyleBackColor = false;
      this.btnPrepearForNTIndicatorSoloATR1Percents.Click += new System.EventHandler(this.btnPrepearForNTIndicatorSoloATR1Percents_Click);
      // 
      // lblATRInfo
      // 
      this.lblATRInfo.AutoSize = true;
      this.lblATRInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.lblATRInfo.Location = new System.Drawing.Point(18, 12);
      this.lblATRInfo.Name = "lblATRInfo";
      this.lblATRInfo.Size = new System.Drawing.Size(235, 13);
      this.lblATRInfo.TabIndex = 19;
      this.lblATRInfo.Text = "ATR data from file in program\'s directory";
      // 
      // txtATR
      // 
      this.txtATR.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.txtATR.Location = new System.Drawing.Point(21, 42);
      this.txtATR.Multiline = true;
      this.txtATR.Name = "txtATR";
      this.txtATR.ScrollBars = System.Windows.Forms.ScrollBars.Both;
      this.txtATR.Size = new System.Drawing.Size(728, 319);
      this.txtATR.TabIndex = 18;
      // 
      // tabPageInfo
      // 
      this.tabPageInfo.Controls.Add(this.splitContainer1);
      this.tabPageInfo.Location = new System.Drawing.Point(4, 22);
      this.tabPageInfo.Name = "tabPageInfo";
      this.tabPageInfo.Padding = new System.Windows.Forms.Padding(3);
      this.tabPageInfo.Size = new System.Drawing.Size(770, 487);
      this.tabPageInfo.TabIndex = 2;
      this.tabPageInfo.Text = "Info";
      this.tabPageInfo.UseVisualStyleBackColor = true;
      // 
      // splitContainer1
      // 
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.Location = new System.Drawing.Point(3, 3);
      this.splitContainer1.Name = "splitContainer1";
      this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.Controls.Add(this.tbl_InstrumentInfoDataGridView);
      this.splitContainer1.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add(this.tbl_InstrumentInfoBindingNavigator);
      this.splitContainer1.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
      this.splitContainer1.Size = new System.Drawing.Size(764, 481);
      this.splitContainer1.SplitterDistance = 416;
      this.splitContainer1.TabIndex = 6;
      // 
      // tbl_InstrumentInfoDataGridView
      // 
      this.tbl_InstrumentInfoDataGridView.AutoGenerateColumns = false;
      this.tbl_InstrumentInfoDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.tbl_InstrumentInfoDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
      this.tbl_InstrumentInfoDataGridView.DataSource = this.tbl_InstrumentInfoBindingSource;
      this.tbl_InstrumentInfoDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tbl_InstrumentInfoDataGridView.Location = new System.Drawing.Point(0, 0);
      this.tbl_InstrumentInfoDataGridView.Name = "tbl_InstrumentInfoDataGridView";
      this.tbl_InstrumentInfoDataGridView.Size = new System.Drawing.Size(764, 416);
      this.tbl_InstrumentInfoDataGridView.TabIndex = 0;
      // 
      // dataGridViewTextBoxColumn1
      // 
      this.dataGridViewTextBoxColumn1.DataPropertyName = "id";
      this.dataGridViewTextBoxColumn1.HeaderText = "id";
      this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
      // 
      // dataGridViewTextBoxColumn2
      // 
      this.dataGridViewTextBoxColumn2.DataPropertyName = "Instrument";
      this.dataGridViewTextBoxColumn2.HeaderText = "Instrument";
      this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
      // 
      // dataGridViewTextBoxColumn3
      // 
      this.dataGridViewTextBoxColumn3.DataPropertyName = "ATR_5Min";
      this.dataGridViewTextBoxColumn3.HeaderText = "ATR_5Min";
      this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
      // 
      // dataGridViewTextBoxColumn4
      // 
      this.dataGridViewTextBoxColumn4.DataPropertyName = "ATR_60Min";
      this.dataGridViewTextBoxColumn4.HeaderText = "ATR_60Min";
      this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
      // 
      // dataGridViewTextBoxColumn5
      // 
      this.dataGridViewTextBoxColumn5.DataPropertyName = "Range_Volume";
      this.dataGridViewTextBoxColumn5.HeaderText = "Range_Volume";
      this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
      // 
      // tbl_InstrumentInfoBindingSource
      // 
      this.tbl_InstrumentInfoBindingSource.DataMember = "tbl_InstrumentInfo";
      this.tbl_InstrumentInfoBindingSource.DataSource = this.ninjaTraderCustomWorkspacesDataSet;
      // 
      // ninjaTraderCustomWorkspacesDataSet
      // 
      this.ninjaTraderCustomWorkspacesDataSet.DataSetName = "NinjaTraderCustomWorkspacesDataSet";
      this.ninjaTraderCustomWorkspacesDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
      // 
      // tbl_InstrumentInfoBindingNavigator
      // 
      this.tbl_InstrumentInfoBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
      this.tbl_InstrumentInfoBindingNavigator.BindingSource = this.tbl_InstrumentInfoBindingSource;
      this.tbl_InstrumentInfoBindingNavigator.CountItem = this.bindingNavigatorCountItem;
      this.tbl_InstrumentInfoBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
      this.tbl_InstrumentInfoBindingNavigator.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.tbl_InstrumentInfoBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.tbl_InstrumentInfoBindingNavigatorSaveItem});
      this.tbl_InstrumentInfoBindingNavigator.Location = new System.Drawing.Point(0, 36);
      this.tbl_InstrumentInfoBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
      this.tbl_InstrumentInfoBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
      this.tbl_InstrumentInfoBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
      this.tbl_InstrumentInfoBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
      this.tbl_InstrumentInfoBindingNavigator.Name = "tbl_InstrumentInfoBindingNavigator";
      this.tbl_InstrumentInfoBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
      this.tbl_InstrumentInfoBindingNavigator.Size = new System.Drawing.Size(764, 25);
      this.tbl_InstrumentInfoBindingNavigator.TabIndex = 3;
      this.tbl_InstrumentInfoBindingNavigator.Text = "bindingNavigator1";
      // 
      // bindingNavigatorAddNewItem
      // 
      this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
      this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
      this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
      this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
      this.bindingNavigatorAddNewItem.Text = "Add new";
      // 
      // bindingNavigatorCountItem
      // 
      this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
      this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
      this.bindingNavigatorCountItem.Text = "of {0}";
      this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
      // 
      // bindingNavigatorDeleteItem
      // 
      this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
      this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
      this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
      this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
      this.bindingNavigatorDeleteItem.Text = "Delete";
      // 
      // bindingNavigatorMoveFirstItem
      // 
      this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
      this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
      this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
      this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
      this.bindingNavigatorMoveFirstItem.Text = "Move first";
      // 
      // bindingNavigatorMovePreviousItem
      // 
      this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
      this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
      this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
      this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
      this.bindingNavigatorMovePreviousItem.Text = "Move previous";
      // 
      // bindingNavigatorSeparator
      // 
      this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
      this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
      // 
      // bindingNavigatorPositionItem
      // 
      this.bindingNavigatorPositionItem.AccessibleName = "Position";
      this.bindingNavigatorPositionItem.AutoSize = false;
      this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
      this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
      this.bindingNavigatorPositionItem.Text = "0";
      this.bindingNavigatorPositionItem.ToolTipText = "Current position";
      // 
      // bindingNavigatorSeparator1
      // 
      this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
      this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
      // 
      // bindingNavigatorMoveNextItem
      // 
      this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
      this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
      this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
      this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
      this.bindingNavigatorMoveNextItem.Text = "Move next";
      // 
      // bindingNavigatorMoveLastItem
      // 
      this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
      this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
      this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
      this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
      this.bindingNavigatorMoveLastItem.Text = "Move last";
      // 
      // bindingNavigatorSeparator2
      // 
      this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
      this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
      // 
      // tbl_InstrumentInfoBindingNavigatorSaveItem
      // 
      this.tbl_InstrumentInfoBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.tbl_InstrumentInfoBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("tbl_InstrumentInfoBindingNavigatorSaveItem.Image")));
      this.tbl_InstrumentInfoBindingNavigatorSaveItem.Name = "tbl_InstrumentInfoBindingNavigatorSaveItem";
      this.tbl_InstrumentInfoBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
      this.tbl_InstrumentInfoBindingNavigatorSaveItem.Text = "Save Data";
      this.tbl_InstrumentInfoBindingNavigatorSaveItem.Click += new System.EventHandler(this.tbl_InstrumentInfoBindingNavigatorSaveItem_Click);
      // 
      // tabPageAlerts
      // 
      this.tabPageAlerts.Controls.Add(this.splitContainer2);
      this.tabPageAlerts.Location = new System.Drawing.Point(4, 22);
      this.tabPageAlerts.Name = "tabPageAlerts";
      this.tabPageAlerts.Size = new System.Drawing.Size(770, 487);
      this.tabPageAlerts.TabIndex = 3;
      this.tabPageAlerts.Text = "Alerts";
      this.tabPageAlerts.UseVisualStyleBackColor = true;
      // 
      // splitContainer2
      // 
      this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer2.Location = new System.Drawing.Point(0, 0);
      this.splitContainer2.Name = "splitContainer2";
      this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer2.Panel1
      // 
      this.splitContainer2.Panel1.Controls.Add(this.splitContainer3);
      // 
      // splitContainer2.Panel2
      // 
      this.splitContainer2.Panel2.Controls.Add(this.tbl_AlertsBindingNavigator);
      this.splitContainer2.Size = new System.Drawing.Size(770, 487);
      this.splitContainer2.SplitterDistance = 433;
      this.splitContainer2.TabIndex = 5;
      // 
      // splitContainer3
      // 
      this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer3.Location = new System.Drawing.Point(0, 0);
      this.splitContainer3.Name = "splitContainer3";
      // 
      // splitContainer3.Panel1
      // 
      this.splitContainer3.Panel1.Controls.Add(this.grbAlerts);
      // 
      // splitContainer3.Panel2
      // 
      this.splitContainer3.Panel2.Controls.Add(this.groupBox1);
      this.splitContainer3.Size = new System.Drawing.Size(770, 433);
      this.splitContainer3.SplitterDistance = 587;
      this.splitContainer3.TabIndex = 1;
      // 
      // grbAlerts
      // 
      this.grbAlerts.Controls.Add(this.grdAlerts);
      this.grbAlerts.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.grbAlerts.ForeColor = System.Drawing.Color.Red;
      this.grbAlerts.Location = new System.Drawing.Point(10, 10);
      this.grbAlerts.Name = "grbAlerts";
      this.grbAlerts.Size = new System.Drawing.Size(574, 288);
      this.grbAlerts.TabIndex = 1;
      this.grbAlerts.TabStop = false;
      this.grbAlerts.Text = "Alerts";
      // 
      // grdAlerts
      // 
      this.grdAlerts.AllowUserToAddRows = false;
      this.grdAlerts.AllowUserToDeleteRows = false;
      this.grdAlerts.AllowUserToOrderColumns = true;
      this.grdAlerts.AllowUserToResizeColumns = false;
      this.grdAlerts.AllowUserToResizeRows = false;
      this.grdAlerts.AutoGenerateColumns = false;
      this.grdAlerts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.grdAlerts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn8,
            this.Price,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewTextBoxColumn10});
      this.grdAlerts.DataSource = this.tbl_AlertsBindingSource;
      this.grdAlerts.Dock = System.Windows.Forms.DockStyle.Fill;
      this.grdAlerts.Location = new System.Drawing.Point(3, 16);
      this.grdAlerts.Name = "grdAlerts";
      this.grdAlerts.Size = new System.Drawing.Size(568, 269);
      this.grdAlerts.TabIndex = 0;
      this.grdAlerts.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.grdAlerts_DataBindingComplete);
      this.grdAlerts.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.grdAlertsDataGridView_RowsAdded);
      this.grdAlerts.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.grdAlerts_MouseDoubleClick);
      // 
      // dataGridViewTextBoxColumn6
      // 
      this.dataGridViewTextBoxColumn6.DataPropertyName = "id";
      dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
      this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle4;
      this.dataGridViewTextBoxColumn6.HeaderText = "id";
      this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
      this.dataGridViewTextBoxColumn6.ReadOnly = true;
      this.dataGridViewTextBoxColumn6.Width = 50;
      // 
      // dataGridViewTextBoxColumn7
      // 
      this.dataGridViewTextBoxColumn7.DataPropertyName = "Instrument";
      dataGridViewCellStyle5.BackColor = System.Drawing.Color.LightBlue;
      dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
      this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle5;
      this.dataGridViewTextBoxColumn7.HeaderText = "Instrument";
      this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
      this.dataGridViewTextBoxColumn7.ReadOnly = true;
      this.dataGridViewTextBoxColumn7.Width = 70;
      // 
      // dataGridViewTextBoxColumn9
      // 
      this.dataGridViewTextBoxColumn9.DataPropertyName = "SetPrice";
      dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
      this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle6;
      this.dataGridViewTextBoxColumn9.HeaderText = "SetPrice";
      this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
      this.dataGridViewTextBoxColumn9.ReadOnly = true;
      // 
      // dataGridViewTextBoxColumn8
      // 
      this.dataGridViewTextBoxColumn8.DataPropertyName = "AlertPrice";
      dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Red;
      this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle7;
      this.dataGridViewTextBoxColumn8.HeaderText = "AlertPrice";
      this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
      // 
      // Price
      // 
      dataGridViewCellStyle8.BackColor = System.Drawing.Color.LightGray;
      dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Blue;
      this.Price.DefaultCellStyle = dataGridViewCellStyle8;
      this.Price.HeaderText = "Price";
      this.Price.Name = "Price";
      this.Price.ReadOnly = true;
      this.Price.Width = 70;
      // 
      // dataGridViewCheckBoxColumn1
      // 
      this.dataGridViewCheckBoxColumn1.DataPropertyName = "Status";
      this.dataGridViewCheckBoxColumn1.FalseValue = false;
      this.dataGridViewCheckBoxColumn1.HeaderText = "Y/n";
      this.dataGridViewCheckBoxColumn1.IndeterminateValue = false;
      this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
      this.dataGridViewCheckBoxColumn1.ReadOnly = true;
      this.dataGridViewCheckBoxColumn1.TrueValue = true;
      this.dataGridViewCheckBoxColumn1.Width = 30;
      // 
      // dataGridViewTextBoxColumn10
      // 
      this.dataGridViewTextBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
      this.dataGridViewTextBoxColumn10.DataPropertyName = "Comment";
      dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
      this.dataGridViewTextBoxColumn10.DefaultCellStyle = dataGridViewCellStyle9;
      this.dataGridViewTextBoxColumn10.HeaderText = "Comment";
      this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
      // 
      // tbl_AlertsBindingSource
      // 
      this.tbl_AlertsBindingSource.DataMember = "tbl_Alerts";
      this.tbl_AlertsBindingSource.DataSource = this.ninjaTraderCustomWorkspacesDataSet1;
      // 
      // ninjaTraderCustomWorkspacesDataSet1
      // 
      this.ninjaTraderCustomWorkspacesDataSet1.DataSetName = "NinjaTraderCustomWorkspacesDataSet1";
      this.ninjaTraderCustomWorkspacesDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.button1);
      this.groupBox1.Controls.Add(this.grdNTMarketData);
      this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.groupBox1.ForeColor = System.Drawing.Color.Red;
      this.groupBox1.Location = new System.Drawing.Point(8, 10);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(166, 288);
      this.groupBox1.TabIndex = 2;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Market data";
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(82, 259);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(75, 23);
      this.button1.TabIndex = 1;
      this.button1.Text = "test";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // grdNTMarketData
      // 
      this.grdNTMarketData.BackgroundColor = System.Drawing.SystemColors.Window;
      this.grdNTMarketData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.grdNTMarketData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.grdNTMarketData.ColumnHeadersVisible = false;
      this.grdNTMarketData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clInstrument,
            this.clPrice});
      this.grdNTMarketData.Dock = System.Windows.Forms.DockStyle.Fill;
      this.grdNTMarketData.Location = new System.Drawing.Point(3, 16);
      this.grdNTMarketData.Name = "grdNTMarketData";
      this.grdNTMarketData.ReadOnly = true;
      this.grdNTMarketData.RowHeadersVisible = false;
      this.grdNTMarketData.Size = new System.Drawing.Size(160, 269);
      this.grdNTMarketData.TabIndex = 0;
      // 
      // clInstrument
      // 
      this.clInstrument.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
      dataGridViewCellStyle10.BackColor = System.Drawing.Color.LightBlue;
      dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
      this.clInstrument.DefaultCellStyle = dataGridViewCellStyle10;
      this.clInstrument.HeaderText = "Instrument";
      this.clInstrument.Name = "clInstrument";
      this.clInstrument.ReadOnly = true;
      // 
      // clPrice
      // 
      this.clPrice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
      dataGridViewCellStyle11.BackColor = System.Drawing.Color.LightGray;
      dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Blue;
      this.clPrice.DefaultCellStyle = dataGridViewCellStyle11;
      this.clPrice.HeaderText = "Price";
      this.clPrice.Name = "clPrice";
      this.clPrice.ReadOnly = true;
      // 
      // tbl_AlertsBindingNavigator
      // 
      this.tbl_AlertsBindingNavigator.AddNewItem = this.toolStripButton1;
      this.tbl_AlertsBindingNavigator.BindingSource = this.tbl_AlertsBindingSource;
      this.tbl_AlertsBindingNavigator.CountItem = this.toolStripLabel1;
      this.tbl_AlertsBindingNavigator.DeleteItem = this.toolStripButton2;
      this.tbl_AlertsBindingNavigator.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.tbl_AlertsBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton3,
            this.toolStripButton4,
            this.toolStripSeparator1,
            this.toolStripTextBox1,
            this.toolStripLabel1,
            this.toolStripSeparator2,
            this.toolStripButton5,
            this.toolStripButton6,
            this.toolStripSeparator3,
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton7});
      this.tbl_AlertsBindingNavigator.Location = new System.Drawing.Point(0, 25);
      this.tbl_AlertsBindingNavigator.MoveFirstItem = this.toolStripButton3;
      this.tbl_AlertsBindingNavigator.MoveLastItem = this.toolStripButton6;
      this.tbl_AlertsBindingNavigator.MoveNextItem = this.toolStripButton5;
      this.tbl_AlertsBindingNavigator.MovePreviousItem = this.toolStripButton4;
      this.tbl_AlertsBindingNavigator.Name = "tbl_AlertsBindingNavigator";
      this.tbl_AlertsBindingNavigator.PositionItem = this.toolStripTextBox1;
      this.tbl_AlertsBindingNavigator.Size = new System.Drawing.Size(770, 25);
      this.tbl_AlertsBindingNavigator.TabIndex = 4;
      this.tbl_AlertsBindingNavigator.Text = "bindingNavigator1";
      // 
      // toolStripButton1
      // 
      this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
      this.toolStripButton1.Name = "toolStripButton1";
      this.toolStripButton1.RightToLeftAutoMirrorImage = true;
      this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
      this.toolStripButton1.Text = "Add new";
      this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
      // 
      // toolStripLabel1
      // 
      this.toolStripLabel1.Name = "toolStripLabel1";
      this.toolStripLabel1.Size = new System.Drawing.Size(35, 22);
      this.toolStripLabel1.Text = "of {0}";
      this.toolStripLabel1.ToolTipText = "Total number of items";
      // 
      // toolStripButton2
      // 
      this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
      this.toolStripButton2.Name = "toolStripButton2";
      this.toolStripButton2.RightToLeftAutoMirrorImage = true;
      this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
      this.toolStripButton2.Text = "Delete";
      this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
      // 
      // toolStripButton3
      // 
      this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
      this.toolStripButton3.Name = "toolStripButton3";
      this.toolStripButton3.RightToLeftAutoMirrorImage = true;
      this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
      this.toolStripButton3.Text = "Move first";
      // 
      // toolStripButton4
      // 
      this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
      this.toolStripButton4.Name = "toolStripButton4";
      this.toolStripButton4.RightToLeftAutoMirrorImage = true;
      this.toolStripButton4.Size = new System.Drawing.Size(23, 22);
      this.toolStripButton4.Text = "Move previous";
      // 
      // toolStripSeparator1
      // 
      this.toolStripSeparator1.Name = "toolStripSeparator1";
      this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
      // 
      // toolStripTextBox1
      // 
      this.toolStripTextBox1.AccessibleName = "Position";
      this.toolStripTextBox1.AutoSize = false;
      this.toolStripTextBox1.Name = "toolStripTextBox1";
      this.toolStripTextBox1.Size = new System.Drawing.Size(50, 21);
      this.toolStripTextBox1.Text = "0";
      this.toolStripTextBox1.ToolTipText = "Current position";
      // 
      // toolStripSeparator2
      // 
      this.toolStripSeparator2.Name = "toolStripSeparator2";
      this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
      // 
      // toolStripButton5
      // 
      this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
      this.toolStripButton5.Name = "toolStripButton5";
      this.toolStripButton5.RightToLeftAutoMirrorImage = true;
      this.toolStripButton5.Size = new System.Drawing.Size(23, 22);
      this.toolStripButton5.Text = "Move next";
      // 
      // toolStripButton6
      // 
      this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
      this.toolStripButton6.Name = "toolStripButton6";
      this.toolStripButton6.RightToLeftAutoMirrorImage = true;
      this.toolStripButton6.Size = new System.Drawing.Size(23, 22);
      this.toolStripButton6.Text = "Move last";
      // 
      // toolStripSeparator3
      // 
      this.toolStripSeparator3.Name = "toolStripSeparator3";
      this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
      // 
      // toolStripButton7
      // 
      this.toolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
      this.toolStripButton7.Name = "toolStripButton7";
      this.toolStripButton7.Size = new System.Drawing.Size(23, 22);
      this.toolStripButton7.Text = "Save Data";
      this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
      // 
      // tabPageScreens
      // 
      this.tabPageScreens.Controls.Add(this.pnlDZInstruments);
      this.tabPageScreens.Location = new System.Drawing.Point(4, 22);
      this.tabPageScreens.Name = "tabPageScreens";
      this.tabPageScreens.Size = new System.Drawing.Size(770, 487);
      this.tabPageScreens.TabIndex = 6;
      this.tabPageScreens.Text = "Screens";
      this.tabPageScreens.UseVisualStyleBackColor = true;
      // 
      // pnlDZInstruments
      // 
      this.pnlDZInstruments.AutoScroll = true;
      this.pnlDZInstruments.BackColor = System.Drawing.Color.WhiteSmoke;
      this.pnlDZInstruments.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.pnlDZInstruments.Controls.Add(this.btnChartsStatistic);
      this.pnlDZInstruments.Controls.Add(this.bntDZListForCode);
      this.pnlDZInstruments.Controls.Add(this.lblDZScreensTitle);
      this.pnlDZInstruments.Controls.Add(this.panel1);
      this.pnlDZInstruments.Location = new System.Drawing.Point(8, 11);
      this.pnlDZInstruments.Name = "pnlDZInstruments";
      this.pnlDZInstruments.Size = new System.Drawing.Size(368, 468);
      this.pnlDZInstruments.TabIndex = 1;
      // 
      // btnChartsStatistic
      // 
      this.btnChartsStatistic.BackColor = System.Drawing.Color.LightGray;
      this.btnChartsStatistic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnChartsStatistic.Location = new System.Drawing.Point(224, 296);
      this.btnChartsStatistic.Name = "btnChartsStatistic";
      this.btnChartsStatistic.Size = new System.Drawing.Size(100, 40);
      this.btnChartsStatistic.TabIndex = 6;
      this.btnChartsStatistic.Text = "Charts Statistic (to ATR TextBox)";
      this.btnChartsStatistic.UseVisualStyleBackColor = false;
      this.btnChartsStatistic.Click += new System.EventHandler(this.btnChartsStatistic_Click);
      // 
      // bntDZListForCode
      // 
      this.bntDZListForCode.BackColor = System.Drawing.Color.LightGray;
      this.bntDZListForCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.bntDZListForCode.Location = new System.Drawing.Point(224, 260);
      this.bntDZListForCode.Name = "bntDZListForCode";
      this.bntDZListForCode.Size = new System.Drawing.Size(100, 30);
      this.bntDZListForCode.TabIndex = 4;
      this.bntDZListForCode.Text = "DZ List for code";
      this.bntDZListForCode.UseVisualStyleBackColor = false;
      this.bntDZListForCode.Click += new System.EventHandler(this.bntDZListForCode_Click);
      // 
      // lblDZScreensTitle
      // 
      this.lblDZScreensTitle.AutoSize = true;
      this.lblDZScreensTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.lblDZScreensTitle.Location = new System.Drawing.Point(215, 19);
      this.lblDZScreensTitle.Name = "lblDZScreensTitle";
      this.lblDZScreensTitle.Size = new System.Drawing.Size(90, 13);
      this.lblDZScreensTitle.TabIndex = 5;
      this.lblDZScreensTitle.Text = "DZ SCREENS:";
      // 
      // panel1
      // 
      this.panel1.BackColor = System.Drawing.SystemColors.ButtonShadow;
      this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.panel1.Controls.Add(this.chkDZ60mAnd1440m);
      this.panel1.Controls.Add(this.btnPrepareDZ);
      this.panel1.Controls.Add(this.btnSaveResult);
      this.panel1.Controls.Add(this.btnSaveDZ);
      this.panel1.Controls.Add(this.btnPrepareResult);
      this.panel1.Location = new System.Drawing.Point(215, 38);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(119, 197);
      this.panel1.TabIndex = 4;
      // 
      // chkDZ60mAnd1440m
      // 
      this.chkDZ60mAnd1440m.AutoSize = true;
      this.chkDZ60mAnd1440m.Location = new System.Drawing.Point(8, 8);
      this.chkDZ60mAnd1440m.Name = "chkDZ60mAnd1440m";
      this.chkDZ60mAnd1440m.Size = new System.Drawing.Size(102, 17);
      this.chkDZ60mAnd1440m.TabIndex = 4;
      this.chkDZ60mAnd1440m.Text = "60m and 1440m";
      this.chkDZ60mAnd1440m.UseVisualStyleBackColor = true;
      // 
      // btnPrepareDZ
      // 
      this.btnPrepareDZ.BackColor = System.Drawing.Color.LightGray;
      this.btnPrepareDZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.btnPrepareDZ.Location = new System.Drawing.Point(7, 31);
      this.btnPrepareDZ.Name = "btnPrepareDZ";
      this.btnPrepareDZ.Size = new System.Drawing.Size(100, 30);
      this.btnPrepareDZ.TabIndex = 0;
      this.btnPrepareDZ.Text = "Prepare DZ";
      this.btnPrepareDZ.UseVisualStyleBackColor = false;
      this.btnPrepareDZ.Click += new System.EventHandler(this.btnPrepareDZ_Click);
      // 
      // btnSaveResult
      // 
      this.btnSaveResult.BackColor = System.Drawing.Color.RoyalBlue;
      this.btnSaveResult.ForeColor = System.Drawing.Color.White;
      this.btnSaveResult.Location = new System.Drawing.Point(7, 145);
      this.btnSaveResult.Name = "btnSaveResult";
      this.btnSaveResult.Size = new System.Drawing.Size(100, 30);
      this.btnSaveResult.TabIndex = 3;
      this.btnSaveResult.Text = "Save Result";
      this.btnSaveResult.UseVisualStyleBackColor = false;
      this.btnSaveResult.Click += new System.EventHandler(this.btnSaveResult_Click);
      // 
      // btnSaveDZ
      // 
      this.btnSaveDZ.BackColor = System.Drawing.Color.RoyalBlue;
      this.btnSaveDZ.ForeColor = System.Drawing.Color.White;
      this.btnSaveDZ.Location = new System.Drawing.Point(7, 60);
      this.btnSaveDZ.Name = "btnSaveDZ";
      this.btnSaveDZ.Size = new System.Drawing.Size(100, 30);
      this.btnSaveDZ.TabIndex = 1;
      this.btnSaveDZ.Text = "Save DZ";
      this.btnSaveDZ.UseVisualStyleBackColor = false;
      this.btnSaveDZ.Click += new System.EventHandler(this.btnSaveDZ_Click);
      // 
      // btnPrepareResult
      // 
      this.btnPrepareResult.BackColor = System.Drawing.Color.LightGray;
      this.btnPrepareResult.Location = new System.Drawing.Point(7, 116);
      this.btnPrepareResult.Name = "btnPrepareResult";
      this.btnPrepareResult.Size = new System.Drawing.Size(100, 30);
      this.btnPrepareResult.TabIndex = 2;
      this.btnPrepareResult.Text = "Prepare Result";
      this.btnPrepareResult.UseVisualStyleBackColor = false;
      this.btnPrepareResult.Click += new System.EventHandler(this.btnPrepareResult_Click);
      // 
      // tbl_InstrumentInfoTableAdapter
      // 
      this.tbl_InstrumentInfoTableAdapter.ClearBeforeFill = true;
      // 
      // tableAdapterManager
      // 
      this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
      this.tableAdapterManager.tbl_InstrumentInfoTableAdapter = this.tbl_InstrumentInfoTableAdapter;
      this.tableAdapterManager.UpdateOrder = NinjaTraderCustomWorkspaces.NinjaTraderCustomWorkspacesDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
      // 
      // tbl_AlertsTableAdapter
      // 
      this.tbl_AlertsTableAdapter.ClearBeforeFill = true;
      // 
      // tableAdapterManager1
      // 
      this.tableAdapterManager1.BackupDataSetBeforeUpdate = false;
      this.tableAdapterManager1.tbl_AlertsTableAdapter = this.tbl_AlertsTableAdapter;
      this.tableAdapterManager1.UpdateOrder = NinjaTraderCustomWorkspaces.NinjaTraderCustomWorkspacesDataSet1TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
      // 
      // tmCurrentTime
      // 
      this.tmCurrentTime.Enabled = true;
      this.tmCurrentTime.Interval = 1000;
      this.tmCurrentTime.Tick += new System.EventHandler(this.tmCurrentTime_Tick);
      // 
      // tmNTMarketData
      // 
      this.tmNTMarketData.Interval = 20000;
      // 
      // tmAlert
      // 
      this.tmAlert.Interval = 300;
      this.tmAlert.Tick += new System.EventHandler(this.tmAlert_Tick);
      // 
      // tbl_ImportantNewsTableAdapter
      // 
      this.tbl_ImportantNewsTableAdapter.ClearBeforeFill = true;
      // 
      // tableAdapterManager2
      // 
      this.tableAdapterManager2.BackupDataSetBeforeUpdate = false;
      this.tableAdapterManager2.tbl_ImportantNewsTableAdapter = this.tbl_ImportantNewsTableAdapter;
      this.tableAdapterManager2.UpdateOrder = NinjaTraderCustomWorkspaces.NinjaTraderCustomWorkspacesDataSet2TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
      // 
      // frm_main
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoScroll = true;
      this.ClientSize = new System.Drawing.Size(778, 513);
      this.Controls.Add(this.tabControl);
      this.Name = "frm_main";
      this.Text = "Main form";
      this.TopMost = true;
      this.Deactivate += new System.EventHandler(this.frm_main_Deactivate);
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_main_FormClosed);
      this.Load += new System.EventHandler(this.frm_main_Load);
      this.tabControl.ResumeLayout(false);
      this.tabPageBase.ResumeLayout(false);
      this.tabPageBase.PerformLayout();
      this.grbNTProcessPrioritet.ResumeLayout(false);
      this.grbNTProcessPrioritet.PerformLayout();
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      this.grbTime.ResumeLayout(false);
      this.grbTime.PerformLayout();
      this.grbMainRule.ResumeLayout(false);
      this.grbMainRule.PerformLayout();
      this.tabPageDesktops.ResumeLayout(false);
      this.tabPageDesktops.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.grdDesktop)).EndInit();
      this.tabPageNews.ResumeLayout(false);
      this.splitContainer4.Panel1.ResumeLayout(false);
      this.splitContainer4.Panel2.ResumeLayout(false);
      this.splitContainer4.Panel2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
      this.splitContainer4.ResumeLayout(false);
      this.grbImportantNews.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.grdImportantNews)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.tbl_ImportantNewsBindingSource)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.ninjaTraderCustomWorkspacesDataSet2)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nmrMinutesBefore)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.tbl_ImportantNewsBindingNavigator)).EndInit();
      this.tbl_ImportantNewsBindingNavigator.ResumeLayout(false);
      this.tbl_ImportantNewsBindingNavigator.PerformLayout();
      this.tabPageATR.ResumeLayout(false);
      this.tabPageATR.PerformLayout();
      this.tabPageInfo.ResumeLayout(false);
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel2.ResumeLayout(false);
      this.splitContainer1.Panel2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
      this.splitContainer1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.tbl_InstrumentInfoDataGridView)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.tbl_InstrumentInfoBindingSource)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.ninjaTraderCustomWorkspacesDataSet)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.tbl_InstrumentInfoBindingNavigator)).EndInit();
      this.tbl_InstrumentInfoBindingNavigator.ResumeLayout(false);
      this.tbl_InstrumentInfoBindingNavigator.PerformLayout();
      this.tabPageAlerts.ResumeLayout(false);
      this.splitContainer2.Panel1.ResumeLayout(false);
      this.splitContainer2.Panel2.ResumeLayout(false);
      this.splitContainer2.Panel2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
      this.splitContainer2.ResumeLayout(false);
      this.splitContainer3.Panel1.ResumeLayout(false);
      this.splitContainer3.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
      this.splitContainer3.ResumeLayout(false);
      this.grbAlerts.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.grdAlerts)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.tbl_AlertsBindingSource)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.ninjaTraderCustomWorkspacesDataSet1)).EndInit();
      this.groupBox1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.grdNTMarketData)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.tbl_AlertsBindingNavigator)).EndInit();
      this.tbl_AlertsBindingNavigator.ResumeLayout(false);
      this.tbl_AlertsBindingNavigator.PerformLayout();
      this.tabPageScreens.ResumeLayout(false);
      this.pnlDZInstruments.ResumeLayout(false);
      this.pnlDZInstruments.PerformLayout();
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btnFill;
    private System.Windows.Forms.ListBox lstInstruments;
    private System.Windows.Forms.TabControl tabControl;
    private System.Windows.Forms.TabPage tabPageBase;
    private System.Windows.Forms.TabPage tabPageDesktops;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.RadioButton rbnFootLeft5MinRight;
    private System.Windows.Forms.RadioButton rbn5MinLeftFootRight;
    private System.Windows.Forms.RadioButton rbn5MinFullScreen;
    private System.Windows.Forms.RadioButton rbnStandart;
    private System.Windows.Forms.RadioButton rbn60MinFullScreen;
    private System.Windows.Forms.RadioButton rbn5MinAnd60Min;
    private System.Windows.Forms.Button btnAddColomn;
    private System.Windows.Forms.Button btnAddRow;
    private System.Windows.Forms.DataGridView grdDesktop;
    private System.Windows.Forms.Button btnDeleteRow;
    private System.Windows.Forms.Button btnDeleteColomn;
    private System.Windows.Forms.Button btnFujitsu2;
    private System.Windows.Forms.Button btnLenovo1;
    private System.Windows.Forms.Button btnLenovo2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Button btnFujitsu1;
    private System.Windows.Forms.ListBox lstConfigurationInstruments;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.ListBox lstConfigurationDesktops;
    private System.Windows.Forms.TextBox txtConfigurationWorkspace;
    private System.Windows.Forms.Button btnUpdateWorkspace;
    private System.Windows.Forms.Button btnDeleteWorkspace;
    private System.Windows.Forms.Button btnSaveDesktop;
    private System.Windows.Forms.Button btnDeleteDesktop;
    private System.Windows.Forms.Button btnViewDesktop;
    private System.Windows.Forms.Button btnNewWorkspace;
    private System.Windows.Forms.Button btnClear;
    private System.Windows.Forms.Button btnHide;
    private System.Windows.Forms.TabPage tabPageInfo;
    private System.Windows.Forms.RadioButton rbnFootRight5MinLeftOneScreen;
    private System.Windows.Forms.RadioButton rbn5Min60minLeftFootRightOneScreen;
    private System.Windows.Forms.RadioButton rbnFullScreens60MinLeft5MinRight;
    private System.Windows.Forms.TabPage tabPageAlerts;
    private System.Windows.Forms.TabPage tabPageNews;
    private NinjaTraderCustomWorkspacesDataSet ninjaTraderCustomWorkspacesDataSet;
    private System.Windows.Forms.BindingSource tbl_InstrumentInfoBindingSource;
    private NinjaTraderCustomWorkspacesDataSetTableAdapters.tbl_InstrumentInfoTableAdapter tbl_InstrumentInfoTableAdapter;
    private NinjaTraderCustomWorkspacesDataSetTableAdapters.TableAdapterManager tableAdapterManager;
    private System.Windows.Forms.BindingNavigator tbl_InstrumentInfoBindingNavigator;
    private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
    private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
    private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
    private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
    private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
    private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
    private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
    private System.Windows.Forms.ToolStripButton tbl_InstrumentInfoBindingNavigatorSaveItem;
    private System.Windows.Forms.DataGridView tbl_InstrumentInfoDataGridView;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
    private System.Windows.Forms.SplitContainer splitContainer1;
    private NinjaTraderCustomWorkspacesDataSet1 ninjaTraderCustomWorkspacesDataSet1;
    private System.Windows.Forms.BindingSource tbl_AlertsBindingSource;
    private NinjaTraderCustomWorkspacesDataSet1TableAdapters.tbl_AlertsTableAdapter tbl_AlertsTableAdapter;
    private NinjaTraderCustomWorkspacesDataSet1TableAdapters.TableAdapterManager tableAdapterManager1;
    private System.Windows.Forms.BindingNavigator tbl_AlertsBindingNavigator;
    private System.Windows.Forms.ToolStripButton toolStripButton1;
    private System.Windows.Forms.ToolStripLabel toolStripLabel1;
    private System.Windows.Forms.ToolStripButton toolStripButton2;
    private System.Windows.Forms.ToolStripButton toolStripButton3;
    private System.Windows.Forms.ToolStripButton toolStripButton4;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    private System.Windows.Forms.ToolStripButton toolStripButton5;
    private System.Windows.Forms.ToolStripButton toolStripButton6;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
    private System.Windows.Forms.ToolStripButton toolStripButton7;
    private System.Windows.Forms.DataGridView grdAlerts;
    private System.Windows.Forms.Label lblMainRule;
    private System.Windows.Forms.GroupBox grbMainRule;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.ListBox lstWorkspaces;
    private System.Windows.Forms.Label lblCurrentTime;
    private System.Windows.Forms.Timer tmCurrentTime;
    private System.Windows.Forms.GroupBox grbTime;
    private System.Windows.Forms.SplitContainer splitContainer2;
    private System.Windows.Forms.SplitContainer splitContainer3;
    private System.Windows.Forms.Timer tmNTMarketData;
    private System.Windows.Forms.DataGridView grdNTMarketData;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.GroupBox grbAlerts;
    private System.Windows.Forms.DataGridViewTextBoxColumn clInstrument;
    private System.Windows.Forms.DataGridViewTextBoxColumn clPrice;
    private System.Windows.Forms.Button btnAlert;
    private System.Windows.Forms.Timer tmAlert;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.CheckBox chkAlertMessage;
    private System.Windows.Forms.CheckBox chkAlertSound;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
    private System.Windows.Forms.DataGridViewTextBoxColumn Price;
    private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
    private System.Windows.Forms.RadioButton rbnRemote;
    private System.Windows.Forms.RadioButton rbnMain;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.Label lblChartLocationText;
    private System.Windows.Forms.Label lblChartLocation;
    private System.Windows.Forms.Button btnDesktopDeleteRowsAndCells;
    private System.Windows.Forms.ListBox lstConfigurationWorkspaces;
    private System.Windows.Forms.Button btnShowSelectedDesktop;
    private System.Windows.Forms.Button btnShowSelectedWorkspace;
    private System.Windows.Forms.RadioButton rbnFullScreens60MinRight5MinLeft;
    private System.Windows.Forms.GroupBox grbNTProcessPrioritet;
    private System.Windows.Forms.RadioButton rbnNTPrioritetNormal;
    private System.Windows.Forms.RadioButton rbnNTPrioritetLow;
    private System.Windows.Forms.DataGridView grdImportantNews;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
    private System.Windows.Forms.BindingSource tbl_ImportantNewsBindingSource;
    private NinjaTraderCustomWorkspacesDataSet2 ninjaTraderCustomWorkspacesDataSet2;
    private System.Windows.Forms.BindingNavigator tbl_ImportantNewsBindingNavigator;
    private System.Windows.Forms.ToolStripButton toolStripButton8;
    private System.Windows.Forms.ToolStripLabel toolStripLabel2;
    private System.Windows.Forms.ToolStripButton toolStripButton9;
    private System.Windows.Forms.ToolStripButton toolStripButton10;
    private System.Windows.Forms.ToolStripButton toolStripButton11;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
    private System.Windows.Forms.ToolStripTextBox toolStripTextBox2;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
    private System.Windows.Forms.ToolStripButton toolStripButton12;
    private System.Windows.Forms.ToolStripButton toolStripButton13;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
    private System.Windows.Forms.ToolStripButton tbl_ImportantNewsBindingNavigatorSaveItem;
    private NinjaTraderCustomWorkspacesDataSet2TableAdapters.tbl_ImportantNewsTableAdapter tbl_ImportantNewsTableAdapter;
    private NinjaTraderCustomWorkspacesDataSet2TableAdapters.TableAdapterManager tableAdapterManager2;
    private System.Windows.Forms.SplitContainer splitContainer4;
    private System.Windows.Forms.GroupBox grbImportantNews;
    private System.Windows.Forms.Label lblNe;
    private System.Windows.Forms.DateTimePicker dtpicNewsTime;
    private System.Windows.Forms.DateTimePicker dtpicNewsDate;
    private System.Windows.Forms.DataGridViewTextBoxColumn id;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
    private System.Windows.Forms.Label lblMinutesBefore;
    private System.Windows.Forms.NumericUpDown nmrMinutesBefore;
    private System.Windows.Forms.RadioButton rbn60MinAnd1440Min;
    private System.Windows.Forms.Button btnSaveDesktopAsIs;
    private System.Windows.Forms.RadioButton rbnStandart2;
    private System.Windows.Forms.Button btnControlCenter;
    private System.Windows.Forms.Button btnAlerts;
    private System.Windows.Forms.TabPage tabPageATR;
    private System.Windows.Forms.Label lblATRInfo;
    private System.Windows.Forms.TextBox txtATR;
    private System.Windows.Forms.Button btnDayChart;
    private System.Windows.Forms.CheckBox chkHomework;
    private System.Windows.Forms.Button btnView60MinChart;
    private System.Windows.Forms.RadioButton rbn1440MinFullScreen;
    private System.Windows.Forms.RadioButton rbnStandart3;
    private System.Windows.Forms.RadioButton rbnWEEKFullScreen;
    private System.Windows.Forms.RadioButton rbnStandart4;
    private System.Windows.Forms.RadioButton rbnStandart6;
    private System.Windows.Forms.RadioButton rbnStandart5;
    private System.Windows.Forms.Button btnReloadChart;
    private System.Windows.Forms.TabPage tabPageScreens;
    private System.Windows.Forms.Button btnPrepareDZ;
    private System.Windows.Forms.Panel pnlDZInstruments;
    private System.Windows.Forms.Button btnSaveDZ;
    private System.Windows.Forms.Button btnPrepearForNTIndicatorSoloATR1Percents;
    private System.Windows.Forms.Button btnSaveResult;
    private System.Windows.Forms.Button btnPrepareResult;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Button bntDZListForCode;
    private System.Windows.Forms.Label lblDZScreensTitle;
    private System.Windows.Forms.ToolTip toolTip1;
    private System.Windows.Forms.Button btnChartsStatistic;
    private System.Windows.Forms.CheckBox chkDZ60mAnd1440m;
    private System.Windows.Forms.CheckBox chkAllInstruments;
    private System.Windows.Forms.Button btnDz;
  }
}

