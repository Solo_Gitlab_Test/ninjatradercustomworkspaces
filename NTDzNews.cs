﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Globalization;

namespace NinjaTraderCustomWorkspaces
{
  public class NTDzNews
  {
    private Int32     m_iNewsID               = 0;
    private Int32     m_iInstrumentID         = 0;
    private String    m_strInstrumentName     = String.Empty;
    private DateTime  m_dtNewsDateTime        = DateTime.MinValue;

    private Int32 m_iSerialNumberInCollection = 0;

    public NTDzNews()
    {
    }

    public NTDzNews(Int32 iNewsID, Int32 iInstrumentID, String strInstrumentName, DateTime dtNewsDateTime)
    {
      m_iNewsID           = iNewsID;     
      m_iInstrumentID     = iInstrumentID;
      m_strInstrumentName = strInstrumentName;
      m_dtNewsDateTime    = dtNewsDateTime;
    }

    #region Create
    public Boolean Create(String strInstrumentID, DateTime dtNewsDateTime)
    {
      try
      {
        using (OleDbConnection objConnection = new OleDbConnection(Program.SoloAMG_DZConnectionString))
        {
          objConnection.Open();
          OleDbCommand objCommand = objConnection.CreateCommand();
          StringBuilder sbQuery   = new StringBuilder();

          #region РАЗОБРАТЬСЯ, ПОЧЕМУ НЕ РАБОТАЕТ ВСТАВКА С ПАРАМЕТРАМИ
          //objCommand.Parameters.Add("@p_InstrumentID", OleDbType.Integer).Value     = iInstrumentID;
          //objCommand.Parameters.Add("@p_NewsDateTime", OleDbType.DBTimeStamp).Value = dtNewsDateTime;

          //sbQuery.Append("INSERT INTO test (InstrumentID, NewsDateTime) VALUES (@p_InstrumentID, '@p_NewsDateTime');");

          //sbQuery.Append("INSERT INTO test (InstrumentID, NewsDateTime) VALUES (" + iInstrumentID.ToString() + "," + "'" + dtNewsDateTime.ToString("yyyy.MM.dd hh:mm:ss") + "')");
          #endregion

          sbQuery.Append("INSERT INTO News (InstrumentID, NewsDateTime) VALUES (" + strInstrumentID + ", ");
          sbQuery.Append("'" + dtNewsDateTime.ToString("yyyy.MM.dd HH:mm:ss") + "')");

          objCommand.CommandText = sbQuery.ToString();
          objCommand.ExecuteNonQuery();

          return true;
        } 
      }
      catch (Exception ex)
      {
        return false;
      }
    }
    #endregion

    #region Delete
    public bool Delete(List<Int32> lstNewsID)
    {
      if (lstNewsID.Count == 0)
          return true;

      using (OleDbConnection objConnection = new OleDbConnection(Program.SoloAMG_DZConnectionString))
      {
        OleDbCommand objCommand             = objConnection.CreateCommand();
        OleDbTransaction objTransaction     = null;
        StringBuilder sbDeleteNewsQuery     = new StringBuilder();

        try
        {
          sbDeleteNewsQuery.Append("DELETE from News where News.ID IN (" + GetSQL_IN_Construction(lstNewsID) + ") ");
          sbDeleteNewsQuery.Append("AND News.ID NOT IN (SELECT News.ID FROM News INNER JOIN DZ_NEWS ON News.ID = DZ_NEWS.NewsID);");

          objConnection.Open();

          objTransaction         = objConnection.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
          objCommand.Transaction = objTransaction;

          objCommand.CommandText = sbDeleteNewsQuery.ToString();
          objCommand.ExecuteNonQuery();

          objTransaction.Commit();

          return true;
        }
        catch (Exception ex)
        {
          try
          {
            Program.ShowErrorMessage(ex.Message);
            objTransaction.Rollback();
          }
          catch
          {
          }

          return false;
        }
      }
    }
    #endregion


    #region GetSQL_IN_Construction
    public String GetSQL_IN_Construction(List<Int32> lst)
    {
      if (lst.Count == 0)
          return "-1";

      String strReturnValue = String.Empty;

      try
      {
        for (int i = 0; i < lst.Count; i++)
        {
          if (i < lst.Count - 1)
          {
              strReturnValue += lst[i].ToString() + ",";
              continue;
          }

          strReturnValue += lst[i].ToString();
        }

      }
      catch (Exception ex)
      {
      }

      return strReturnValue;
    }
    #endregion

    #region Properties
    public Int32 NewsID
    {
      get { return m_iNewsID; }
    }

    public Int32 InstrumentID
    {
      get { return m_iInstrumentID; }
    }

    public String InstrumentName
    {
      get { return m_strInstrumentName; }
    }

    public DateTime NewsDateTime
    {
      get { return m_dtNewsDateTime; }
    }

    public Int32 NumberInCollection
    {
      get { return m_iSerialNumberInCollection; }
      set { m_iSerialNumberInCollection = value; }
    }

    public String DisplayValue
    {
      get
      {
        String strReturnValue = String.Empty;

        strReturnValue = this.NumberInCollection.ToString() + ") " + this.InstrumentName + " " + this.m_dtNewsDateTime.ToString();

        return strReturnValue;

      }
    }

    #endregion
  }
}
