﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;

namespace NinjaTraderCustomWorkspaces
{
  public class NTWorkspace
  {
    private Int32               m_iID;
    private String              m_strName;
    private NTDesktopCollection m_objDesktopCollection;

    public NTWorkspace()
    { 
    }

    public NTWorkspace(String strName)
    {
      m_strName              = strName;
      m_objDesktopCollection = new NTDesktopCollection();
    }

    public NTWorkspace(Int32 iID, String strName, NTChartCollection objMainCharts, NTChartCollection objRemoteCharts)
    {
      m_iID                  = iID;
      m_strName              = strName;
      m_objDesktopCollection = new NTDesktopCollection(m_iID);

      m_objDesktopCollection.Fill(objMainCharts, objRemoteCharts);
    }

    public void Create(String strNTWorkspaceName)
    {
      using (OleDbConnection objConnection = new OleDbConnection(Program.WorkspacesDataConnectionString))
      {
        objConnection.Open();
        OleDbCommand objCommand = objConnection.CreateCommand();
        StringBuilder sbQuery   = new StringBuilder();
        
        sbQuery.Append("INSERT INTO tbl_Workspaces (WorkspaceName) VALUES ('");
        sbQuery.Append(strNTWorkspaceName);
        sbQuery.Append("');");

        objCommand.CommandText = sbQuery.ToString();
        objCommand.ExecuteNonQuery();
      } 
    }

    public void Delete()
    {
      foreach (NTDesktop NTDesktop in this.DesktopCollection)
        NTDesktop.Delete();

      using (OleDbConnection objConnection = new OleDbConnection(Program.WorkspacesDataConnectionString))
      {
        objConnection.Open();
        OleDbCommand objCommand = objConnection.CreateCommand();
        StringBuilder sbQuery   = new StringBuilder();

        sbQuery.Append("DELETE * FROM tbl_Workspaces WHERE ID = " + this.m_iID.ToString() + ";");

        objCommand.CommandText = sbQuery.ToString();
        objCommand.ExecuteNonQuery();
      }
    }

    public void Update(String strNTWorkspaceNewName)
    {
      using (OleDbConnection objConnection = new OleDbConnection(Program.WorkspacesDataConnectionString))
      {
        objConnection.Open();
        OleDbCommand objCommand = objConnection.CreateCommand();
        StringBuilder sbQuery = new StringBuilder();

        sbQuery.Append("UPDATE tbl_Workspaces SET WorkspaceName='");
        sbQuery.Append(strNTWorkspaceNewName);
        sbQuery.Append("' WHERE ID = ");
        sbQuery.Append(this.m_iID.ToString());
        sbQuery.Append(";");

        objCommand.CommandText = sbQuery.ToString();
        objCommand.ExecuteNonQuery();
      }
    }

    public void CreateMain60Min5MinAndFootPrint()
    {
      m_objDesktopCollection.Clear();
      m_objDesktopCollection.FillMain60Min5MinAndFootPrint();
    }

    public void CreateRemote60Min5MinAndFootPrint()
    {
      m_objDesktopCollection.Clear();
      m_objDesktopCollection.FillRemote60Min5MinAndFootPrint();
    }

    public void SetPositionAndShow()
    {
      foreach (NTDesktop objNTDesktop in this.DesktopCollection)
        objNTDesktop.SetPositionAndShow();
    }

    public Int32 ID
    {
      get
      {
        return m_iID;
      }
    }

    public String Name
    {
      get
      {
        return m_strName;
      }
      set
      {
        m_strName = value;
      }
    }

    public NTDesktopCollection DesktopCollection
    {
      get
      {
        return m_objDesktopCollection;
      }
    }

    public override string ToString()
    {
      return m_strName;
    }
  }
}
