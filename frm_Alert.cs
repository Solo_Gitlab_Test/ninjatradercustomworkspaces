﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NinjaTraderCustomWorkspaces
{
  public partial class frm_Alert : Form
  {
    private String m_strMessage;

    public frm_Alert()
    {
      InitializeComponent();
    }

    public frm_Alert(String strMessage)
    {
      InitializeComponent();
      m_strMessage = strMessage;
    }

    private void frm_Alert_Load(object sender, EventArgs e)
    {
      lblMessage.Text = m_strMessage;
      this.Location = new Point(Program.ScreensResolution.MainSecondScreenBounds.X,
                                Program.ScreensResolution.MainSecondScreenBounds.Y);
    }

  }
}
