﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.OleDb;
using System.Globalization;

namespace NinjaTraderCustomWorkspaces
{

  class NTDzCollection : System.Collections.CollectionBase
  {
    public NTDzCollection()
    {}


    public bool Fill(DateTime dtFromDate, DateTime dtToDate)
    {
      try
      {
        if (this.Count > 0)
            this.Clear();

        DateTime dtFromDateTime   = new DateTime(dtFromDate.Year, dtFromDate.Month, dtFromDate.Day, 0,0,0);
        DateTime dtToDateTime     = new DateTime(dtToDate.Year, dtToDate.Month, dtToDate.Day, 23,59,59);

        using (OleDbConnection objConnection = new OleDbConnection(Program.SoloAMG_DZConnectionString))
        {
          objConnection.Open();
          OleDbCommand objCommand = objConnection.CreateCommand();
          StringBuilder sbQuery   = new StringBuilder();

          sbQuery.Append("SELECT AMGDZ.[ID], Instrument.[ID], Instrument.[Name], ");
          sbQuery.Append("AMGDZ.[Position], AMGDZ.[Direction], AMGDZ.[InstrumentRisk], AMGDZ.[LevelPrice], AMGDZ.[MainDate], AMGDZ.[EntryTimeDate], ");
          sbQuery.Append("AMGDZ.[ExitTimeDate], AMGDZ.[EntryPrice], AMGDZ.[ExitPrice], AMGDZ.[Quantity], AMGDZ.[ScreenFile], AMGDZ.[Comment] ");
          sbQuery.Append("FROM Instrument INNER JOIN AMGDZ ON Instrument.[ID] = AMGDZ.[InstrumentID] ");
          sbQuery.Append("WHERE AMGDZ.[MainDate] >= #" + dtFromDateTime.ToString("yyyy.MM.dd HH:mm:ss").Replace('.', '-') + "# ");
          sbQuery.Append("AND AMGDZ.[MainDate] <= #"   + dtToDateTime.ToString("yyyy.MM.dd HH:mm:ss").Replace('.', '-') + "# ");
          sbQuery.Append("ORDER BY AMGDZ.[MainDate];");

          #region sql query
          /*
          SELECT AMGDZ.ID, Instrument.ID, Instrument.[Name], AMGDZ.Position, AMGDZ.Direction, AMGDZ.InstrumentRisk, 
          AMGDZ.LevelPrice, AMGDZ.MainDate, AMGDZ.EntryTimeDate, AMGDZ.ExitTimeDate, AMGDZ.EntryPrice, 
          AMGDZ.ExitPrice, AMGDZ.Quantity, AMGDZ.ScreenFile, AMGDZ.Comment 
          FROM Instrument INNER JOIN AMGDZ ON Instrument.ID = AMGDZ.InstrumentID 
          WHERE AMGDZ.MainDate >= #2018-06-01 00:00:00# AND AMGDZ.MainDate <= #2018-06-20 23:59:59# 
          ORDER BY AMGDZ.MainDate;
          */
          #endregion

          objCommand.CommandText = sbQuery.ToString();

          using (OleDbDataReader objReader = objCommand.ExecuteReader())
          {
            while (objReader.Read())
            {
              NTDz objNTDzNews = new NTDz();

              objNTDzNews.ID                  = (Int32)objReader[0];
              objNTDzNews.InstrumentID        = (Int32)objReader[1];
              objNTDzNews.InstrumentName      = (String)objReader[2];
              objNTDzNews.DZPosition          = (Enums.DZPosition) Enum.Parse(typeof(Enums.DZPosition), objReader[3].ToString());
              objNTDzNews.DZDirection         = (Enums.DZDirection) Enum.Parse(typeof(Enums.DZDirection), objReader[4].ToString());
              objNTDzNews.InstrumentRisk      = (Enums.InstrumentRisk) Enum.Parse(typeof(Enums.InstrumentRisk), objReader[5].ToString());
              objNTDzNews.LevelPrice          = (Double)objReader[6];
              objNTDzNews.MainDate            = DateTime.Parse(objReader[7].ToString());
              objNTDzNews.EntryDatetime       = (objReader[8].ToString()  == String.Empty) ? DateTime.MinValue : DateTime.Parse(objReader[8].ToString());
              objNTDzNews.ExitDatetime        = (objReader[9].ToString()  == String.Empty) ? DateTime.MinValue : DateTime.Parse(objReader[9].ToString());
              objNTDzNews.EntryPrice          = (objReader[10].ToString() == String.Empty) ? -1 : (Double)objReader[10];
              objNTDzNews.ExitPrice           = (objReader[11].ToString() == String.Empty) ? -1 : (Double)objReader[11];
              objNTDzNews.Quantity            = (objReader[12].ToString() == String.Empty) ? -1 : (Int32)objReader[12]; 
              objNTDzNews.SelectScreenFile    = objReader[13].ToString();
              objNTDzNews.Comment             = objReader[14].ToString();


              this.Add(objNTDzNews);

            }
          }

          foreach(NTDz objNTDz in this)
          {
            if (objNTDz.NTDZNews == null)
                objNTDz.NTDZNews = new NTDzNewsCollection();

            objNTDz.NTDZNews.FillByDZ_ID(objNTDz.ID);
          }
        }
      }
      catch (Exception ex)
      {
      }

      return false;
    }

    public NTDz GetDZObjectByDZID(Int32 iDZID)
    {
      try
      {
        foreach(NTDz objNTDz in this)
        {
          if (objNTDz.ID == iDZID)
              return objNTDz;
        }
      }
      catch (Exception ex)
      {
      }

      return new NTDz();
    }

    #region Standart Functions & indexator
    public Int32 Add(NTDz objNTDz)
    {
      return (List.Add(objNTDz));
    }

    public Int32 IndexOf(NTDz objNTDz)
    {
      return (List.IndexOf(objNTDz));
    }

    public void Insert(Int32 index, NTDz objNTDz)
    {
      List.Insert(index, objNTDz);
    }

    public void Remove(NTDz objNTDz)
    {
      List.Remove(objNTDz);
    }

    public NTDz this[int index]
    {
      get
      {
        return (List[index] as NTDz);
      }
      set
      {
        List[index] = value;
      }
    }
    #endregion

  }





}
