﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;
using System.Drawing;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace NinjaTraderCustomWorkspaces
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    /// 

    private static String m_strWorkspacesDataConnectionString       = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=WorkspacesData.accdb;Persist Security Info=False";
    private static String m_strSoloAMG_DZConnectionString           = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=SoloAMG_DZ.accdb;Persist Security Info=False";

    private static String m_strHandle_Desktop = "00010014";
    private static String m_strHandle_NinjaTrader                   = String.Empty;
    private static String m_strNTProcessName                        = "NinjaTrader";
    private static Int32 m_iNTProcessID                             = 0;
    private static String m_strATRFileName                          = @"atr.txt";
    private static List<IntPtr> m_lstNTWindows                      = new List<IntPtr>();

    private static List<String> m_lstAllNTInstruments               = new List<String>();
    private static List<String> m_lstAllNTInstrumentsRemote         = new List<String>();
    private static List<String> m_lstDistinctNTInstruments          = new List<String>();
    private static List<String> m_lstDistinctNTInstrumentsRemote    = new List<String>();

    private static List<String> m_lstCaptionSubstrings              = new List<String>() { "Min", "Range", "Volume", "Weekly" };
    private static String m_strRegExpKey                            = @"(\d\d.\d\d.\d\d\d\d|Weekly)"; // @"\d\d.\d\d.\d\d\d\d";
    private static String m_strRemoteIPAddress                      = "192.168.1.45";
    private static String m_strRemoteDnsName                        = "Lenovo";
    private static Boolean m_IsPingReplySucceess                    = false;
    private static Boolean m_IsPingReplySucceessFlag                = false;

    private static Int32 m_iRemotePort                              = 11001;

    public static Size FormSmallSize                                = new Size(402, 552);
    public static Size FormLargeSize                                = new Size(786, 468);
    /*
    public static NinjaTrader.Client.Client objNTClient             = new NinjaTrader.Client.Client();
    */
    public static Int32 intNTConnectionStatus                       = -1;
    
    public static String m_strSelectedMonitorName                   = String.Empty;    
    public static NTMonitorCollection m_objNTMonitorCollection      = new NTMonitorCollection();

    public struct ScreensResolution
    {
      public static Rectangle MainFirstScreenBounds;
      public static Rectangle MainFirstScreenWorkingArea;
      public static Rectangle MainSecondScreenBounds;

      public static Rectangle RemoteFirstScreenBounds;
      public static Rectangle RemoteFirstScreenWorkingArea;
      public static Rectangle RemoteSecondScreenBounds;      
    }

    [STAThread]
    static void Main()
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Application.Run(new frm_main());
      //Application.Run(new frm_Dz());
      //Application.Run(new frm_AMGDz());
    }

    public static Boolean IsNTMainProccessRuning()
    {
      List<Process> lstProcesses = new List<Process>();
      Process[] pl               = Process.GetProcesses();

      foreach (Process p in pl)
      {
        if (p.ProcessName == m_strNTProcessName)
        {
            lstProcesses.Add(p);

            m_strHandle_NinjaTrader = Emulations.Convert.Functions.IntPtrToStringHex(p.Handle);
            m_iNTProcessID          = p.Id;
        }
      }

      if (lstProcesses.Count > 0)
          return true;

      return false;
    }

    public static void Initialize()
    {
      ScreensResolution.MainFirstScreenBounds         = Screen.PrimaryScreen.Bounds; // 1280x800
      ScreensResolution.MainFirstScreenWorkingArea    = Screen.PrimaryScreen.WorkingArea;
      ScreensResolution.MainSecondScreenBounds        = System.Windows.Forms.Screen.AllScreens[1].Bounds; // 1920x1080

      // ### Solo (решено)
      // проблема задержки при запуске когда не включен удаленный компьютер здесь. возможное решение проверять в ф-ции SendMessageToNTServer запущен ли remote компьютер
      // end Solo

      ScreensResolution.RemoteFirstScreenBounds       = Emulations.Convert.Functions.StringToRectangle(SendMessageToNTServer("GetFirstScreenBounds"));
      ScreensResolution.RemoteFirstScreenWorkingArea  = Emulations.Convert.Functions.StringToRectangle(SendMessageToNTServer("GetFirstScreenWorkingArea"));
      ScreensResolution.RemoteSecondScreenBounds      = Emulations.Convert.Functions.StringToRectangle(SendMessageToNTServer("GetSecondScreenBounds"));

      if (!IsNTMainProccessRuning())
      {
          ShowErrorMessage("No instances of NinjaTrader. Exiting.");
          Application.Exit();
      }

      m_lstNTWindows = Emulations.ProcessItems.GetWindowHandlesByProcessName(m_strNTProcessName, m_strRegExpKey, Emulations.ProcessItems.Instruments.RegularExpression);

      //if (!NTSubscribe())
      //{
      //    ShowErrorMessage("Error during subscribe NinjaTrader. Exiting.");
      //    Application.Exit();
      //}

      m_objNTMonitorCollection.Create();
    }

    //public static Boolean NTSubscribe()
    //{
    //  try
    //  {
    //    foreach (String strInstrument in NTDistinctInstruments)
    //    {
    //      objNTClient.SubscribeMarketData(strInstrument);
    //      intNTConnectionStatus = objNTClient.Connected(0);

    //      if (intNTConnectionStatus == 0)
    //          continue;

    //      return false;
    //    }

    //    return true;
    //  }
    //  catch (Exception ex)
    //  {
    //    ShowErrorMessage(ex.Message);
    //    return false;
    //  }
    //}

    //public static Boolean NTUnSubscribe()
    //{
    //  try
    //  {
    //    foreach (String strInstrument in NTDistinctInstruments)
    //    {
    //      if (objNTClient.UnsubscribeMarketData(strInstrument) == 0)
    //          continue;

    //      return false;
    //    }

    //    if (objNTClient.TearDown() == -1)
    //        return false;

    //    return true;
    //  }
    //  catch (Exception ex)
    //  {
    //    ShowErrorMessage(ex.Message);
    //    return false;
    //  }
    //}

    public static void SetNTProcessPriority(ProcessPriorityClass enmProcessPriorityClass)
    {
      Process NTProcess       = Process.GetProcessById(Program.NTProcessID);
      NTProcess.PriorityClass = enmProcessPriorityClass;
    }

    public static String GetTimeWithMillisecond()
    {
      return DateTime.Now.ToLongTimeString() + " " + DateTime.Now.Millisecond;
    }

    public static void ShowErrorMessage(String strErrorMessage)
    {
      DialogResult rslt = MessageBox.Show(strErrorMessage, "Error", MessageBoxButtons.OK);
    }

    public static String SendMessageToNTServer(String strMessage)
    {
      if (!m_IsPingReplySucceessFlag && !m_IsPingReplySucceess)
      {
          m_IsPingReplySucceess     = Emulations.Net.IsPingReplySucceess(m_strRemoteDnsName);
          m_IsPingReplySucceessFlag = true;
      }

      if (!m_IsPingReplySucceess)
          return "ServerNotAvailable";

      //if (!Emulations.Net.IsPingReplySucceess(m_strRemoteDnsName))
      //    return "ServerNotAvailable";

      String strReturnValue = String.Empty;

      TcpClient client = new TcpClient();

      try
      {
        client.Connect(new IPEndPoint(IPAddress.Parse(m_strRemoteIPAddress), m_iRemotePort));

        StreamWriter sw = new StreamWriter(client.GetStream());
        sw.AutoFlush = true;

        sw.WriteLine(strMessage);

        StreamReader sr = new StreamReader(client.GetStream());
        strReturnValue = sr.ReadLine();
      }
      catch(Exception ex)
      {
        strReturnValue = "ServerNotAvailable";
      }
      finally
      {
        client.Close();
      }

      return strReturnValue;
    }


    public static String Handle_Desktop
    {
      get
      {
        return m_strHandle_Desktop;
      }
    }


    public static String Handle_NinjaTrader
    {
      get
      {
        return m_strHandle_NinjaTrader;
      }
    }

    public static String NTProcessName
    {
      get
      {
        return m_strNTProcessName;
      }
    }

    public static String WorkspacesDataConnectionString
    {
      get
      {
        return m_strWorkspacesDataConnectionString;
      }
    }

    public static String SoloAMG_DZConnectionString
    {
      get
      {
        return m_strSoloAMG_DZConnectionString;
      }
    }

    public static String ATRFileName
    {
      get
      {
        return m_strATRFileName;
      }
    }

    public static IntPtr NTAlertWindow
    {
      get
      {
        IntPtr hResult = IntPtr.Zero;
        List<IntPtr> lst = Emulations.ProcessItems.GetWindowHandlesByProcessName(m_strNTProcessName, "Alerts", Emulations.ProcessItems.Instruments.RegularExpression);

        if (lst.Count > 0)
          hResult = lst[0];

        return hResult;
      }
    }

    public static IntPtr NTOutputWindow
    {
      get
      {
        IntPtr hResult = IntPtr.Zero;
        List<IntPtr> lst = Emulations.ProcessItems.GetWindowHandlesByProcessName(m_strNTProcessName, "Output", Emulations.ProcessItems.Instruments.RegularExpression);

        if (lst.Count > 0)
          hResult = lst[0];

        return hResult;
      }
    }

    public static IntPtr NTDayChart
    {
      get
      {
        IntPtr hResult = IntPtr.Zero;
        List<IntPtr> lst = Emulations.ProcessItems.GetWindowHandlesByProcessName(m_strNTProcessName, "1440", Emulations.ProcessItems.Instruments.RegularExpression);

        if (lst.Count > 0)
          hResult = lst[0];

        return hResult;
      }
    }

    public static IntPtr NTControlCenterWindow
    {
      get
      {
        IntPtr hResult = IntPtr.Zero;
        List<IntPtr> lst = Emulations.ProcessItems.GetWindowHandlesByProcessName(m_strNTProcessName, "Control", Emulations.ProcessItems.Instruments.RegularExpression);

        if (lst.Count > 0)
          hResult = lst[0];

        return hResult;
      }
    }

    public static System.Collections.Generic.List<System.IntPtr> NTWindows
    {
      get
      {
        m_lstNTWindows = Emulations.ProcessItems.GetWindowHandlesByProcessName(m_strNTProcessName, m_strRegExpKey, Emulations.ProcessItems.Instruments.RegularExpression);

        return m_lstNTWindows;
      }
    }

    public static System.Collections.Generic.List<string> CaptionSubstrings
    {
      get
      {
        return m_lstCaptionSubstrings;
      }
    }

    public static String RegExpKey
    {
      get
      {
        return m_strRegExpKey;
      }
    }

    public static System.Collections.Generic.List<String> NTAllInstruments
    {
      get
      {
        m_lstAllNTInstruments.Clear();

        String strWindowCaption   = String.Empty;
        String strInstrumentName  = String.Empty;
        List<IntPtr> lstNTWindows = NTWindows;

        for (int i = 0; i < lstNTWindows.Count; i++)
        {
          strWindowCaption  = Emulations.Window.GetWindowTextByHandle(lstNTWindows[i]);
          strInstrumentName = strWindowCaption.Substring(0, strWindowCaption.IndexOf('(') - 1);

          m_lstAllNTInstruments.Add(strInstrumentName);
        }

        m_lstAllNTInstruments.Sort();

        return m_lstAllNTInstruments;
      }
    }

    public static System.Collections.Generic.List<String> NTAllInstrumentsRemote
    {
      get
      {
        m_lstAllNTInstrumentsRemote.Clear();

        String strRemoteCommandResult = Program.SendMessageToNTServer("GetNTChartWindows");

        if (strRemoteCommandResult == "ServerNotAvailable")
            return new List<String>();

        String[] strArr               = strRemoteCommandResult.Split(Char.Parse(";"));

        for (int i = 0; i < strArr.Length; i++)
        {
          String strWindowCaption  = strArr[i].Split(Char.Parse("|"))[0];
          String strInstrumentName = strWindowCaption.Substring(0, strWindowCaption.IndexOf('(') - 1);

          m_lstAllNTInstrumentsRemote.Add(strInstrumentName);
        }

        m_lstAllNTInstrumentsRemote.Sort();

        return m_lstAllNTInstrumentsRemote;
      }
    }

    public static System.Collections.Generic.List<String> NTDistinctInstruments
    {
      get
      {
        m_lstDistinctNTInstruments.Clear();

        String strWindowCaption   = String.Empty;
        String strInstrumentName  = String.Empty;
        List<IntPtr> lstNTWindows = NTWindows;
        List<String> lstTemp      = new List<String>() { };

        for (int i = 0; i < lstNTWindows.Count; i++)
        {
          strWindowCaption  = Emulations.Window.GetWindowTextByHandle(lstNTWindows[i]);
          strInstrumentName = strWindowCaption.Substring(0, strWindowCaption.IndexOf('(') - 1);

          lstTemp.Add(strInstrumentName);
        }

        IEnumerable<String> noduplicates = lstTemp.Distinct();

        foreach (String str in noduplicates)
          m_lstDistinctNTInstruments.Add(str);

        m_lstDistinctNTInstruments.Sort();

        return m_lstDistinctNTInstruments;
      }
    }

    public static System.Collections.Generic.List<String> NTDistinctInstrumentsRemote
    {
      get
      {
        m_lstDistinctNTInstrumentsRemote.Clear();

        String strRemoteCommandResult = Program.SendMessageToNTServer("GetNTChartWindows");

        if (strRemoteCommandResult == "ServerNotAvailable")
            return new List<String>();

        String[] strArr               = strRemoteCommandResult.Split(Char.Parse(";"));
        List<String> lstTemp          = new List<String>() { };

        for (int i = 0; i < strArr.Length; i++)
        {
          String strWindowCaption  = strArr[i].Split(Char.Parse("|"))[0];
          String strInstrumentName = strWindowCaption.Substring(0, strWindowCaption.IndexOf('(') - 1);

          lstTemp.Add(strInstrumentName);
        }

        IEnumerable<String> noduplicates = lstTemp.Distinct();

        foreach (String str in noduplicates)
          m_lstDistinctNTInstrumentsRemote.Add(str);

        m_lstDistinctNTInstrumentsRemote.Sort();

        return m_lstDistinctNTInstrumentsRemote;
      }
    }

    public static String SelectedMonitorName
    {
      get
      {
        return m_strSelectedMonitorName;
      }
      set
      {
        m_strSelectedMonitorName = value;
      }
    }

    public static NTMonitor SelectedMonitor
    {
      get
      {
        return m_objNTMonitorCollection[m_strSelectedMonitorName];
      }
    }

    public static NTMonitorCollection MonitorCollection
    {
      get
      {
        return m_objNTMonitorCollection;
      }
    }

    public static Int32 NTProcessID
    {
      get
      {
        return m_iNTProcessID;
      }
    }
  }
}
