﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.OleDb;

namespace NinjaTraderCustomWorkspaces
{
  public class NTDesktopCollection : System.Collections.CollectionBase
  {
    private Int32 m_iWorkspaceID;

    public NTDesktopCollection()
    { 
    }

    public NTDesktopCollection(Int32 iWorkspaceID)
    {
      m_iWorkspaceID = iWorkspaceID;
    }

    public void Fill(NTChartCollection objMainCharts, NTChartCollection objRemoteCharts)
    {
      this.Clear();

      using (OleDbConnection objConnection = new OleDbConnection(Program.WorkspacesDataConnectionString))
      {
        objConnection.Open();
        OleDbCommand objCommand = objConnection.CreateCommand();
        StringBuilder sbQuery   = new StringBuilder();

        sbQuery.Append("SELECT ID, DesktopName, MonitorName, GridRowsCount, GridColumnsCount FROM tbl_Desktops WHERE WorkspaceID = " + this.WorkspaceID.ToString() + ";");

        objCommand.CommandText = sbQuery.ToString();

        using (OleDbDataReader objReader = objCommand.ExecuteReader())
        {
          while (objReader.Read())
          {
            NTDesktop objNTDesktop = new NTDesktop((Int32)objReader[0], (String)objReader[1],
                                                   (String)objReader[2], (Int32)objReader[3],
                                                   (Int32)objReader[4]);

            objNTDesktop.Regenerate(objMainCharts, objRemoteCharts);
            this.Add(objNTDesktop);
          }
        }
      }
    }

    public void FillMain60Min5MinAndFootPrint()
    {
      List<String> lstDistinctInstruments = Program.NTDistinctInstruments;

      for (int i = 0; i < lstDistinctInstruments.Count; i++)
      {
        String strInstrumentName = lstDistinctInstruments[i];
        NTDesktop objNTDesktop   = new NTDesktop(strInstrumentName);
        
        objNTDesktop.CreateMain60Min5MinAndFootPrint(strInstrumentName);

        this.Add(objNTDesktop);
      }
    }

    public void FillRemote60Min5MinAndFootPrint()
    {
      List<String> lstDistinctInstruments = Program.NTDistinctInstrumentsRemote;

      for (int i = 0; i < lstDistinctInstruments.Count; i++)
      {
        String strInstrumentName = lstDistinctInstruments[i];
        NTDesktop objNTDesktop   = new NTDesktop(strInstrumentName);

        objNTDesktop.CreateRemote60Min5MinAndFootPrint(strInstrumentName);

        this.Add(objNTDesktop);
      }
    }

    public void HideChartsOnFirstScreen()
    {
      foreach (NTDesktop objNTDesktop in this)
      {
        foreach (NTChart objNTChart in objNTDesktop.ChartCollection)
        {
          if (objNTChart.Disposition == Enums.ChartDisposition.MainFirstScreen ||
              objNTChart.Disposition == Enums.ChartDisposition.RemoteFirstScreen)
              objNTChart.Hide();
        }
      }
    }

    public Int32 Add(NTDesktop objNTDesktop)
    {
      return (List.Add(objNTDesktop));
    }

    public Int32 IndexOf(NTDesktop objNTDesktop)
    {
      return (List.IndexOf(objNTDesktop));
    }

    public void Insert(Int32 index, NTDesktop objNTDesktop)
    {
      List.Insert(index, objNTDesktop);
    }

    public void Remove(NTDesktop objNTDesktop)
    {
      List.Remove(objNTDesktop);
    }

    public NTDesktop this[int index]
    {
      get
      {
        return (List[index] as NTDesktop);
      }
      set
      {
        List[index] = value;
      }
    }

    public NTDesktop this[String strName]
    {
      get
      {
        for (int i = 0; i < this.Count; i++)
        {
          if (this[i].Name == strName)
              return this[i] as NTDesktop;
        }

        return null;
      }
    }

    public Int32 WorkspaceID
    {
      get
      {
        return m_iWorkspaceID;
      }
    }
  }
}
