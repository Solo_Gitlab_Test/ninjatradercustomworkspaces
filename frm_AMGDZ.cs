﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace NinjaTraderCustomWorkspaces
{
  public partial class frm_AMGDz : Form
  {
    private String m_strSolo_InstrumentsData__FilePath = @"C:\Users\Solo\Documents\NinjaTrader 7\bin\Custom\Indicator\Solo_InstrumentsData.cs";

    public frm_AMGDz()
    {
      InitializeComponent();
    }
    
    public void DZObjectToForm(NTDz objNTDz)
    {
      if (objNTDz.ID == 0)
          return;

      try
      {
        cbDZInstrument.SelectedIndex        = cbDZInstrument.FindString(objNTDz.InstrumentName);
        txtLevelPrice.Text                  = objNTDz.LevelPrice.ToString();
        cbPosition.SelectedIndex            = cbPosition.FindString(objNTDz.DZPosition.ToString());
        cbDirection.SelectedIndex           = cbDirection.FindString(objNTDz.DZDirection.ToString());
        cbInstrumentRisk.SelectedIndex      = cbInstrumentRisk.FindString(objNTDz.InstrumentRisk.ToString());

        dtDZMainDate.Value                  = objNTDz.MainDate;
        dtEntryTimeDate.Value               = (objNTDz.EntryDatetime  == DateTime.MinValue) ? DateTime.Now : objNTDz.EntryDatetime;
        dtEntryTimeTime.Value               = (objNTDz.EntryDatetime  == DateTime.MinValue) ? DateTime.Now : objNTDz.EntryDatetime;
        dtExitTimeDate.Value                = (objNTDz.ExitDatetime   == DateTime.MinValue)  ? DateTime.Now : objNTDz.ExitDatetime;
        dtExitTimeTime.Value                = (objNTDz.ExitDatetime   == DateTime.MinValue)  ? DateTime.Now : objNTDz.ExitDatetime;

        txtEntryPrice.Text                  = (objNTDz.EntryPrice     == -1) ? String.Empty : objNTDz.EntryPrice.ToString();
        txtExitPrice.Text                   = (objNTDz.ExitPrice      == -1) ? String.Empty : objNTDz.ExitPrice.ToString();
        nmrQuantity.Value                   = (objNTDz.Quantity       == -1) ? 0 : objNTDz.Quantity;

        txtScreenFile.Text                  = objNTDz.SelectScreenFile;
        txtComment.Text                     = objNTDz.Comment;

        cbDZNews.DataSource                 = objNTDz.NTDZNews;
        cbDZNews.ValueMember                = "NewsID";
        cbDZNews.DisplayMember              = "DisplayValue";

      }
      catch (Exception ex)
      {
      }  
    }

    public NTDz FormToDZObject()
    {
      NTDz objDZ = new NTDz();

      try
      {
      }
      catch (Exception ex)
      {
      }  
  
      return objDZ;

      //NTDz objNTDz = new NTDz((Int32)cbDZInstrument.SelectedValue, 
        //                        (String)cbDZInstrument.Text.Trim(), 
        //                        Double.Parse(txtLevelPrice.Text), 
        //                        (Enums.DZPosition)cbPosition.SelectedValue, 
        //                        (Enums.DZDirection)cbDirection.SelectedValue, 
        //                        (Enums.InstrumentRisk)cbInstrumentRisk.SelectedValue, 
        //                        dtDZMainDate.Value,
        //                        txtComment.Text.Trim());
    }

    private void frm_Dz_Load(object sender, EventArgs e)
    {
      SetDZControlsEnable(false, Enums.DZOperationType.Default);
      PrepareForm_Default();
    }

    private void btnFilterViewList_Click(object sender, EventArgs e)
    {
      FillNewsListBox();

      NTDzCollection objNTDzCollection  = new NTDzCollection();
      objNTDzCollection.Fill(dtFilterFromDate.Value, dtFilterToDate.Value);

      DZObjectToForm(objNTDzCollection.GetDZObjectByDZID(Int32.Parse(txtTest.Text)));

    }

    private void btnCreateCode_Click(object sender, EventArgs e)
    {

      MessageBox.Show("НЕ ЗАБУДЬ СКОМПИЛИРОВАТЬ В NINJATRADER ФАЙЛ Solo_InstrumentData!");
    }

    private void btnNewDZ_Click(object sender, EventArgs e)
    {
      PrepareForm_Default();
      SetDZControlsEnable(true, Enums.DZOperationType.New);
      btnNewDZ.Enabled  = false;
      btnSaveDZ.Enabled = true;
    }

    private void btnSaveDZ_Click(object sender, EventArgs e)
    {
      try
      {
        if (CheckForm() == false)
            return;
          
        SetDZControlsEnable(false, Enums.DZOperationType.Default);
        btnNewDZ.Enabled  = true;
        btnSaveDZ.Enabled = false;

        NTDz objNTDz = new NTDz((Int32)cbDZInstrument.SelectedValue, 
                                (String)cbDZInstrument.Text.Trim(), 
                                Double.Parse(txtLevelPrice.Text), 
                                (Enums.DZPosition)cbPosition.SelectedValue, 
                                (Enums.DZDirection)cbDirection.SelectedValue, 
                                (Enums.InstrumentRisk)cbInstrumentRisk.SelectedValue, 
                                dtDZMainDate.Value,
                                txtComment.Text.Trim());
 
        if (objNTDz.Create())
        {
          MessageBox.Show("News has been added");
        }
        else
        {
          MessageBox.Show("News has NOT been added. Error.");
        }

      }
      catch (Exception ex)
      {
      }
    }

    private void btnEditDZ_Click(object sender, EventArgs e)
    {

    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      PrepareForm_Default();
      SetDZControlsEnable(false, Enums.DZOperationType.Default);
      btnNewDZ.Enabled  = true;
      btnSaveDZ.Enabled = false;
    }

    private void btnAddNews_Click(object sender, EventArgs e)
    {
      try
      {
        DateTime dtNewsDateTime = new DateTime(dtAddNewsDate.Value.Year, dtAddNewsDate.Value.Month, dtAddNewsDate.Value.Day,
                                                dtAddNewsTime.Value.Hour, dtAddNewsTime.Value.Minute, dtAddNewsTime.Value.Second);
            
        NTDzNews objNTDzNews = new NTDzNews();

        if (!objNTDzNews.Create(cbNewsInstrument.SelectedValue.ToString(), dtNewsDateTime))
            return;

        FillNewsListBox();

      }
      catch (Exception ex)
      {
      }
    }

    private void btnDeleteNews_Click(object sender, EventArgs e)
    {
      try
      {
        DialogResult dialogResult = MessageBox.Show("Are you sure you want to delete this news?", "Delete News", MessageBoxButtons.YesNo);
            
        if (dialogResult == DialogResult.No)
            return;

        NTDzNews objNTDzNews  = new NTDzNews();
        List<Int32> lst       = new List<Int32>();

        foreach (var item in lstNews.SelectedItems) 
        {
          Int32 iValue = (item as NTDzNews).NewsID;

          lst.Add(iValue);
        }

        if (!objNTDzNews.Delete(lst))
            return;

        FillNewsListBox();

      }
      catch (Exception ex)
      {
      }
    }

    private void btnSelectScreenFile_Click(object sender, EventArgs e)
    {

    }

    private void txtLevelPrice_KeyPress(object sender, KeyPressEventArgs e)
    {
      TextBoxCheckKeyPress(e);
    }

    private void txtEntryPrice_KeyPress(object sender, KeyPressEventArgs e)
    {
      TextBoxCheckKeyPress(e);
    }

    private void txtExitPrice_KeyPress(object sender, KeyPressEventArgs e)
    {
      TextBoxCheckKeyPress(e);
    }

    private void txtLevelPrice_KeyUp(object sender, KeyEventArgs e)
    {
      TextBoxCheckKeyUp(sender, e);
    }

    private void txtEntryPrice_KeyUp(object sender, KeyEventArgs e)
    {
      TextBoxCheckKeyUp(sender, e);
    }

    private void txtExitPrice_KeyUp(object sender, KeyEventArgs e)
    {
      TextBoxCheckKeyUp(sender, e);
    }

    public void PrepareForm_Default()
    {
      try
      {
        if (cbDZInstrument.Items.Count == 0)
        {
            StringBuilder sbInsertDesktopQuery = new StringBuilder();
            sbInsertDesktopQuery.Append("SELECT Instrument.ID, Instrument.Name FROM Instrument;");
            FillDropDownList(Program.SoloAMG_DZConnectionString, sbInsertDesktopQuery.ToString(), cbDZInstrument);
            FillDropDownList(Program.SoloAMG_DZConnectionString, sbInsertDesktopQuery.ToString(), cbNewsInstrument);
        }
        else
        {
            cbDZInstrument.SelectedIndex = 0;
        }

        // как брать с combobox значения - https://stackoverflow.com/questions/906899/binding-an-enum-to-a-winforms-combo-box-and-then-setting-it
        cbPosition.DataSource           = Enum.GetValues(typeof(Enums.DZPosition));
        cbPosition.SelectedItem         = Enums.DZPosition.Empty;
        cbDirection.DataSource          = Enum.GetValues(typeof(Enums.DZDirection));
        cbDirection.SelectedItem        = Enums.DZDirection.Empty;
        cbInstrumentRisk.DataSource     = Enum.GetValues(typeof(Enums.InstrumentRisk));
        cbInstrumentRisk.SelectedItem   = Enums.InstrumentRisk.InstrumentRisk01;

        txtLevelPrice.Text              = String.Empty;
        
        SetDefaultDateTime();

        txtEntryPrice.Text              = String.Empty;
        txtExitPrice.Text               = String.Empty;
        nmrQuantity.Value               = 1;
        txtScreenFile.Text              = String.Empty;
        txtComment.Text                 = String.Empty;
      }
      catch (Exception ex)
      {
      }
    }

    public bool CheckForm()
    {
      // здесь сделать проверку на правильную дату дз, на полноту заполнения полей
      try
      {
        if (cbPosition.SelectedIndex == (int)Enums.DZPosition.Empty)
        {
            MessageBox.Show("НЕ ВЫБРАНО ПОЛЕ : < Position >");
            return false;
        }

        if (cbDirection.SelectedIndex == (int)Enums.DZDirection.Empty)
        {
            MessageBox.Show("НЕ ВЫБРАНО ПОЛЕ : < Direction >");
            return false;
        }

        if (cbInstrumentRisk.SelectedIndex == (int)Enums.InstrumentRisk.Empty)
        {
            MessageBox.Show("НЕ ВЫБРАНО ПОЛЕ : < Instrument Risk >");
            return false;
        }

        return true;
      }
      catch (Exception ex)
      {
      }

      return false;
    }

    public bool CreateCode()
    {
      try
      {
        if (!CheckForm())
            return false;

      }
      catch (Exception ex)
      {
      }

      return false;
    }

    public void FillDropDownList(String ConnectionString, String Query, ComboBox DropDownName)
    {
      // If you use a DataTable (or any object which implmenets IEnumerable)
      // you can bind the results of your query directly as the 
      // datasource for the ComboBox. 
      DataTable dt = new DataTable();

      // Where possible, use the using block for data access. The 
      // using block handles disposal of resources and connection 
      // cleanup for you:
      using (OleDbConnection cn = new OleDbConnection(ConnectionString))
      {
        using (var cmd = new OleDbCommand(Query, cn))
        {
          cn.Open();

          try
          {
            dt.Load(cmd.ExecuteReader());
          }
          catch (OleDbException e)
          {
            // Do some logging or something. 
            MessageBox.Show("There was an error accessing your data. DETAIL: " + e.ToString());
          }
        }
      }

      DropDownName.DataSource     = dt;
      DropDownName.ValueMember    = dt.Columns[0].ColumnName;
      DropDownName.DisplayMember  = dt.Columns[1].ColumnName;
    }

    public void FillListBoxThroughTheCollection(ListBox ListBoxName)
    {
      try
      {
        DateTime dtFromDate   = new DateTime(dtFilterFromDate.Value.Year, dtFilterFromDate.Value.Month, dtFilterFromDate.Value.Day, 0,0,0);
        DateTime dtToDate     = new DateTime(dtFilterToDate.Value.Year, dtFilterToDate.Value.Month, dtFilterToDate.Value.Day, 23,59,59);

        NTDzNewsCollection objNTDzNewsCollection = new NTDzNewsCollection();
        objNTDzNewsCollection.FillByDateTimeInterval(dtFromDate, dtToDate);

        ListBoxName.DataSource     = objNTDzNewsCollection;
        ListBoxName.ValueMember    = "NewsID";
        ListBoxName.DisplayMember  = "DisplayValue";//"NewsDateTime";

      }
      catch (Exception ex)
      {
      }    
    }

    public void FillListBox(String ConnectionString, String Query, ListBox ListBoxName, Int32 ValueMemberColumnsIndex, Int32 DisplayMemberColumnsIndex)
    {
      // If you use a DataTable (or any object which implmenets IEnumerable)
      // you can bind the results of your query directly as the 
      // datasource for the ComboBox. 
      DataTable dt = new DataTable();

      // Where possible, use the using block for data access. The 
      // using block handles disposal of resources and connection 
      // cleanup for you:
      using (OleDbConnection cn = new OleDbConnection(ConnectionString))
      {
        using (var cmd = new OleDbCommand(Query, cn))
        {
          cn.Open();

          try
          {
            dt.Load(cmd.ExecuteReader());
          }
          catch (OleDbException e)
          {
            // Do some logging or something. 
            MessageBox.Show("There was an error accessing your data. DETAIL: " + e.ToString());
          }
        }
      }

      ListBoxName.DataSource     = dt;
      ListBoxName.ValueMember    = dt.Columns[ValueMemberColumnsIndex].ColumnName;
      ListBoxName.DisplayMember  = dt.Columns[DisplayMemberColumnsIndex].ColumnName;
    }

    public void SetDZControlsEnable(Boolean blnEnable, Enums.DZOperationType enmDZOperationType)
    {
      try
      {
        String[] arrCtrlNames = new String[] { "dtEntryTimeDate", "dtEntryTimeTime", 
                                                "dtExitTimeDate",  "dtExitTimeTime",
                                                "txtEntryPrice",   "txtExitPrice", 
                                                "nmrQuantity",     "btnSelectScreenFile",
                                                "txtScreenFile",   "cbDZNews"};

        foreach (Control ctrl in panel5.Controls)
        {
          if (ctrl.Name == "gbAddNews")
              continue;

          if (enmDZOperationType == Enums.DZOperationType.New)
          {
              if (arrCtrlNames.Contains(ctrl.Name))
                  continue;
          }

          ctrl.Enabled = blnEnable;
        } 
      }
      catch (Exception ex)
      {
      }
    }

    private void TextBoxCheckKeyPress(KeyPressEventArgs e)
    {
      try
      {
        char number = e.KeyChar;

        if ((e.KeyChar <= 47 || e.KeyChar >= 58) && number != 8 && number != 44) //цифры, клавиша BackSpace точка и запятая а ASCII
        {
              e.Handled = true;
        }
      }
      catch (Exception ex)
      {
      }
    }

    private void TextBoxCheckKeyUp(object sender, KeyEventArgs e)
    {
      try
      {
        if (e.KeyData == (Keys.Control | Keys.V))
            (sender as TextBox).Paste();

        Double dblValue;
        if (Double.TryParse((sender as TextBox).Text, out dblValue))
          return;

        (sender as TextBox).Text = String.Empty;
      }
      catch (Exception ex)
      {
      }
    }
    
    private void SetDefaultDateTime()
    {
      try
      {
        DateTime dtValue = DateTime.MinValue;

        dtValue =  new DateTime(dtFormDefaultDateTime.Value.Year, dtFormDefaultDateTime.Value.Month, dtFormDefaultDateTime.Value.Day, 0, 0, 0);


        dtFilterFromDate.Value  = dtValue;
        dtFilterToDate.Value    = dtValue;

        dtAddNewsDate.Value     = dtValue;
        dtAddNewsTime.Value     = dtValue;

        dtDZMainDate.Value      = dtValue;
          
        dtEntryTimeDate.Value   = dtValue;
        dtEntryTimeTime.Value   = dtValue;
          
        dtExitTimeDate.Value    = dtValue;
        dtExitTimeTime.Value    = dtValue;
      }
      catch (Exception ex)
      {
      }
    }

    public void FillNewsListBox()
    {
      try
      {
        FillListBoxThroughTheCollection(lstNews);
        return;

//        /*
        StringBuilder sbQuery   = new StringBuilder();
        DateTime dtFromDate   = new DateTime(dtFilterFromDate.Value.Year, dtFilterFromDate.Value.Month, dtFilterFromDate.Value.Day, 0,0,0);
        DateTime dtToDate     = new DateTime(dtFilterToDate.Value.Year, dtFilterToDate.Value.Month, dtFilterToDate.Value.Day, 23,59,59);

        sbQuery.Append("SELECT (Select Sum(1) From News P Where P.ID <= P1.ID) AS [Number], ");
        sbQuery.Append("P1.ID, [Number] & ') ' & Instrument.Name & ' - ' &  P1.NewsDateTime as NewsDateTime ");
        sbQuery.Append("FROM News as P1 INNER JOIN Instrument ON P1.InstrumentID = Instrument.ID ");
        sbQuery.Append("WHERE P1.NewsDateTime >= #" + dtFromDate.ToString("yyyy.MM.dd HH:mm:ss").Replace('.', '-') + "# ");
        sbQuery.Append("AND P1.NewsDateTime <= #"   + dtToDate.ToString("yyyy.MM.dd HH:mm:ss").Replace('.', '-') + "# ");
        sbQuery.Append("ORDER BY P1.ID;");

        FillListBox(Program.SoloAMG_DZConnectionString, sbQuery.ToString(), lstNews, 1, 2);
//        */
      }
      catch (Exception ex)
      {
      }
    }

    private void btnSetDefaultFormDateTime_Click(object sender, EventArgs e)
    {
      SetDefaultDateTime();
    }

  }
}
