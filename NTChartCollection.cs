﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace NinjaTraderCustomWorkspaces
{
  public class NTChartCollection : System.Collections.CollectionBase
  {
    private Int32 m_iDesktopID;

    public IntPtr GetHandleByChartWindowCaption(String strChartWindowCaption)
    {
      foreach (NTChart objNTChart in this)
        if (objNTChart.ChartWindowCaption == strChartWindowCaption)
            return objNTChart.Handle; 

      return IntPtr.Zero;
    }

    public Boolean IsCharExist(Enums.ChartType enmChartType)
    {
      Boolean blnReturn = false;

      try
      {
        foreach (NTChart objNTChart in this)
        {
          switch (enmChartType)
          {
            case Enums.ChartType.Time5Min:    blnReturn = blnReturn | objNTChart.Is5MinChart;       break;
            case Enums.ChartType.Time60Min:   blnReturn = blnReturn | objNTChart.Is60MinChart;      break;
            case Enums.ChartType.Time1440Min: blnReturn = blnReturn | objNTChart.Is1440MinChart;    break;
            case Enums.ChartType.TimeWeekly:  blnReturn = blnReturn | objNTChart.IsWeeklyChart;     break;
            case Enums.ChartType.FootPrint:   blnReturn = blnReturn | objNTChart.IsFootPrintChart;  break;
          }
        }
      }
      catch (Exception ex)
      {
      }

      return blnReturn;
    }

    public void FillMain()
    {
      // заполнение коллекции всеми имеющимися графиками
      List<IntPtr> lstWindows = Program.NTWindows;
      FillMain(lstWindows);
    }

    private void FillMain(List<IntPtr> lstWindows)
    {
      this.Clear();

      List<NTChart> lstNTCharts = new List<NTChart>();

      for (int i = 0; i < lstWindows.Count; i++)
      {
        String strWindowCaption = Emulations.Window.GetWindowTextByHandle(lstWindows[i]);
        NTChart objNTChart   = new NTChart(strWindowCaption, lstWindows[i]);
        lstNTCharts.Add(objNTChart);
      }

      lstNTCharts.Sort();

      foreach (NTChart objNTChart in lstNTCharts)
        this.Add(objNTChart);      
    }

    public void FillRemote()
    {
      this.Clear();

      String strRemoteCommandResult = Program.SendMessageToNTServer("GetNTChartWindows");

      if (strRemoteCommandResult == "ServerNotAvailable")
          return;

      List<NTChart> lstNTCharts = new List<NTChart>();
      String[] strArr           = strRemoteCommandResult.Split(Char.Parse(";"));

      for (int i = 0; i < strArr.Length; i++)
      {
        String strWindowCaption = strArr[i].Split(Char.Parse("|"))[0];
        IntPtr hndHandle        = Emulations.Convert.Functions.StringToIntPtr(strArr[i].Split(Char.Parse("|"))[1]);
        NTChart objNTChart      = new NTChart(strWindowCaption, hndHandle, Enums.ChartLocation.Remote);

        lstNTCharts.Add(objNTChart);
      }

      lstNTCharts.Sort();

      foreach (NTChart objNTChart in lstNTCharts)
        this.Add(objNTChart);
    }

    public void FillMain60Min5MinAndFootPrint(String strInstrumentName)
    {
      // заполнение коллекции по имени графика, например, имя = "6E 12-12"
      List<IntPtr> lstWindows = Program.NTWindows;

      for (int i = 0; i < lstWindows.Count; i++)
      {
        String strWindowText = Emulations.Window.GetWindowTextByHandle(lstWindows[i]);
        NTChart objNTChart   = new NTChart(strWindowText, lstWindows[i]);

        if (objNTChart.Name != strInstrumentName)
            continue;

        if (objNTChart.Is5MinChart || objNTChart.IsFootPrintChart || objNTChart.Is60MinChart || objNTChart.Is1440MinChart ||
            objNTChart.IsAlerts || objNTChart.IsControlCenter || objNTChart.IsWeeklyChart)
        {
            this.Add(objNTChart);
        }
      }

      SetPositionsFor5MinAndFootPrint();
    }

    public void FillRemote60Min5MinAndFootPrint(String strInstrumentName)
    {
      this.Clear();

      String strRemoteCommandResult = Program.SendMessageToNTServer("GetNTChartWindows");

      if (strRemoteCommandResult == "ServerNotAvailable")
          return;

      String[] strArr = strRemoteCommandResult.Split(Char.Parse(";"));

      for (int i = 0; i < strArr.Length; i++)
      {
        String strWindowCaption = strArr[i].Split(Char.Parse("|"))[0];
        IntPtr hndHandle        = Emulations.Convert.Functions.StringToIntPtr(strArr[i].Split(Char.Parse("|"))[1]);
        NTChart objNTChart      = new NTChart(strWindowCaption, hndHandle, Enums.ChartLocation.Remote);

        if (objNTChart.Name != strInstrumentName)
            continue;

        if (objNTChart.Is5MinChart || objNTChart.IsFootPrintChart || objNTChart.Is60MinChart || objNTChart.Is1440MinChart ||
            objNTChart.IsAlerts || objNTChart.IsControlCenter || objNTChart.IsWeeklyChart)
        {
            this.Add(objNTChart);
        }

        SetPositionsFor5MinAndFootPrint();
      }
    }

    public void SetPositionsFor5MinAndFootPrint()
    {
      foreach (NTChart objNTChart in this)
      {
        if (objNTChart.Is5MinChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionTopHalfScreenHeight(Program.ScreensResolution.MainSecondScreenBounds);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
                objNTChart.SetPositionTopHalfScreenHeight(Program.ScreensResolution.RemoteSecondScreenBounds);
        }

        if (objNTChart.IsFootPrintChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionBottomHalfScreenHeight(Program.ScreensResolution.MainSecondScreenBounds);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
                objNTChart.SetPositionBottomHalfScreenHeight(Program.ScreensResolution.RemoteSecondScreenBounds);
        }
      }
    }

    public void BaseShow_Standart()
    {
      foreach (NTChart objNTChart in this)
      {
        if (objNTChart.Is5MinChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionTopHalfScreenHeight(Program.ScreensResolution.MainSecondScreenBounds);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
                objNTChart.SetPositionTopHalfScreenHeight(Program.ScreensResolution.RemoteSecondScreenBounds);

            objNTChart.Show();
        }

        if (objNTChart.IsFootPrintChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionBottomHalfScreenHeight(Program.ScreensResolution.MainSecondScreenBounds);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
                objNTChart.SetPositionBottomHalfScreenHeight(Program.ScreensResolution.RemoteSecondScreenBounds);
          
            objNTChart.Show();
        }

        if (objNTChart.Is60MinChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.MainFirstScreenWorkingArea);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
              objNTChart.SetPositionFullScreen(Program.ScreensResolution.RemoteFirstScreenWorkingArea);
          
            objNTChart.Show();
        }
      } 
    }

    public void BaseShow_Standart2()
    {
      foreach (NTChart objNTChart in this)
      {
        if (objNTChart.Is5MinChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionTopHalfScreenHeight(Program.ScreensResolution.MainFirstScreenWorkingArea);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
                objNTChart.SetPositionTopHalfScreenHeight(Program.ScreensResolution.RemoteFirstScreenWorkingArea);

            objNTChart.Show();
        }

        if (objNTChart.IsFootPrintChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.MainSecondScreenBounds);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.RemoteSecondScreenBounds);

            objNTChart.Show();
        }

        if (objNTChart.Is60MinChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionBottomHalfScreenHeight(Program.ScreensResolution.MainFirstScreenWorkingArea);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
                objNTChart.SetPositionBottomHalfScreenHeight(Program.ScreensResolution.RemoteFirstScreenWorkingArea);

            objNTChart.Show();
        }
      }
    }

    public void BaseShow_Standart3()
    {
      foreach (NTChart objNTChart in this)
      {
        if (objNTChart.Is5MinChart)
        {
          if (objNTChart.Location == Enums.ChartLocation.Main)
            objNTChart.SetPositionTopHalfScreenHeight(Program.ScreensResolution.MainSecondScreenBounds);

          if (objNTChart.Location == Enums.ChartLocation.Remote)
            objNTChart.SetPositionTopHalfScreenHeight(Program.ScreensResolution.RemoteSecondScreenBounds);

          objNTChart.Show();
        }

        if (objNTChart.Is1440MinChart)
        {
          if (objNTChart.Location == Enums.ChartLocation.Main)
            objNTChart.SetPositionBottomHalfScreenHeight(Program.ScreensResolution.MainSecondScreenBounds);

          if (objNTChart.Location == Enums.ChartLocation.Remote)
            objNTChart.SetPositionBottomHalfScreenHeight(Program.ScreensResolution.RemoteSecondScreenBounds);

          objNTChart.Show();
        }

        if (objNTChart.Is60MinChart)
        {
          if (objNTChart.Location == Enums.ChartLocation.Main)
            objNTChart.SetPositionFullScreen(Program.ScreensResolution.MainFirstScreenWorkingArea);

          if (objNTChart.Location == Enums.ChartLocation.Remote)
            objNTChart.SetPositionFullScreen(Program.ScreensResolution.RemoteFirstScreenWorkingArea);

          objNTChart.Show();
        }
      }
    }

    public void BaseShow_Standart4()
    {
      foreach (NTChart objNTChart in this)
      {
        if (objNTChart.Is60MinChart)
        {
          if (objNTChart.Location == Enums.ChartLocation.Main)
            objNTChart.SetPositionTopHalfScreenHeight(Program.ScreensResolution.MainSecondScreenBounds);

          if (objNTChart.Location == Enums.ChartLocation.Remote)
            objNTChart.SetPositionTopHalfScreenHeight(Program.ScreensResolution.RemoteSecondScreenBounds);

          objNTChart.Show();
        }

        if (objNTChart.IsWeeklyChart)
        {
          if (objNTChart.Location == Enums.ChartLocation.Main)
            objNTChart.SetPositionBottomHalfScreenHeight(Program.ScreensResolution.MainSecondScreenBounds);

          if (objNTChart.Location == Enums.ChartLocation.Remote)
            objNTChart.SetPositionBottomHalfScreenHeight(Program.ScreensResolution.RemoteSecondScreenBounds);

          objNTChart.Show();
        }

        if (objNTChart.Is1440MinChart)
        {
          if (objNTChart.Location == Enums.ChartLocation.Main)
            objNTChart.SetPositionFullScreen(Program.ScreensResolution.MainFirstScreenWorkingArea);

          if (objNTChart.Location == Enums.ChartLocation.Remote)
            objNTChart.SetPositionFullScreen(Program.ScreensResolution.RemoteFirstScreenWorkingArea);

          objNTChart.Show();
        }
      }
    }

    public void BaseShow_Standart5()
    {
      foreach (NTChart objNTChart in this)
      {
        if (objNTChart.Is5MinChart)
        {
          if (objNTChart.Location == Enums.ChartLocation.Main)
            objNTChart.SetPositionTopHalfScreenHeight(Program.ScreensResolution.MainSecondScreenBounds);

          if (objNTChart.Location == Enums.ChartLocation.Remote)
            objNTChart.SetPositionTopHalfScreenHeight(Program.ScreensResolution.RemoteSecondScreenBounds);

          objNTChart.Show();
        }

        if (objNTChart.Is1440MinChart)
        {
          if (objNTChart.Location == Enums.ChartLocation.Main)
            objNTChart.SetPositionQuarter(Program.ScreensResolution.MainSecondScreenBounds, Enums.ChartQuarterPosition.BottomRight);

          if (objNTChart.Location == Enums.ChartLocation.Remote)
            objNTChart.SetPositionQuarter(Program.ScreensResolution.RemoteSecondScreenBounds, Enums.ChartQuarterPosition.BottomRight);

          objNTChart.Show();
        }

        if (objNTChart.Is60MinChart)
        {
          if (objNTChart.Location == Enums.ChartLocation.Main)
            objNTChart.SetPositionQuarter(Program.ScreensResolution.MainSecondScreenBounds, Enums.ChartQuarterPosition.BottomLeft);

          if (objNTChart.Location == Enums.ChartLocation.Remote)
            objNTChart.SetPositionQuarter(Program.ScreensResolution.RemoteSecondScreenBounds, Enums.ChartQuarterPosition.BottomLeft);

          objNTChart.Show();
        }
      }
    }

    public void BaseShow_Standart6()
    {
      foreach (NTChart objNTChart in this)
      {
        if (objNTChart.Is60MinChart)
        {
          if (objNTChart.Location == Enums.ChartLocation.Main)
            objNTChart.SetPositionTopHalfScreenHeight(Program.ScreensResolution.MainSecondScreenBounds);

          if (objNTChart.Location == Enums.ChartLocation.Remote)
            objNTChart.SetPositionTopHalfScreenHeight(Program.ScreensResolution.RemoteSecondScreenBounds);

          objNTChart.Show();
        }

        if (objNTChart.IsWeeklyChart)
        {
          if (objNTChart.Location == Enums.ChartLocation.Main)
            objNTChart.SetPositionQuarter(Program.ScreensResolution.MainSecondScreenBounds, Enums.ChartQuarterPosition.BottomRight);

          if (objNTChart.Location == Enums.ChartLocation.Remote)
            objNTChart.SetPositionQuarter(Program.ScreensResolution.RemoteSecondScreenBounds, Enums.ChartQuarterPosition.BottomRight);

          objNTChart.Show();
        }

        if (objNTChart.Is1440MinChart)
        {
          if (objNTChart.Location == Enums.ChartLocation.Main)
            objNTChart.SetPositionQuarter(Program.ScreensResolution.MainSecondScreenBounds, Enums.ChartQuarterPosition.BottomLeft);

          if (objNTChart.Location == Enums.ChartLocation.Remote)
            objNTChart.SetPositionQuarter(Program.ScreensResolution.RemoteSecondScreenBounds, Enums.ChartQuarterPosition.BottomLeft);

          objNTChart.Show();
        }
      }
    }

    public void BaseShow_1440And60FullScreen()
    {
      foreach (NTChart objNTChart in this)
      {
        if (objNTChart.Is60MinChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.MainFirstScreenWorkingArea);

            objNTChart.Show();
        }

        if (objNTChart.Is1440MinChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.MainSecondScreenBounds);
        }
      }
    }

    public void BaseShow_5MinAnd60Min()
    {
      foreach (NTChart objNTChart in this)
      {
        if (objNTChart.Is5MinChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionTopHalfScreenHeight(Program.ScreensResolution.MainSecondScreenBounds);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
                objNTChart.SetPositionTopHalfScreenHeight(Program.ScreensResolution.RemoteSecondScreenBounds);
          
            objNTChart.Show();
        }

        if (objNTChart.Is60MinChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionBottomHalfScreenHeight(Program.ScreensResolution.MainSecondScreenBounds);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
                objNTChart.SetPositionBottomHalfScreenHeight(Program.ScreensResolution.RemoteSecondScreenBounds);
          
            objNTChart.Show();
        }
      }
    }

    public void BaseShow_5MinFullScreen()
    {
      foreach (NTChart objNTChart in this)
      {
        if (objNTChart.Is5MinChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.MainSecondScreenBounds);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.RemoteSecondScreenBounds);
          
            objNTChart.Show();
        }
      }
    }

    public void BaseShow_1440MinFullScreen()
    {
      foreach (NTChart objNTChart in this)
      {
        if (objNTChart.Is1440MinChart)
        {
          if (objNTChart.Location == Enums.ChartLocation.Main)
            objNTChart.SetPositionFullScreen(Program.ScreensResolution.MainSecondScreenBounds);

          if (objNTChart.Location == Enums.ChartLocation.Remote)
            objNTChart.SetPositionFullScreen(Program.ScreensResolution.RemoteSecondScreenBounds);

          objNTChart.Show();
        }
      }
    }

    public void BaseShow_WEEKFullScreen()
    {
      foreach (NTChart objNTChart in this)
      {
        if (objNTChart.IsWeeklyChart)
        {
          if (objNTChart.Location == Enums.ChartLocation.Main)
            objNTChart.SetPositionFullScreen(Program.ScreensResolution.MainSecondScreenBounds);

          if (objNTChart.Location == Enums.ChartLocation.Remote)
            objNTChart.SetPositionFullScreen(Program.ScreensResolution.RemoteSecondScreenBounds);

          objNTChart.Show();
        }
      }
    }

    public void BaseShow_60MinFullScreen()
    {
      foreach (NTChart objNTChart in this)
      {
        if (objNTChart.Is60MinChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.MainSecondScreenBounds);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.RemoteSecondScreenBounds);

            objNTChart.Show();
            //JumpToBarOnChartControl(objNTChart);

            SetNTWindowText("test", objNTChart);
        }
      }

      
    }

    private void SetNTWindowText(String strText, NTChart objNTChart)
    {
      try
      {
        String strOldText = Emulations.Window.GetWindowTextByHandle(objNTChart.Handle);

        //System.Windows.Forms.MessageBox.Show(strOldText);

        String strNewText = strOldText + " ... new text";

        Emulations.Window.SetWindowTextBySendMessage(objNTChart.Handle, strNewText);
      }
      catch (Exception ex)
      {
      }    
    }

    private void JumpToBarOnChartControl(NTChart objNTChart)
    {
      try
      {
        IntPtr p = objNTChart.Handle;

        System.Windows.Forms.Form frm = (System.Windows.Forms.Form)System.Windows.Forms.Control.FromHandle(p);


      }
      catch (Exception ex)
      {
      }
    }

    public void BaseShow_5MinLeftFootRight()
    {
      foreach (NTChart objNTChart in this)
      {
        if (objNTChart.Is5MinChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.MainFirstScreenWorkingArea);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.RemoteFirstScreenWorkingArea);
          
            objNTChart.Show();
        }

        if (objNTChart.IsFootPrintChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.MainSecondScreenBounds);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.RemoteSecondScreenBounds);
          
            objNTChart.Show();
        }
      }
    }

    public void BaseShow_FootLeft5MinRight()
    {
      foreach (NTChart objNTChart in this)
      {
        if (objNTChart.Is5MinChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.MainSecondScreenBounds);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.RemoteSecondScreenBounds);
          
            objNTChart.Show();
        }

        if (objNTChart.IsFootPrintChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.MainFirstScreenWorkingArea);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.RemoteFirstScreenWorkingArea);
          
            objNTChart.Show();
        }
      }
    }

    public void BaseShow_FootRight5MinLeftOneScreen()
    {
      foreach (NTChart objNTChart in this)
      {
        if (objNTChart.Is5MinChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionLeftHalfScreenWidth(Program.ScreensResolution.MainSecondScreenBounds);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
                objNTChart.SetPositionLeftHalfScreenWidth(Program.ScreensResolution.RemoteSecondScreenBounds);
          
            objNTChart.Show();
        }

        if (objNTChart.IsFootPrintChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionRightHalfScreenWidth(Program.ScreensResolution.MainSecondScreenBounds);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
                objNTChart.SetPositionRightHalfScreenWidth(Program.ScreensResolution.RemoteSecondScreenBounds);
          
            objNTChart.Show();
        }

        if (objNTChart.Is60MinChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.MainFirstScreenWorkingArea);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.RemoteFirstScreenWorkingArea);
          
            objNTChart.Show();
        }
      } 
    }

    public NTChart Get5minChartByShortName(String strShortName/*, NinjaTraderCustomWorkspaces.Enums.ChartType enmChartType*/)
    {
      foreach (NTChart objNTChart in this)
      {
        if (objNTChart.Is5MinChart && objNTChart.ShortName == strShortName)
        {
            return objNTChart;
        }
      }

      return null;
    }


    public void BaseShow_FootLeft5MinRightOneScreen()
    {
      foreach (NTChart objNTChart in this)
      {
        if (objNTChart.Is5MinChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
               objNTChart.SetPositionRightHalfScreenWidth(Program.ScreensResolution.MainSecondScreenBounds);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
                objNTChart.SetPositionRightHalfScreenWidth(Program.ScreensResolution.RemoteSecondScreenBounds);
          
            objNTChart.Show();
        }

        if (objNTChart.IsFootPrintChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionLeftHalfScreenWidth(Program.ScreensResolution.MainSecondScreenBounds);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
                objNTChart.SetPositionLeftHalfScreenWidth(Program.ScreensResolution.RemoteSecondScreenBounds);
          
            objNTChart.Show();
        }

        if (objNTChart.Is60MinChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.MainFirstScreenWorkingArea);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.RemoteFirstScreenWorkingArea);
          
            objNTChart.Show();
        }
      }
    }

    public void BaseShow_5Min60minLeftFootRightOneScreen()
    {
      foreach (NTChart objNTChart in this)
      {
        if (objNTChart.Is5MinChart)
        {
          if (objNTChart.Location == Enums.ChartLocation.Main)
            objNTChart.SetPositionTopLeftQuarterScreenWidth(Program.ScreensResolution.MainSecondScreenBounds);

          if (objNTChart.Location == Enums.ChartLocation.Remote)
            objNTChart.SetPositionTopLeftQuarterScreenWidth(Program.ScreensResolution.RemoteSecondScreenBounds);

          objNTChart.Show();
        }

        if (objNTChart.IsFootPrintChart)
        {
          if (objNTChart.Location == Enums.ChartLocation.Main)
            objNTChart.SetPositionRightHalfScreenWidth(Program.ScreensResolution.MainSecondScreenBounds);

          if (objNTChart.Location == Enums.ChartLocation.Remote)
            objNTChart.SetPositionRightHalfScreenWidth(Program.ScreensResolution.RemoteSecondScreenBounds);

          objNTChart.Show();
        }

        if (objNTChart.Is60MinChart)
        {
          if (objNTChart.Location == Enums.ChartLocation.Main)
            objNTChart.SetPositionBottomLeftQuarterScreenWidth(Program.ScreensResolution.MainSecondScreenBounds);

          if (objNTChart.Location == Enums.ChartLocation.Remote)
            objNTChart.SetPositionBottomLeftQuarterScreenWidth(Program.ScreensResolution.RemoteSecondScreenBounds);

          objNTChart.Show();
        }
      }
    }

    public void BaseShow_FullScreens60MinLeft5MinRight()
    {
      foreach (NTChart objNTChart in this)
      {
        if (objNTChart.Is5MinChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.MainSecondScreenBounds);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.RemoteSecondScreenBounds);
          
            objNTChart.Show();
        }

        if (objNTChart.Is60MinChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.MainFirstScreenWorkingArea);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.RemoteFirstScreenWorkingArea);
          
            objNTChart.Show();
        }
      }
    }

    public void BaseShow_FullScreens60MinRight5MinLeft()
    {
      foreach (NTChart objNTChart in this)
      {
        if (objNTChart.Is60MinChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                  objNTChart.SetPositionFullScreen(Program.ScreensResolution.MainSecondScreenBounds);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.RemoteSecondScreenBounds);

            objNTChart.Show();
        }

        if (objNTChart.Is5MinChart)
        {
            if (objNTChart.Location == Enums.ChartLocation.Main)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.MainFirstScreenWorkingArea);

            if (objNTChart.Location == Enums.ChartLocation.Remote)
                objNTChart.SetPositionFullScreen(Program.ScreensResolution.RemoteFirstScreenWorkingArea);

            objNTChart.Show();
        }
      }
    }

    public void SetPositionAndShow()
    {
      foreach (NTChart objNTChart in this)
        objNTChart.SetPositionAndShow();
    }

    public void Show()
    {
      foreach (NTChart objNTChart in this)
      {
        objNTChart.Show();
      }
    }

    public Int32 Add(NTChart objNTChart)
    {
      return (List.Add(objNTChart));
    }

    public Int32 IndexOf(NTChart objNTChart)
    {
      return (List.IndexOf(objNTChart));
    }

    public void Insert(Int32 index, NTChart objNTChart)
    {
      List.Insert(index, objNTChart);
    }

    public void Remove(NTChart objNTChart)
    {
      List.Remove(objNTChart);
    }

    public NTChart this[int index]
    {
      get
      {
        return (List[index] as NTChart);
      }
      set
      {
        List[index] = value;
      }
    }

    public NTChart this[String strName]
    {
      get
      {
        for (int i = 0; i < this.Count; i++)
        {
          if (this[i].ChartWindowCaption == strName)
              return this[i] as NTChart;
          
          if (this[i].Name == strName)
              return this[i] as NTChart;

          if (this[i].ShortName + " " + this[i].Period == strName)
              return this[i] as NTChart;
        }

        return null;
      }
    }

    public NTChart this[NTStructures.ChartPoint structChartPoint]
    {
      get
      {
        for (int i = 0; i < this.Count; i++)
        {
          if (this[i].GridX == structChartPoint.X && this[i].GridY == structChartPoint.Y)
              return this[i] as NTChart;
        }

        return null;
      }
    }

    public Int32 DesktopID
    {
      get
      {
        return m_iDesktopID;
      }
    }
  }
}
