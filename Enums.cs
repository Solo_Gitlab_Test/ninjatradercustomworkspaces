using System;
using System.Collections.Generic;
using System.Text;

namespace NinjaTraderCustomWorkspaces
{
  public static class Enums
  {
    public enum ChartType: int
    {
      Time5Min    = 1,
      FootPrint   = 2,
      Time60Min   = 3,
      Time1440Min = 4,
      TimeWeekly  = 5
    };

    public enum ChartDisposition : int
    {
      MainFirstScreen    = 1, // Fujitsu 1
      MainSecondScreen   = 2, // Fujitsu 2
      RemoteFirstScreen  = 3, // Lenovo 1
      RemoteSecondScreen = 4, // Lenovo 2
      Empty              = 5
    };

    public enum ChartQuarterPosition : int
    {
      TopLeft      = 1, 
      TopRight     = 2, 
      BottomLeft   = 3, 
      BottomRight  = 4,
      FullScreen   = 5
    };

    public enum ChartLocation : int
    {
      Main   = 0, 
      Remote = 1
    };

    public enum DZPosition : int
    {
      Empty       = 0,
      LONG        = 1, 
      SHORT       = 2
    };

    public enum DZDirection : int
    {
      Empty         = 0,
      TREND         = 1, 
      NOTTREND      = 2
    };

    public enum InstrumentRisk : int
    {
      Empty               = 0,
      InstrumentRisk01    = 1
      //InstrumentRiskAUTO  = 2 // автоматический расчет риска
    };	

    public enum DZOperationType : int
    {
      New  = 0,
      Edit = 1,
      Save = 2,
      Default = 3
    };

  }
}
